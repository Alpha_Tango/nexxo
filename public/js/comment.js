var comment = {
  initUi : function(){
		$(".btn-open-comments").off().click(function(){
		    var parentType = $(this).data("parent-type");
		    var parentId = $(this).data("parent-id");
		    comment.getCommentsNews(parentType, parentId);
		});
	},

	initFormAddComment : function(){
		$(".form-add-comment").off().keydown(function(e){ 
			var code = e.which; // recommended to use e.which, it's normalized across browsers
			if(code==13){
	    	e.preventDefault();
	    	console.log("detect comment validation");
	    	var parentType = $(this).data("parent-type");
	    	var parentId = $(this).data("parent-id");
	    	var text = $(this).val();
	    	var token = $("#news__token").val();
        if(text != "")
	        comment.addComment(parentType, parentId, text);
  		} // missing closing if brace
		});

    $(".btn-open-comments").off().click(function(){
        var parentType = $(this).data("parent-type");
        var parentId = $(this).data("parent-id");
        comment.getCommentsNews(parentType, parentId);
    });

    $(".reply-to-comment").off().keydown(function(e){ 
      var code = e.which; // recommended to use e.which, it's normalized across browsers
      if(code==13){
        e.preventDefault();
        console.log("detect answer validation");
        var commentId = $(this).data("comment-id");
        var parentType = $(this).data("parent-type");
        var parentId = $(this).data("parent-id");
        var text = $(this).val();
        var token = $("#news__token").val();
        comment.addComment(parentType, parentId, text, commentId);
      }
    });


    $(".btn-close-comments").off().click(function(){
        var parentType = $(this).data("parent-type");
        var parentId = $(this).data("parent-id");
        $(".comment-block[data-parent-type='"+parentType+"'][data-parent-id='"+parentId+"']").html("");
    });

    $(".form-add-comment, .reply-to-comment").val("");

    $(".btn-open-anwsers").off().click(function(){ console.log("btn-open-anwsers");
      var commentId = $(this).data("comment-id");
      if($(".container-answers[data-comment-id='"+commentId+"']").hasClass("d-none"))
      $(".container-answers[data-comment-id='"+commentId+"']").removeClass("d-none");
      else 
      $(".container-answers[data-comment-id='"+commentId+"']").addClass("d-none");
    });

    $(".btn-comment-like").off().click(function(){
      var parentId = $(this).data("parent-id");
      var parentType = $(this).data("parent-type");
      var commentId = $(this).data("comment-id");
      var likeType = $(this).data("like-type");
      comment.sendLikeComment(parentId, parentType, commentId, likeType, $(this));
    });

    comment.initEditComment();
	},

  initEditComment : function(){
    $(".btn-edit-comment").off().click(function(){
      var commentId = $(this).data("comment-id");
      var commentText = $(".comments-list li[data-comment-id='"+commentId+"'] p .comment-text").html();
      $(".comments-list li[data-comment-id='"+commentId+"'] p").addClass("d-none");
      $(".comments-list li[data-comment-id='"+commentId+"'] .input-edit-comment").removeClass("d-none").val(commentText);
      $(".comments-list li[data-comment-id='"+commentId+"'] .btn-update-comment").removeClass("d-none");

      $(".comments-list li[data-comment-id='"+commentId+"'] .btn-update-comment").off().click(function(){
          var parentId = $(this).data("parent-id");
          var parentType = $(this).data("parent-type");
          var commentId = $(this).data("comment-id");
          var text = $(".comments-list li[data-comment-id='"+commentId+"'] .input-edit-comment").val();
          comment.updateComment(parentId, parentType, commentId, text);
      });

    });

    $(".btn-delete-comment").off().click(function(){
      var commentId = $(this).data("comment-id");
      $(".comments-list li[data-comment-id='"+commentId+"'] .conf-delete-comment").removeClass("d-none");

      $(".comments-list li[data-comment-id='"+commentId+"'] .btn-conf-delete-comment").off().click(function(){
          var parentId = $(this).data("parent-id");
          var parentType = $(this).data("parent-type");
          var commentId = $(this).data("comment-id");
          comment.deleteComment(parentId, parentType, commentId);
      });

      $(".comments-list li[data-comment-id='"+commentId+"'] .btn-cancel-delete-comment").off().click(function(){
        var commentId = $(this).data("comment-id");
        $(".comments-list li[data-comment-id='"+commentId+"'] .conf-delete-comment").addClass("d-none");
      });


    });
  },

  updateComment : function(parentId, parentType, commentId, text){
    $.ajax({
        type: "POST",
        url: "/comment/edit/"+parentId+"/"+parentType+"/"+commentId,
        data: { "comment" : { "text" : text, "_token" : $("#comment__token").val() }},
        success: function(res){
          console.log("response of /comment/save-update/"+parentId+"/"+parentType+"/"+commentId, res);
          if(res.error == false){ 
            toastr.success(res.errorMsg);
            $(".comments-list li[data-comment-id='"+commentId+"'] p .comment-text").html(res.newText);
            $(".comments-list li[data-comment-id='"+commentId+"'] p").removeClass("d-none");
            $(".comments-list li[data-comment-id='"+commentId+"'] .input-edit-comment").addClass("d-none");
            $(".comments-list li[data-comment-id='"+commentId+"'] .btn-update-comment").addClass("d-none");
          }else{
            toastr.success(res.errorMsg);
          }
          
        },
        error: function(error){
          console.log("send comment like error", error);
        }
    }); 
  },

  deleteComment : function(parentId, parentType, commentId, text){
    $.ajax({
        type: "POST",
        url: "/comment/delete/"+parentId+"/"+parentType+"/"+commentId,
        data: { "comment" : { "text" : text, "_token" : $("#comment__token").val() }},
        success: function(res){
          console.log("response of /comment/delete/"+parentId+"/"+parentType+"/"+commentId, res);
          if(res.error == false){ 
            toastr.success(res.errorMsg);
            $(".comments-list li[data-comment-id='"+commentId+"']").remove();
          }else{
            toastr.error(res.errorMsg);
          }
        },
        error: function(error){
          console.log("delete comment like error", error);
        }
    }); 
  },

  getAntiLikeType : function(likeType){
    if(likeType == "likes") return "dislikes";
    if(likeType == "dislikes") return "likes";
  },

  sendLikeComment : function(parentId, parentType, commentId, likeType, button){
      $.ajax({
          type: "POST",
          url: "/comment/send-like/"+parentId+"/"+parentType+"/"+commentId+"/"+likeType,
          // data: data,
          success: function(res){
            console.log("response of /comment/send-like/"+parentId+"/"+parentType+"/"+commentId+"/"+likeType, res);
            /*if(res.error == false){ 
              if(likeType=="likes") toastr.success(res.errorMsg);
              if(likeType=="dislikes") toastr.error(res.errorMsg);
            }else{
              if(likeType=="likes") toastr.success(res.errorMsg);
              if(likeType=="dislikes") toastr.error(res.errorMsg);
            }*/
            var antiLikeType = comment.getAntiLikeType(likeType);
            console.log("antiLikeType", antiLikeType, likeType);
            $("a[data-like-type='"+likeType+"'][data-comment-id='"+commentId+"']"+
               "[data-parent-id='"+parentId+"'][data-parent-type='"+parentType+"'] span b").html(res.newTotal[likeType]);
            
            $("a[data-like-type='"+antiLikeType+"'][data-comment-id='"+commentId+"']"+
               "[data-parent-id='"+parentId+"'][data-parent-type='"+parentType+"'] span b").html(res.newTotal[antiLikeType]);

            res.notif["newTotal"] = res.newTotal;                       
            if(typeof res.notif != "undefined" && res.notif != "")
              Chat.broadcastNotification(res.notif);
          },
          error: function(error){
            console.log("send comment like error", error);
          }
      });
  },

  getCommentsNews : function(parentType, parentId){
    $(".comment-block[data-parent-id='"+parentId+"'][data-parent-type='"+parentType+"']").html(
            "<div class='full-width padding-15 text-center'>"+
              "<i class='fa fa-spin fa-circle-o-notch'></i><br>"+
              t.t("Loading comments")+
            "</div>");
      $.ajax({
        type: "POST",
        url: "/comment-stream/get-comments/"+parentType+"/"+parentId,
        success: function(res){
          if(typeof res.error == "undefined"){
          	$(".comment-block[data-parent-id='"+parentId+"'][data-parent-type='"+parentType+"']").html(res);
          	comment.initFormAddComment();
            reportObj.initUi();
			       //toastr.success(res.errorMsg);
          }else{
              $(".comment-block[data-parent-id='"+parentId+"'][data-parent-type='"+parentType+"']").html(
                "<div class='full-width padding-15 text-center'>"+
                  "<i class='fa fa-spin fa-circle-o-notch'></i><br>"+
                  t.t("Sorry, an error has occured")+
                "</div>");
              toastr.error(res.errorMsg);
          }
        },
      });
  },

  addComment : function(parentType, parentId, text, commentId=null){
    var url = "/comment-stream/add-comment/"+parentType+"/"+parentId;
    if(commentId != null) url += "/"+commentId;

    var textComment = $(".form-add-comment, .reply-to-comment").val();
    $(".form-add-comment, .reply-to-comment").val("");
    console.log("addComment", url);

    if(commentId == null){ //si ce n'est pas une réponse sur un autre commentaire            
    $(".comment-block[data-parent-id='"+parentId+"'][data-parent-type='"+parentType+"'] ul.comments-list.root").prepend(
                "<li class='margin-left-25 padding-bottom-15 comment-loading'>"+
                  "<i class='fa fa-spin fa-circle-o-notch'></i> "+
                  t.t("Sending your comment")+
                "</div>");
    }else{
      $(".comment-block[data-parent-id='"+parentId+"'][data-parent-type='"+parentType+"'][data-comment-id='"+commentId+"'] .container-answers ul.comments-list:first").append(
                "<li class='margin-left-25 padding-bottom-15 comment-loading'>"+
                  "<i class='fa fa-spin fa-circle-o-notch'></i> "+
                  t.t("Sending your comment")+
                "</div>");
    }

    $.ajax({
      type: "POST",
      data : { "comment" : { 
                  _token : $("#comment__token").val(),
                  text : text,
                }
      },
      url: url,
      success: function(res){
          
        if(typeof res.error == "undefined"){
          console.log("success "+url, commentId, res);
          $(".comment-loading").remove();
          if(commentId == null){ //si ce n'est pas une réponse sur un autre commentaire            
            $(".comment-block[data-parent-id='"+parentId+"'][data-parent-type='"+parentType+"'] ul.comments-list.root").prepend(res.html);
          }else{
            $(".comment-block[data-parent-id='"+parentId+"'][data-parent-type='"+parentType+"'] .container-answers[data-comment-id='"+commentId+"'] ul.comments-list:first").append(res.html);
          }
          comment.initFormAddComment();

          if(typeof res.notif != "undefined" && res.notif != "")
            Chat.broadcastNotification(res.notif, res.html, commentId);
        }else{
            //alert("error"); 
            toastr.error(res.error);
            $(".comment-loading").remove();
            $(".form-add-comment, .reply-to-comment").val(textComment);
            /*setTimeout(function(){
              $("article.post[data-news-id='"+parentId+"'], "+
                ".comment-block[data-parent-id='"+parentId+"'], "+
                "li.open-notification[data-about-id='"+parentId+"'][data-about-type='news']").remove();
            }, 4000);*/
        }
      },
      error: function(error){
         $(".comment-loading").remove();
         $(".form-add-comment, .reply-to-comment").val(textComment);
         toastr.error(t.t("Sorry, an error has occured"));
      }
      //dataType: dataType
    });
  },

};