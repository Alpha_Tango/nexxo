var group = {
	initUi : function(){
		$(".btn-modal-add-friend-to-group").off().click(function(){  console.log("onclick #btn-add-friend-to-group");
			var groupId = $(this).data("group-id");
			console.log("groupId", groupId);
			$("#btn-add-friend-to-group").data("group-id", groupId);
		});

		$("#btn-add-friend-to-group").off().click(function(){ console.log("onclick #btn-add-friend-to-group");
			var groupId = $(this).data("group-id");
			var membersId = $("#create-friend-group-add-friends .show-tick .filter-option").html();
			$.ajax({
	          type: "POST",
	          url: "/group/add-members/"+groupId+"/"+membersId,
	          success: function(res){
	          	if(res.error == false){
	          		$("#block-group-"+groupId).replaceWith(res.html);
					group.initUi();
					toastr.success(res.errorMsg);
		        }else{
		        	toastr.error(res.errorMsg);
	        	}
	          },
	          //dataType: dataType
	        });
		});

		$(".btn-modal-delete-group").off().click(function(){
			var groupId = $(this).data("group-id");
			var groupName = $(this).data("group-name");
			console.log("groupId", groupId);
			$("#btn-conf-delete-group").data("group-id", groupId);
			$("#name-group-to-delete").html(groupName);
		});

		$("#btn-save-new-group").off().click(function(){
			var data = {
				"group" : {
					_token : $("#group__token").val(),
					name : $("#group_name").val()
				}
			};

			$.ajax({
	          type: "POST",
	          url: $("form[name='group']").attr("action"),
	          async:true,
			  data: data,
	          success: function(res){
	          	if(res.error == false){
	          		toastr.success(res.errorMsg);	
	          		console.log("groupe created", res.html);
	          		$("#container-groups-blocks").append(res.html);
					group.initUi();
					$(".btn-modal-delete-group").off().click(function(){
						var groupId = $(this).data("group-id");
						var groupName = $(this).data("group-name");
						console.log("groupId", groupId);
						$("#btn-conf-delete-group").data("group-id", groupId);
						$("#name-group-to-delete").html(groupName);
					});
	          	}else{
	          		toastr.error("<b>"+res.errorMsg+"</b>");
	          	}
	          }
	        });
		});

		$(".btn-modal-add-friend-to-group").off().click(function(){  console.log("onclick #btn-add-friend-to-group");
			var groupId = $(this).data("group-id");
			console.log("groupId", groupId);
			$("#btn-add-friend-to-group").data("group-id", groupId);
		});

		$("#btn-conf-delete-group").off().click(function(){ console.log("onclick #btn-add-friend-to-group");
			var groupId = $(this).data("group-id");
			$.ajax({
	          type: "POST",
	          url: "/group/remove-group/"+groupId,
	          //data: { membersSlug: membersSlug},
	          success: function(res){
	          	if(res.error == false){
		            toastr.success(res.errorMsg);
		            $("#block-group-"+groupId).remove();
		        }else{
		        	toastr.error(res.errorMsg);
	        	}
	          }
	        });
		});

		$(".btn-modal-delete-group").off().click(function(){
			var groupId = $(this).data("group-id");
			console.log("groupId", groupId);
			$("#btn-conf-delete-group").data("group-id", groupId);
		});

		$(".btn-toogle-row").off().click(function(){
			var rowId = $(this).data("row-id");
			if($(".row-toggle[data-row-id='"+rowId+"']").hasClass("d-none")){
				$(".row-toggle[data-row-id='"+rowId+"']").removeClass("d-none");
				$(".btn-toogle-row[data-row-id='"+rowId+"'] .fa").removeClass("fa-angle-right").addClass("fa-angle-down");
			}else{
				$(".row-toggle[data-row-id='"+rowId+"']").addClass("d-none");
				$(".btn-toogle-row[data-row-id='"+rowId+"'] .fa").removeClass("fa-angle-down").addClass("fa-angle-right");
			}
		});

		$("#btn-modal-delete-friend-group").off().click(function(){
			var groupId = $(this).data("group-id");
			console.log("groupId", groupId);
			$("#btn-conf-delete-friend-group").data("group-id", groupId);
		});


		$("#btn-conf-delete-friend-group").off().click(function(){ console.log("onclick #btn-add-friend-to-group");
			var groupId = $(this).data("group-id");
			var membersId = $("#create-friend-group-add-friends .show-tick .filter-option").html();
			$.ajax({
	          type: "POST",
	          url: "/group/delete-members/"+groupId+"/"+membersId,
	          success: function(res){
	          	if(res.error == false){
	          		$("#block-group-"+groupId).replaceWith(res.html);
					group.initUi();
					toastr.success(res.errorMsg);
		        }else{
		        	toastr.error(res.errorMsg);
	        	}
	          },
	          //dataType: dataType
	        });
		});

		newsObj.initBWList();

	}

};