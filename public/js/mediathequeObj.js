var mediathequeObj = {

	initMedia : function(){
		$("#btn-load-media").click(function(){
			mediathequeObj.loadMedia();
		});
	},

	loadMedia : function(){
		$("#process-status").html("<i class='fa fa-spin fa-refresh'></i> Mise à jour de la médiathèque, merci de patienter...").removeClass("d-none");
		$.ajax({
        type: "POST",
        url: "/mediatheque/load",
        success: function(res){
          console.log("success /mediatheque/load");
          //if(typeof res.error == "undefined"){
          	//$("#media-list").html(res);
          	//comment.initFormAddComment();
			       toastr.success("chargement de la médiathèque terminé. " + res.msg);
            $("#process-status").html("<span class='letter-green'><i class='fa fa-check'></i> "+res.msg+"</span>");
          //}else{
          //    toastr.error(res.errorMsg);
          //}
        },
        //dataType: dataType
      });
	}
};