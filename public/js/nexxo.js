var nexxo = {

  //baseUrl : BASE_URL,

  initToastr : function(){ console.log("initToastr");
    toastr.options = {
      "debug": false,
      "positionClass": "toast-bottom-left",
      "onclick": null,
      "fadeIn": 300,
      "fadeOut": 1000,
      "timeOut": 5000,
      "extendedTimeOut": 1000
    };
  },


  initCookieConsent : function(){ //return;
      window.addEventListener("load", function(){
      window.cookieconsent.initialise({
        "palette": {
          "popup": {
            "background": "#252e39"
          },
          "button": {
            "background": "transparent",
            "text": "#14a7d0",
            "border": "#14a7d0"
          }
        },
        "content": {
          "message": "Ce site web utilise des cookies pour améliorer votre navigation.",
          "dismiss": "Ok",
          "link": "En savoir plus"
        }
      })});
  },

  initDarkTheme : function(){
    $(".btn-dark-theme").change(function(){
      if($("body").hasClass("dark-theme")){
        $("body").removeClass("dark-theme");
        pageObj.savePreferences("USE_DARK_THEME", false, APP_USER_SLUG);
      }else{
        $("body").addClass("dark-theme");
        pageObj.savePreferences("USE_DARK_THEME", true, APP_USER_SLUG);
      }
    });
    
    if($("body").hasClass("dark-theme"))
      $(".btn-dark-theme").prop('checked', true);
    else
      $(".btn-dark-theme").prop('checked', false);
  },


  initWebSocket : function(){
    console.log("initWebSocket");
    Chat.initChat();
  },

	openQuickView : function(type, id){
    $("#modal-quickview #modal-content").html(
        "<h5 class='padding-15 no-margin'><i class='fa fa-spin fa-circle-o-notch'></i> " + t.t("Loading ...")+"</h5>");
    
    //$("#modal-quickview").modal("show");
		$.ajax({
      type: "POST",
      url: "/"+type+"/quickview/"+id,
      success: function(html){
      	$("#modal-quickview #modal-content").html(html);
      	//$("#modal-quickview").modal("show");
      }
    });
	},


  scrollTo: function(target){ 
    console.log("scrollTo target", target);
    if($(target).length>=1){
      var heightTopBar = $("#site-header").height();
      $('html, body').stop().animate({
            scrollTop: $(target).offset().top - 15 - heightTopBar
        }, 500, '');
    }
  },


  initBtnRequest : function(){ 
    $(".btn-send-request-friend").off().click(function(){
      console.log("test");
      var slugAskForPage = $(this).data("slug-page");
      var originPage = $(this).data("slug-origin-page");

      var confirm = (typeof $(this).data("slug-page-confirm") != "undefined") ? $(this).data("slug-page-confirm") : 'false';
      console.log("confirm", typeof confirm);
      
      if(confirm == "false") {
        $("#modal-quickview #modal-content").html(
        "<h5 class='padding-15 no-margin'><i class='fa fa-spin fa-circle-o-notch'></i> " + t.t("Loading ...")+"</h5>");
        //$("#modal-quickview").modal("show");
      }

      console.log("CHECK SLUG ORIGIN", originPage);
      var url = "/relation/send-request-friend/"+slugAskForPage+"/"+confirm;
      if(typeof originPage != "undefined") url += "/" + originPage

      
      $.ajax({
        type: "POST",
        url: url,
        // data: data,
        success: function(res){
          console.log("response of /relation/send-request-friend/"+slugAskForPage+"/"+confirm, res);
          if(confirm == true){
            if(typeof res.notif != "undefined" && res.notif != "")
              Chat.broadcastNotification(res.notif);
              toastr.success(t.t("Request sent !"));
              //$("#modal-quickview").modal("hide");
          }else{
            $("#modal-quickview #modal-content").html(res);
            //$("#modal-quickview").modal("show");
            nexxo.initBtnRequest();
          }
        },
        error: function(error){
          console.log("btn-send-request-friend error", error);
        }
      });
    });

    $(".btn-send-request-admin").off().click(function(){
      console.log("test");
      var slugAskForPage = $(this).data("slug-page");
      var confirm = (typeof $(this).data("slug-page-confirm") != "undefined") ? $(this).data("slug-page-confirm") : 'false';
      console.log("confirm", typeof confirm);
      
      if(confirm == "false") {
        $("#modal-quickview #modal-content").html(
        "<h5 class='padding-15 no-margin'><i class='fa fa-spin fa-circle-o-notch'></i> " + t.t("Loading ...")+"</h5>");
        //$("#modal-quickview").modal("show");
      }

      $.ajax({
        type: "POST",
        url: "/relation/send-request-admin/"+slugAskForPage+"/"+confirm,
        // data: data,
        success: function(res){
          console.log("response of /relation/send-request-admin/"+slugAskForPage+"/"+confirm);
          if(confirm == true) {
            if(typeof res.notif != "undefined" && res.notif != "")
              Chat.broadcastNotification(res.notif);
            
            //$("#modal-quickview").modal("hide");
            toastr.success(t.t("Request sent !"));
          }else{
            $("#modal-quickview #modal-content").html(res);
            nexxo.initBtnRequest();
          }
        },
        error: function(error){
          console.log("btn-send-request-admin error", error);
        }
      });
    });

    $(".btn-send-request-follow").off().click(function(){
      var button = $(this);
      var slugAskForPage = $(this).data("slug-page");
      var confirm = (typeof $(this).data("slug-page-confirm") != "undefined") ? $(this).data("slug-page-confirm") : 'false';
      console.log("confirm", typeof confirm);

      if(confirm == "false") {
        $("#modal-quickview #modal-content").html(
        "<h5 class='padding-15 no-margin'><i class='fa fa-spin fa-circle-o-notch'></i> " + t.t("Loading ...")+"</h5>");
        //$("#modal-quickview").modal("show");
      }

      $.ajax({
        type: "POST",
        url: "/relation/send-request-follow/"+slugAskForPage+"/"+confirm,
        // data: data,
        success: function(res){
          console.log(" +addClass response of /relation/send-request-follow/"+slugAskForPage+"/"+confirm);
          if(typeof res.error == "undefined"){
            if(confirm == 'false')
            $("#modal-quickview #modal-content").html(res);
            //else
                //$("#modal-quickview").modal("hide");
          }else{
            var newsId = button.data("news-id");
            if(res.action == "add"){
              toastr.success(t.t("You follow this page."));
              nexxo.increaseCountRelation(slugAskForPage, "follow");
              $(".profile-menu .btn-send-request-follow").addClass("letter-blue");
              $(".fa-link[data-slug-page='"+slugAskForPage+"']").addClass("letter-blue");
            }else{
              toastr.success(t.t("You don't follow this page anymore."));
              nexxo.decreaseCountRelation(slugAskForPage, "follow");
              $(".profile-menu .btn-send-request-follow").removeClass("letter-blue");
              $(".fa-link[data-slug-page='"+slugAskForPage+"']").removeClass("letter-blue");
            }

            if(typeof res.notif != "undefined" && res.notif != "")
              Chat.broadcastNotification(res.notif);
            //console.log("button ???", button);
          }
          nexxo.initBtnRequest();
        },
        error: function(error){
          console.log("btn-send-request-follow error", error);
        }
      });
    });

    $(".btn-send-request-participate").off().click(function(){
      var slugAskForPage = $(this).data("slug-page");
      var confirm = (typeof $(this).data("slug-page-confirm") != "undefined") ? $(this).data("slug-page-confirm") : 'false';
      var btn = $(this);

      if(confirm == "false") {
        $("#modal-quickview #modal-content").html(
        "<h5 class='padding-15 no-margin'><i class='fa fa-spin fa-circle-o-notch'></i> " + t.t("Loading ...")+"</h5>");
        //$("#modal-quickview").modal("show");
      }
      
      console.log("confirm", typeof confirm);
      $.ajax({
        type: "POST",
        url: "/relation/send-request-participate/"+slugAskForPage+"/"+confirm,
        // data: data,
        success: function(res){
          console.log("response of /relation/send-request-participate/"+slugAskForPage+"/"+confirm);
          if(typeof res.error == "undefined"){
            if(confirm == 'false')
              $("#modal-quickview #modal-content").html(res);
            //else
             // $("#modal-quickview").modal("hide");
          }else{
            if(res.action == "add"){
              toastr.success(t.t("You participate to this event."));
              nexxo.increaseCountRelation(slugAskForPage, "participate");
              $(".profile-menu .btn-send-request-participate").addClass("letter-blue");
              if(typeof res.notif != "undefined" && res.notif != "")
                Chat.broadcastNotification(res.notif);
            }else{
              toastr.success(t.t("You don't participate to this event."));
              nexxo.decreaseCountRelation(slugAskForPage, "participate");
              $(".profile-menu .btn-send-request-participate").removeClass("letter-blue");
            }
          }
          nexxo.initBtnRequest();
        },
        error: function(error){
          console.log("btn-send-request-participate error", error);
        }
      });
    });

    $(".btn-send-request-like").off().click(function(){
      var slugAskForPage = $(this).data("slug-page");
      var confirm = (typeof $(this).data("slug-page-confirm") != "undefined") ? $(this).data("slug-page-confirm") : 'false';
      var btn = $(this);
      console.log("confirm", typeof confirm);

      $.ajax({
        type: "POST",
        url: "/relation/send-request-like/"+slugAskForPage+"/"+confirm,
        // data: data,
        success: function(res){
          console.log("response of /relation/send-request-like/"+slugAskForPage+"/"+confirm);
          if(typeof res.error == "undefined" || res.error == true){
            toastr.error(t.t("An error sending your like request"));
          }else{
            if(res.action == "add"){
              toastr.success(t.t("You like this page."));
              nexxo.increaseCountRelation(slugAskForPage, "like");
              btn.addClass("letter-green");
              if(typeof res.notif != "undefined" && res.notif != "")
                Chat.broadcastNotification(res.notif);
            }else{
              toastr.success(t.t("You don't like it anymore."));
              nexxo.decreaseCountRelation(slugAskForPage, "like");
              btn.removeClass("letter-green");
            }
           
          }
          nexxo.initBtnRequest();
        },
        error: function(error){
          console.log("/relation/cancel-like/"+slugAskForPage + " error", error);
        }
      });
    });

    $(".btn-cancel-friend").off().click(function(){
      var slugAskForPage = $(this).data("slug-page");
      var originPage = $(this).data("slug-origin-page");

      var url = "/relation/cancel-friend/"+slugAskForPage;
      if(originPage != "") url += "/" + originPage

      $.ajax({
        type: "POST",
        url: url,
        success: function(res){
          console.log("response of /relation/cancel-friend/"+slugAskForPage);
          if(res.error == false){
            toastr.success(t.t("You are no longer friend/member."));
            nexxo.decreaseCountRelation(slugAskForPage, "friend");
            $(".profile-menu .btn-send-request-friend").removeClass("letter-blue");
            $(".community-friends .item#"+slugAskForPage).remove();
          }
        },
        error: function(error){
          console.log("/relation/cancel-friend/"+slugAskForPage + " error", error);
        }
      });
    });

    $(".btn-cancel-follow").off().click(function(){
      var slugAskForPage = $(this).data("slug-page");
      $.ajax({
        type: "POST",
        url: "/relation/cancel-follow/"+slugAskForPage,
        success: function(res){
          console.log("response of /relation/cancel-follow/"+slugAskForPage);
          if(res.error == false){
            toastr.success(t.t("You don't follow this page anymore."));
            nexxo.decreaseCountRelation(slugAskForPage, "follow");
            $(".profile-menu .btn-send-request-follow").removeClass("letter-blue");
          }
        },
        error: function(error){
          console.log("/relation/cancel-follow/"+slugAskForPage + "  error", error);
        }
      });
    });

    $(".btn-cancel-participate").off().click(function(){
      var slugAskForPage = $(this).data("slug-page");
      $.ajax({
        type: "POST",
        url: "/relation/cancel-participate/"+slugAskForPage,
        success: function(res){
          console.log("response of /relation/cancel-participate/"+slugAskForPage);
          if(res.error == false){
            toastr.success(t.t("You don't participate it anymore."));
            nexxo.decreaseCountRelation(slugAskForPage, "participate");
            $(".profile-menu .btn-send-request-participate").removeClass("letter-blue");
          }
        },
        error: function(error){
          console.log("/relation/cancel-participate/"+slugAskForPage + "  error", error);
        }
      });
    });

    $(".btn-cancel-admin").off().click(function(){
      var slugAskForPage = $(this).data("slug-page");
      $.ajax({
        type: "POST",
        url: "/relation/cancel-admin/"+slugAskForPage,
        success: function(res){
          console.log("response of /relation/cancel-admin/"+slugAskForPage);
          if(res.error == false){
            toastr.success(t.t("You are no longer an administrator of this page."));
            nexxo.decreaseCountRelation(slugAskForPage, "admin");
          }
        },
        error: function(error){
          console.log("/relation/cancel-admin/"+slugAskForPage + "  error", error);
        }
      });
    });

  },


  increaseCountRelation : function(slugAskForPage, typeRequest){
    var i = $(".friend-item-content#"+slugAskForPage+" .count-item-"+typeRequest).html();
    i = parseInt(i); i++;
    $(".friend-item-content#"+slugAskForPage+" .count-item-"+typeRequest).html(i);
  },
  decreaseCountRelation : function(slugAskForPage, typeRequest){
    var i = $(".friend-item-content#"+slugAskForPage+" .count-item-"+typeRequest).html();
    i = parseInt(i); i--;
    $(".friend-item-content#"+slugAskForPage+" .count-item-"+typeRequest).html(i);
  },

  /* -----------------------
   * Progress bars Animation
   * --------------------- */
   progresBars : function () {
        //$('.skills-item').appear({force_process: true});
        $.each($('.skills-item'), function () {
            var current_bar = $(this);
            if (!current_bar.data('inited')) {
                current_bar.find('.skills-item-meter-active').fadeTo(300, 1).addClass('skills-animate');
                current_bar.data('inited', true);
            }
        });
    },


    playSound : function(key){ //console.log("playSound", key, AUTH_NOTIF_SOUND, AUTH_CHAT_SOUND);

      $("#sound-notification")[0].volume=0.5;
      $("#sound-message")[0].volume=0.5;

      if(key == "notification" && AUTH_NOTIF_SOUND == "true")
        $("#sound-notification")[0].play();

      if(key == "message" && AUTH_CHAT_SOUND == "true")
        $("#sound-message")[0].play();
    } 

}