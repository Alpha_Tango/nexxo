
var Sig = {

	//tilesProvider : "//stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png",
	//tilesAttributions : "<a href='http://www.opencyclemap.org'>OpenCycleMap</a>",
	//"//stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}",
	//tilesProvider : '//cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png', 
	//tilesAttributions : '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	
	tilesProvider : '//{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
	tilesAttributions : '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>',

	tilesDarkProvider : '//stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
	tilesDarkAttributions : '<a href="https://www.openstreetmap.org">OSM</a>',

	libIsLoaded : false,
	mapIsLoaded : false,
	
	map : false,
	tileLayer : false,
	markerDrag : false,

	markersLayer : false,
	geoJsonCollection : false,

	defaultCoordinates : [46.13417004624326, 2.377594901489258],

	currentData : new Array(),

	rectangle : false,

	init : function(canvasId, onLoaded){
		if($("#"+canvasId).length < 1) {
			console.log("map canvas doesn't exists : #"+canvasId);
			return false;
		}
		if(Sig.libIsLoaded == false){
			$("#"+canvasId).html("<h6 class='text-center full-width margin-top-50'>"+
									"<i class='fa fa-spin fa-refresh'></i> "+t.t("Map is loading")+
								 "</h6>").removeClass("d-none");
	            
			$.ajax({
	          type: "POST",
	          url: "/app/load-sig-assets",
	          // data: data,
	          success: function(res){
	          	Sig.libIsLoaded = true;
                console.log("ok, asset sig loaded");
	            $("#sig-assets").html(res);
	            if(typeof onLoaded == "function") onLoaded();
	          }
	        });
	    }else{
	    	 if(typeof onLoaded == "function") onLoaded();
	    }
	},


	loadMap : function(canvasId, params){

		if(Sig.mapIsLoaded == true && $("#"+canvasId).html() != ""){
            console.log("map already loaded");
			return;
		}
		console.warn("--------------- loadMap ---------------------");
		
		$("#"+canvasId).html("");
		$("#"+canvasId).show(200);
		$("#"+canvasId).css({"background-color": this.mapColor});

		console.log("sig params", params);
		if(canvasId != ""){

			var initCenter = typeof params != "undefined" && typeof params.center != "undefined"
							 ? params.center :  Sig.defaultCoordinates;

			var zoom = typeof params != "undefined" && typeof params.zoom != "undefined"
							 ? params.zoom :  4;

			var maxZoom = typeof params != "undefined" && typeof params.maxZoom != "undefined"
							 ? params.maxZoom :  14;

			var scrollWheelZoom = typeof params != "undefined" && typeof params.scrollWheelZoom != "undefined"
							 ? params.scrollWheelZoom :  false;

			var zoomControl = typeof params != "undefined" && typeof params.zoomControl != "undefined"
							 ? params.zoomControl : true;

			var options = { "zoomControl" : zoomControl,
							"scrollWheelZoom":scrollWheelZoom,
							"center" : initCenter,
							"zoom" : zoom,
							"maxZoom" : maxZoom,
							"minZoom" : 3,
							"worldCopyJump" : false };

			var tilesProvider = this.tilesProvider;
			var tilesAttributions = this.tilesAttributions;
			var opacity = 1;
			if($("body").hasClass("dark-theme")){
				tilesProvider = this.tilesDarkProvider;
				tilesAttributions = this.tilesDarkAttributions;	
				opacity = 0.5;
			}

			console.log("GO!");
			var map = L.map(canvasId, options);console.log("GO2!");
			Sig.map = map;console.log("GO3!");
			Sig.tileLayer = L.tileLayer(tilesProvider, { 
				attribution: '' + tilesAttributions, //'Map tiles by <a href="http://stamen.com">Stamen Design</a>',
				subdomains: 'abc',
				zIndex:1,
				minZoom: options.minZoom,
				maxZoom: options.maxZoom
			});

			Sig.tileLayer.addTo(map).setOpacity(opacity);//.setOpacity(initParams.mapOpacity);
			
		}
		
		//rafraichi les tiles après le redimentionnement du mapCanvas
		//map.invalidateSize(false);
		this.mapIsLoaded = true;
		return map;
	},

	getMarkerIcon : function(type){
		/*var iconUrl = "/js/sig/leaflet/images/marker-icon.png";
		if(type != "") iconUrl = "/js/sig/leaflet1.3.3/images/"+type+".png";
		return L.icon({
				    iconUrl: iconUrl,
				    iconSize: [53, 60], //38, 95],
				    iconAnchor: [27, 57],//22, 94],
				    popupAnchor: [0, -55]//-3, -76] 
				});*/
		//return L.AwesomeMarkers.icon({icon: ico + " fa-" + color, iconColor:color, prefix: 'fa' });
		var faIcon = { 	'user': 'user',
						'association': 'group',
						'freegroup': 'circle',
						'business': 'industry',
						'project': 'lightbulb-o',
						'event': 'calendar',
						'assembly': 'sun-o',
						'classified': 'thumb-tack',
						};

		var faColor = { 'user': 'orange',
						'association': 'green',
						'freegroup': 'blue',
						'business': 'lightblue',
						'project': 'purple',
						'event': 'cadetblue',
						'assembly': 'orange',
						'classified': 'orange',
						};

		var icon = faIcon[type];
		var color = faColor[type];
		return L.AwesomeMarkers.icon({ icon: icon, prefix: 'fa', markerColor:color, iconColor: 'white' });
	},


	callNominatim : function(countryCode, city, street){
		console.log("callNominatim", typeof countryCode, typeof city, typeof street);
		
		var request = countryCode != "" ? "/"+ countryCode : "";
		if(countryCode != "" && city != ""){
			request += "/";
			request += city != "" ? city : "";
		}
		if(city != "" && street != ""){
			request += "/";
			request += street != "" ? street : "";
		}

		console.log("nominatim request", request, countryCode, city, street);

		$.ajax({
				url: "//nominatim.openstreetmap.org/search" + request + "?format=json&polygon=0&addressdetails=1",
				type: 'GET',
				dataType: 'json',
				async:true,
				crossDomain:true,
				timeout: 10000,
				complete: function () {},
				success: function (obj){
					console.log(obj);
					var html = "";
					var nbRes = 0;
				
					$.each(obj, function(key, val){
						if((street == "" && typeof val.address.city != "undefined") || (street != "") ){
							//if(val.type == "administrative" || val.type == "city"){ 
							nbRes++;
							html += '<li class="inline-items">'+
										'<div class="col-sm-8 no-padding">'+
											'<b class="elipsis"><i class="fa fa-map-marker"></i> '+val.display_name+'</b>'+
										'</div>'+

										'<div class="col-sm-4 no-padding">'+
											'<a href="#map-container" class="btn btn-success btn-sm float-right no-margin btn-select-address" '+
													 'data-lat="'+val.lat+'" data-lon="'+val.lon+'">'+
												'<i class="fa fa-check"></i> <i class="fa fa-map-marker"></i> '+t.t("select")+
											'</a>'+
										'</div>'+

									'</li>';
							//}
						}
					});

					if(nbRes == 0){
						html =  '<li class="inline-items">'+
									'<div class="notification-event">'+
										'<a href="#map-container" class="h6 notification-friend elipsis">'+
											'<i class="fa fa-ban"></i> aucun résultat'+
										'</a>'+
									'</div>'+
								'</li>';
					}

					$('#geoloc-result').html(html).show();
					$(".btn-select-address").click(function(){
		                var lat = $(this).data("lat");
		                var lon = $(this).data("lon");
		                Sig.startGeoloc(lat, lon);
		            });
				},
				error: function (thisError) {
					console.log(thisError);
					$("#geoloc-result").html('<li class="inline-items">'+
		                                        '<div class="col-sm-8 no-padding">'+
		                                            '<i class="fa fa-ban"></i> '+
		                                            '<b>Une erreur est survenue pendant la recherche d\'addresse.<br>'+
		                                            'Merci de positionner votre page manuellement.'+
		                                            '</b>'+
		                                        '</div>'+
		                                     '</li>');
					Sig.startGeoloc(Sig.defaultCoordinates[0], Sig.defaultCoordinates[1], 5);
				}
			});
	},

	startGeoloc : function(lat, lon, zoom){
		var canvasId = "map-container";
        if(typeof zoom == "undefined") zoom = 14;
        Sig.init(canvasId, function(){
            Sig.loadMap(canvasId, {
                center : [lat, lon],
                zoom: zoom,
                maxZoom: 17
            });
            $("#map-container").removeClass("d-none");
            var coordinates = [lat, lon];
            var type = $("#page_type").length ? $("#page_type").val() : "user";
            console.log("type", type);
            var icon = Sig.getMarkerIcon(type);
            var markerOptions = { icon : icon };

            if(Sig.markerDrag != false){
                console.log("removeLayer");
                Sig.map.removeLayer(Sig.markerDrag);
            }

            Sig.markerDrag = L.marker(coordinates, markerOptions)
                            .addTo(Sig.map);

            Sig.markerDrag.bindPopup("<div class='text-center'>"+
            							"<b>"+t.t("Move this marker to the position you want")+"</b>"+
            							"<button class='btn bg-green margin-top-10' id='btn-validate-position'>"+
            								"<i class='fa fa-check'></i> "+t.t("Save this position")+
            							"</button>" +
            						 "</div>")
                            .openPopup()
                            .dragging.enable();

            $("#page_latitude").val(Sig.markerDrag.getLatLng().lat);
            $("#page_longitude").val(Sig.markerDrag.getLatLng().lng);

            Sig.markerDrag.on('dragend', function(e){
                this.openPopup();  
                $("#page_latitude").val(this.getLatLng().lat);
                $("#page_longitude").val(this.getLatLng().lng);

	            $(".app-search #btn-validate-position, .app-agenda #btn-validate-position").click(function(){
	            	var lat = $("#page_latitude").val();
	            	var lng = $("#page_longitude").val();
	            	var coordinates = new Array(lng, lat);
	            	pageObj.saveAddressPosition(
	            				 $("#page_country").val(),
	            				 $("#page_city").val(),
	            				 $("#page_streetAddress").val(),
	            				 coordinates
	            			);
	            });
            });

            $(".app-search #btn-validate-position, .app-agenda #btn-validate-position").click(function(){
            	var lat = $("#page_latitude").val();
            	var lng = $("#page_longitude").val();
            	var position = new Array(lat, lng);
            	pageObj.saveAddressPosition(
            				 $("#page_country").val(),
            				 $("#page_city").val(),
            				 $("#page_streetAddress").val(),
            				 position
            			);
            });


            //Sig.markerDrag = marker;
        });
	},

	showDataOnMap : function(datas, fitBounds=true){ console.log("showDataOnMap", datas);

		if(Sig.markersLayer != false){
            console.log("removeLayer");
            Sig.map.removeLayer(Sig.markersLayer);
            Sig.currentData = new Array();
        }

		this.markersLayer = L.markerClusterGroup({"maxClusterRadius" : 60});
		this.map.addLayer(Sig.markersLayer);
		
		//collection de marker geojson
		this.geoJsonCollection = { type: 'FeatureCollection', features: new Array() };
		var nbMarker = 0;
		$.each(datas, function(key, data){ 
			if(data.coordinates != null && data.coordinates[0] != 0 && data.coordinates[0] != false){ nbMarker++;
				console.log(data);
				var type = typeof data.type != "undefined" ? data.type : "";
				var icon = Sig.getMarkerIcon(type);
            	var markerOptions = { icon : icon };

				var marker = L.marker(new L.LatLng(data.coordinates[1], data.coordinates[0]), markerOptions);

				Sig.currentData[data.id] = { data : data, marker, marker };

				var description = data.description != null ? data.description.substr(0, 300) : "";
				if(data.description != null && data.description.length > 300) description+="...";

				var address = data.city != null ? data.city : "";
				address += data.streetAddress != null ? " " + data.streetAddress : "";

				var html = '<div style="width:300px;" class="d-flex">'+
								'<div class="no-padding">';
					
					if(data.type == "classified" && data.imageProfil == "default.png")
					html+=			'<img src="/uploads/imgProfil/default_classified.png" height="30">';
					else
					if(data.slug != "noslug" || data.imageProfil == "default.png")
					html+=			'<img src="/uploads/imgProfil/'+data.imageProfil+'" height="30">';
					else html+=			'<img src="'+data.imageProfil+'" height="30">';
					
					html+=		 '</div>';
					html+=		 '<div class="col-10">'+
									'<div class="h6 no-margin author-name">'+data.name+'</div>';

							if(address != "")
							html+=	'<div class="author-name"><small><i class="fa fa-map-marker"></i> '+address+'</small></div>';

							html+=	'<label class="page-description">'+description+'</label><br>';

							if(data.slug != "noslug"){
							html+=	'<button class="btn btn-blue full-width open-quickview" data-type="page" data-id="'+data.slug+'" '+
											 'data-toggle="modal" data-target="#modal-quickview"'+'>'+
										t.t('Quick view')+
									'</button>';
							}else{
								html+=	'<a href="'+data.website+'" target="_blank" class="btn btn-sm btn-blue letter-white full-width">'+
											'<i class="fa fa-reply fa-rotate-180"></i> ' + t.t('Website')+
										'</a>';
							}

							html+= '</div>'+
							'</div>';

				marker.bindPopup(html);
				marker.on("popupopen", function(){
					$(".open-quickview").click(function(){
						nexxo.openQuickView($(this).data("type"), $(this).data("id"));
					});
				});

					
				
				Sig.markersLayer.addLayer(marker);
			}
		});

		if(nbMarker > 0 && fitBounds == true)
		setTimeout(function(){ 
			Sig.map.fitBounds(Sig.markersLayer.getBounds(), { 'maxZoom' : 13, 'animate':false });
		}, 300);

		
	},

	showNewsStremOnMap : function(datas, fitBounds=true){ console.log("showDataOnMap", datas);

		if(Sig.markersLayer != false){
            console.log("removeLayer");
            Sig.map.removeLayer(Sig.markersLayer);
            Sig.currentData = new Array();
        }

		this.markersLayer = L.markerClusterGroup({"maxClusterRadius" : 60});
		this.map.addLayer(Sig.markersLayer);
		
		//collection de marker geojson
		this.geoJsonCollection = { type: 'FeatureCollection', features: new Array() };
		var nbMarker = 0;
		$.each(datas, function(key, data){ 
			if(data.scopeGeo.origin != null && data.scopeGeo.origin != 0) {
			//data.coordinates != null && data.coordinates[0] != 0 && data.coordinates[0] != false){ 
				nbMarker++;
				console.log(data);
				var type = typeof data.type != "undefined" ? data.type : "";
				var icon = Sig.getMarkerIcon(type);
            	var markerOptions = { icon : icon };

				var marker = L.marker(new L.LatLng(data.scopeGeo.origin[1], data.scopeGeo.origin[0]), markerOptions);

				Sig.currentData[data.id] = { data : data, marker, marker };

				var description = data.description != null ? data.description.substr(0, 300) : "";
				if(data.description != null && data.description.length > 300) description+="...";

				var address = data.city != null ? data.city : "";
				address += data.streetAddress != null ? " " + data.streetAddress : "";

				var html = '<div style="width:300px;" class="d-flex">'+
								'<div class="no-padding">';
					if(data.slug != "noslug" || data.imageProfil == "default.png")
					html+=			'<img src="/uploads/imgProfil/'+data.imageProfil+'" height="30">';
					else
					html+=			'<img src="'+data.imageProfil+'" height="30">';

					html+=		 '</div>';
					html+=		 '<div class="col-10">'+
									'<div class="h6 no-margin author-name">'+data.name+'</div>';

							if(address != "")
							html+=	'<div class="author-name"><small><i class="fa fa-map-marker"></i> '+address+'</small></div>';

							html+=	'<label>'+description+'</label><br>';

							if(data.slug != "noslug"){
							html+=	'<button class="btn btn-blue full-width open-quickview" data-type="page" data-id="'+data.slug+'" '+
											 'data-toggle="modal" data-target="#modal-quickview"'+'>'+
										t.t('Quick view')+
									'</button>';
							}else{
								html+=	'<a href="'+data.website+'" target="_blank" class="btn btn-sm btn-blue letter-white full-width">'+
											'<i class="fa fa-reply fa-rotate-180"></i> ' + t.t('Website')+
										'</a>';
							}

							html+= '</div>'+
							'</div>';

				marker.bindPopup(html);
				marker.on("popupopen", function(){
					$(".open-quickview").click(function(){
						nexxo.openQuickView($(this).data("type"), $(this).data("id"));
					});
				});

					
				
				Sig.markersLayer.addLayer(marker);
			}
		});

		if(nbMarker > 0 && fitBounds == true)
		setTimeout(function(){ 
			Sig.map.fitBounds(Sig.markersLayer.getBounds(), { 'maxZoom' : 13, 'animate':false });
		}, 300);

		
	},

	showSingleMarker : function(coordinates, type){
		var icon = Sig.getMarkerIcon(type);
    	var markerOptions = { icon : icon };
		var marker = L.marker(new L.LatLng(coordinates[1], coordinates[0]), markerOptions);
		Sig.map.addLayer(marker);
		return marker;
	},

	showRectangle : function(bounds){
		if(Sig.rectangle != false){
            console.log("removeLayer");
            Sig.map.removeLayer(Sig.rectangle);
        }

        var rectangle = L.rectangle(bounds, {color: "#ff7800", weight: 1});
        Sig.map.addLayer(rectangle);
        return rectangle;
	},

	showCircle : function(coordinates, radius){
		console.log("coords", coordinates);
		if(Sig.markersLayer != false){
            console.log("removeLayer");
            Sig.map.removeLayer(Sig.markersLayer);
        }
		var circle = L.circle(new L.LatLng(coordinates[1], coordinates[0]), 
								{ 	color: '#48b7e9',
								    fillColor: '#48b7e9',
								    fillOpacity: 0.2,
								    radius : radius });//.addTo(Sig.map);
		Sig.map.addLayer(circle);
		return circle;
	},

	showCirclePolygon : function(coordinates, radius){
		var circle = L.circle(new L.LatLng(coordinates[1], coordinates[0]), { radius : radius });
		var circlePolygon = L.Circle.toPolygon(currentCircleMarker, 50, Sig.map);
		var poly = L.polygon(circlePolygon, {color: '#48b7e9',
										    fillColor: '#48b7e9',
										    fillOpacity: 0.1}).addTo(Sig.map);
		Sig.map.addLayer(poly);
		return poly;
	},

	openMarkerById : function(id){
		if(typeof Sig.currentData[id] == "undefined") return false;

		console.log("openMarkerById", Sig.currentData[id]["marker"]);
		
		var marker = Sig.currentData[id]["marker"];
		var data = Sig.currentData[id]["data"];
		console.log("openMarkerById", data);
		//Sig.map.setZoom(9);
		//Sig.map.panTo(new L.LatLng(data.coordinates[1], data.coordinates[0]));
		//marker.fire("click");//openPopup();
		var visibleOne = null;
		if(typeof marker != "undefined"){
			visibleOne = Sig.markersLayer.getVisibleParent(marker);
			//console.log("visibleOne._bounds", visibleOne._bounds);
			if(visibleOne != null && typeof visibleOne._bounds != "undefined"){
				Sig.map.fitBounds(visibleOne._bounds);
				setTimeout(function(){ 
							Sig.map.setView(new L.LatLng(data.coordinates[1], data.coordinates[0]), Sig.map.getZoom(), {"animate" : false });
							setTimeout(function(){ marker.fire("click"); }, 500);
							},1200);
			}else{
				if(visibleOne != null)
					visibleOne.fire("click");
				else{
					Sig.map.setView(new L.LatLng(data.coordinates[1], data.coordinates[0]), Sig.map.getZoom(), {"animate" : false });
					setTimeout(function(){ marker.fire("click"); }, 500);
				}
			}
		}
		var coordinates;
		console.log("visibleOne", visibleOne);
		/*if(visibleOne != null){
			if(typeof visibleOne._childCount != "undefined"){
				var i = 0;
				while(typeof visibleOne._childCount != "undefined" && i<5){
					coordinates = visibleOne.getLatLng();
					//Sig.map.panTo(coordinates, {"animate" : false });
					visibleOne.fire("click");
					if(typeof marker != "undefined")
						visibleOne = Sig.markersLayer.getVisibleParent(marker);
					//thisSig.currentParentToOpen = visibleOne;
					i++;
				}
				console.log(i);
			}
			else{
				if(typeof visibleOne._spiderLeg == "undefined")	{
					Sig.map.fire("click");
					Sig.map.setZoom(15, {"animate" : false });
					//Sig.map.panTo(coordinates, {"animate" : false });
					//thisSig.currentParentToOpen = null;

				}
			}
		}*/
		
	}

};


