var searchObj = {

	myPosition : new Array(0, 0),
	myMarker : false,

	currentRadius : 10000,
	currentKm : 10,
	currentCircleMarker : false,

	typesSelected : "",

	canvasId : "map-container",

	renderPartial : "true",

	baseUrlSearch : 'search',


	getKm : function(dist){
		if(dist < 5) dist = 0.2*dist;
		if(dist >= 5 && dist < 10) dist = 0.5*dist;
		if(dist > 50 && dist < 60) dist = dist*2;
		if(dist >= 60 && dist < 70) dist = dist*3;
		if(dist >=70 && dist < 80) dist = dist*4;
		if(dist >=80 && dist <= 100) dist = dist*5;
		if(dist == 0) dist = 0.1;
		
		return parseFloat(dist).toFixed(1);
	},

	initUi : function(myPosition){
		searchObj.myPosition = myPosition;
		Sig.mapIsLoaded = false;

		console.log("myPosition", searchObj.myPosition);
        $("#formControlRange").val(searchObj.currentKm);
		$("#formControlRange").change(function(){
			var dist = parseInt($(this).val());
			var km = searchObj.getKm(dist);

			searchObj.currentRadius = km*1000;
			searchObj.currentKm = km;
			$(".around-me-distance").html(km + " km");

			console.log('range change', searchObj.currentRadius, searchObj.currentKm);
			
			searchObj.showCircle();

            if(searchObj.interop != true) //no refresh if interop, all data are already loaded
				searchObj.globalSearch();
		});

		

		$("#check-search-aroundme").prop('checked', false);
        $("#check-search-aroundme").change(function(){
          console.log( $("#check-search-aroundme").is(':checked'));
          if($("#check-search-aroundme").is(':checked')){
            searchObj.showCircle();
            $(".widget-aroundme").removeClass("d-none");
            $(".app-search").addClass("sig-open"); console.log("searchObj.results", searchObj.results);
           
			$("#btn-show-map").addClass("d-none");
			$("#btn-hide-map").removeClass("d-none");

            setTimeout(function(){ Sig.showDataOnMap(searchObj.results, false); },1000);

          }else{
            $(".widget-aroundme").addClass("d-none");
            if(searchObj.currentCircleMarker != false){
	            	Sig.map.removeLayer(searchObj.currentCircleMarker);
	            	searchObj.currentCircleMarker = false;
	        }
            searchObj.myMarker.dragging.disable();
            searchObj.myMarker.off();
          }

          if(searchObj.interop != true) //no refresh if interop, all data are already loaded
          	searchObj.globalSearch();

          nexxo.initSwiper();
        });

        $("#btn-refresh-search").click(function(){
			searchObj.globalSearch();
		});

		$(".btn-type-page").click(function(){
			if($(this).hasClass("selected")){
				$(this).removeClass("selected");
			}else{
				$(this).addClass("selected");
			}
			$("#type-search-selected").html(searchObj.typesSelected);
			searchObj.globalSearch();
		});

        $("#search-in-page").off().change(function(){
            //console.log("location : ", location);
            $("#search-result-list").html("<i class='fa fa-spin fa-refresh'></i>");
            searchObj.globalSearch(); 
        });

        $("#search-in-page").keyup(function(){
        	$(".js-user-search").val($(this).val());
        });
        $(".js-user-search").keyup(function(){
        	$("#search-in-page").val($(this).val());
        });
        $("#search-in-page").val($(".js-user-search").val());

        $("#btn-share-search").click(function(){
        	if($("#search-url").hasClass("d-none")){
				$("#search-url").removeClass("d-none").addClass("d-inline");
			}else{
				$("#search-url").addClass("d-none").removeClass("d-inline");
			}
        });

        $("#btn-select-all-types").click(function(){
        	$(".btn-type-page").removeClass("selected");
        	searchObj.globalSearch(); 
        });

        $(".btn-open-help-intro").click(function(){
        	$(".help-intro").toggleClass("d-none");
        });

        $("#link-around").click(function(){
        	$("#check-search-aroundme").click();
        });

        $("#btn-show-input-create-channel").click(function(){
        	$("#div-create-channel").toggleClass("d-none");
        });

        $("#btn-create-channel").click(function(){
        	var url = $("#input-create-channel").val();
        	if(url != ""){
        		$("#lbl-msg-import-channel").html("<i class='fa fa-spin fa-refresh'></i> " + t.t("Loading channel")).
        									 addClass("letter-green").removeClass("letter-red");
	        	$.ajax({
		          type: "GET",
		          url: "/mediatheque/import-channels",
		          data: { "url": url },
		          success: function(res){
		          	if(res.error == false) {
		          		toastr.success(t.t(res.msg));
		          		$("#lbl-msg-import-channel").html("<i class='fa fa-check'></i> " + t.t(res.msg)).addClass("letter-green").removeClass("letter-red");
		          		$("#lbl-msg-import-channel").append("<br>"+
		          											t.t("You can now access to the new page here") +
		          											" : <a href='/page/"+res.slug+"' target='_blank'>"+res.name+"</a>"+
		          											"<br><small>"+t.t("if you are the owner of this channel, you can send a request to become admin of this new page") + "</small>"
		          											);
		          		searchObj.globalSearch(); 
		          	}
		          	else{
		          		toastr.error(t.t(res.msg));
		          		$("#lbl-msg-import-channel").html("<i class='fa fa-exclamation-triangle'></i> " + t.t(res.msg)).addClass("letter-red").removeClass("letter-green");
		          	}
		          },
		          error: function(error){
		          		console.log("res error /mediatheque/import-channels", error);
		          }
		        });
	        }
        });
        
        searchObj.initBtnOpenMarker();
        searchObj.initPreSearchUi();
        searchObj.initLocaliseMe();
	},

	initLocaliseMe : function(){
		$("#btn-init-localise-me").click(function(){
			$(".widget-localiseMe").removeClass("d-none");
		});

		$("#page_city, #page_streetAddress").keyup(function(e){
			var code = e.which; // recommended to use e.which, it's normalized across browsers
			if(code==13){
        		e.preventDefault();
	            $('.dropdown-menu').html('<li><a href="#address-block" class="dropdown-item">'+
	                                     '<i class="fa fa-spin fa-refresh"></i></a></li>').show();
	            var countryCode = $("#page_country").val();
	            var city = $("#page_city").val();
	            var street = $("#page_streetAddress").val();
	            $(".geoloc-result").removeClass("d-none");
	            $("#geoloc-result").html('<li class="inline-items">'+
	                                        '<div class="col-sm-8 no-padding">'+
	                                            '<i class="fa fa-spin fa-refresh"></i> '+
	                                            '<b>'+t.t("Looking for the position")+'</b>'+
	                                        '</div>'+
	                                     '</li>');

	            Sig.callNominatim(countryCode, city, street);
	            $(".app-search").addClass("sig-open");
	        }

        });

        $("#check-user-geoloc").prop('checked', false);
        $("#check-user-geoloc").change(function(){
          console.log( $("#check-user-geoloc").is(':checked'));
          if($("#check-user-geoloc").is(':checked')){
            var countryCode = $("#page_country").val();
            var city = $("#page_city").val();
            var street = $("#page_streetAddress").val();
            $(".geoloc-result").removeClass("d-none");
            $("#geoloc-result").html('<li class="inline-items">'+
                                        '<div class="col-sm-8 no-padding">'+
                                            '<i class="fa fa-spin fa-refresh"></i> '+
                                            '<b>'+t.t("Looking for the position")+'</b>'+
                                        '</div>'+
                                     '</li>');

            Sig.callNominatim(countryCode, city, street);
            
          }else{

          }
        });
	},

	showCircle : function(){
    	if(searchObj.myPosition != false){
			console.log("myPos", searchObj.myPosition);
			Sig.init(searchObj.canvasId, function(){
	        	$("#"+searchObj.canvasId).removeClass("d-none");
	        	//enable zoom by scrolling if not on mobil device
	        	var scrollWheelZoom = ($("body").width() < 767) ? false : true;
	            Sig.loadMap(searchObj.canvasId, {
	                center : new L.LatLng(searchObj.myPosition[1], searchObj.myPosition[0]),
	                zoom: 10,
	                maxZoom: 14,
	                zoomControl: true,
	                scrollWheelZoom : scrollWheelZoom
	            });

	            if(searchObj.myMarker != false)
	            	Sig.map.removeLayer(searchObj.myMarker);

	            searchObj.myMarker = Sig.showSingleMarker(searchObj.myPosition, "default");

	            searchObj.myMarker.bindPopup("<div class='text-center'>"+
		            							"<b>"+t.t("Move your marker to the position you want")+"</b>"+
		            						 "</div>")
	            					.dragging.enable();
	            searchObj.myMarker.openPopup();
	            searchObj.myMarker.on('dragend', function(e){
	                //this.openPopup();  
	                searchObj.myPosition = [this.getLatLng().lng, this.getLatLng().lat];
	                searchObj.showCircle();
	                searchObj.globalSearch();
	            });

	            if(searchObj.currentCircleMarker == false){
	            	searchObj.currentCircleMarker = Sig.showCircle(searchObj.myPosition, searchObj.currentRadius);
	            	console.log("searchObj.currentCircleMarker", searchObj.currentCircleMarker);
	            }else{
	            	searchObj.currentCircleMarker.setLatLng([searchObj.myPosition[1], searchObj.myPosition[0]]).setRadius(searchObj.currentRadius);
	            }
	            if(searchObj.currentCircleMarker != false)
	            	Sig.map.fitBounds(searchObj.currentCircleMarker.getBounds(), { 'maxZoom' : 13, 'animate':false });
		
	        });
		}
    },

    showMap : function(afterLoad){ 
        Sig.init(searchObj.canvasId, function(){ //console.log("call showMap xxx");
        	$("#"+searchObj.canvasId).removeClass("d-none");
            $(".app-search").addClass("sig-open");

            var scrollWheelZoom = ($("body").width() < 767) ? false : true;
	        Sig.loadMap(searchObj.canvasId, {
                center : [46.13417004624326, 2.377594901489258],
                zoom: 5,
                maxZoom: 14,
                scrollWheelZoom: scrollWheelZoom
            }); 
            Sig.showDataOnMap(searchObj.results);

            if(afterLoad != null)
            	afterLoad();
            
        });
    },

    hideMap : function(){ console.log("call hideMap");
    	$("#"+searchObj.canvasId).addClass("d-none");
        $(".app-search").removeClass("sig-open");
	},

    initListResUi : function(){
    	$("#btn-show-map").off().click(function(){
            searchObj.showMap();
            $(this).addClass("d-none");
            $("#btn-hide-map").removeClass("d-none");
        });
    	$("#btn-hide-map").off().click(function(){
            searchObj.hideMap();
            $(this).addClass("d-none");
            $("#btn-show-map").removeClass("d-none");
        });
        $(".open-quickview").off().click(function(){
			nexxo.openQuickView($(this).data("type"), $(this).data("id"));
		});
        nexxo.initBtnRequest();
        if(typeof searchObj.results != "undefined"){
        	if(parseInt($(".res-total-count").html()) > $(".friend-item").length)
        		$("#result-length").html($(".friend-item").length + " / " + $(".res-total-count").html());
        	else 
        		$("#result-length").html(searchObj.results.length);
    	}
    },

    /* globalSearch use all parameters available */
    globalSearch : function(){
    	var searchTxt = $(".js-user-search").val();
		if(searchTxt == "" || typeof(searchTxt) == "undefined") searchTxt = $("#search-in-page").val();
		searchTxt = searchTxt.replace(/\#/g, "><");
		searchTxt = searchTxt.replace('?', "");
		console.log("search", searchTxt);
		if(searchTxt == "") searchTxt = "-";

		var geo = "";
		if($("#check-search-aroundme").is(':checked'))
			geo = "lat="+searchObj.myPosition[1]+"&lon="+searchObj.myPosition[0]+"&radius="+searchObj.currentKm;
	
		var types = "";
		$.each($(".btn-type-page"), function(key, btnType){
			if($(btnType).hasClass("selected")){
				if(types != "") types += ",";
				types += $(btnType).data("type-name");
			}
		});

		if(types != ""){
			if(geo != "") types = "&types=" + types;
			else		  types =  "types=" + types;
		}

		var dates = "";
		if($('#calendar').length == 1){
			var startDate = $('#calendar').fullCalendar("getView").start.format("YYYY-MM-DD");
			var endDate = $('#calendar').fullCalendar("getView").end.format("YYYY-MM-DD");
			dates = "/"+startDate+"/"+endDate;
		}

		var getParams = (geo+types != "") ? "?" + geo + types : "?";

		if($(".btn-show-calendar.active").data("type") == "personal")
			getParams += "myagenda=true";

		
		var url = "/"+searchObj.baseUrlSearch+"/" + searchTxt + "/" + searchObj.renderPartial + dates + getParams;
		var urlShare = "/"+searchObj.baseUrlSearch+"/"+searchTxt+getParams;
		var urlShareIframe = "/map-embed/"+searchTxt+getParams;
		if(searchTxt == "-") urlShare = "/"+searchObj.baseUrlSearch+"/"+getParams;
		
		if(searchObj.interop == true){
			var bounds = "false";
			if(searchObj.currentCircleMarker != false){
				bounds = searchObj.currentCircleMarker.getBounds();
				var boundsStr = "&bounds=" + 
								bounds.getSouthWest().lng.toString().substring(0,8) + "%2C" + 
								bounds.getSouthWest().lat.toString().substring(0,8) + "%2C" + 
								bounds.getNorthEast().lng.toString().substring(0,8) + "%2C" + 
								bounds.getNorthEast().lat.toString().substring(0,8); 
				//Sig.rectangle = Sig.showRectangle(bounds);

				console.log("found bounds", bounds, boundsStr);
			}
			url = "/in-transition/" + searchTxt + "/" + searchObj.renderPartial + getParams + boundsStr;
			urlShare = url;
		}

		console.log("url complet", url);

		$("#search-result-list").html("<i class='fa fa-spin fa-circle-o-notch'></i> " + t.t("Loading ..."));

		$.ajax({
          type: "POST",
          url: url,
          success: function(res){
          	if(searchObj.renderPartial == "true"){
	          	$("#search-result-list").html(res);
	          	//init btn to show each result on the map
	        	searchObj.initBtnOpenMarker();
	          	//Activate the Tooltips
	    		$('#search-result-list [data-toggle="tooltip"], [rel="tooltip"]').tooltip();
	    		$("#search-url").val(nexxo.baseUrl + urlShare);

	            $("#share-iframe").val("<iframe  src=\"" + nexxo.baseUrl + urlShareIframe + "\" "+
	            								"height='500' width='500'>"+
	            						"</iframe>");

	            if(Sig.mapIsLoaded == true)
					Sig.showDataOnMap(searchObj.results);
				
				nexxo.initSwiper();
	            console.log("global search ok", url);
	        }else{
	        	if(searchObj.renderPartial == "json"){
	            console.log("searchObj.renderPartial", searchObj.renderPartial);
	        		if(searchObj.typesSelected == "event"){
	            		console.log("searchObj.typesSelected", searchObj.typesSelected);
	        			agendaObj.showData(res.jsonRes);
	        			searchObj.results = res.jsonRes;
	        			if(Sig.mapIsLoaded == true)
							Sig.showDataOnMap(res.jsonRes);
	        		}
	        	}
	        }
          }
        });
    },

    initPreSearchUi : function(){
    	if(searchObj.latSearch != false && searchObj.lngSearch != false && searchObj.radiusSearch != false){
    		//searchObj.myPosition = new Array(searchObj.latSearch, searchObj.lngSearch);
    		searchObj.myPosition = new Array(searchObj.lngSearch, searchObj.latSearch);
    		
    		searchObj.currentKm = searchObj.radiusSearch,
    		searchObj.currentRadius = searchObj.radiusSearch * 1000;

    		//retrouve la valeur du range correspondant au km du radius
    		var rangeVal = 0;
    		for(var i=0; i<=100; i++){
	    		rangeVal = searchObj.getKm(i);
	    		if(rangeVal == searchObj.currentKm){
	    		 rangeVal = i; break;
	    		}
	    	}
	    	console.log("rangeVal", rangeVal);
    		$("#formControlRange").val(rangeVal);
			$(".around-me-distance").html(searchObj.currentKm + " km");

    		$(".app-search").addClass("sig-open");
			searchObj.showCircle();

    		$("#check-search-aroundme").prop('checked', true);
    		$(".widget-aroundme").removeClass("d-none");
            $(".app-search").addClass("sig-open"); console.log("searchObj.results", searchObj.results);
           
			$("#btn-show-map").addClass("d-none");
			$("#btn-hide-map").removeClass("d-none");
			setTimeout(function(){ Sig.showDataOnMap(searchObj.results); }, 1500);
    	}

    	if(searchObj.yourSearch != false){
    		$("#search-in-page, .js-user-search").val(searchObj.yourSearch);
    	}

    	if(searchObj.typesSearch != false){
    		var typesSplit = searchObj.typesSearch.split(",");
    		console.log("typesSplit", typesSplit);
	    	$.each(typesSplit, function(key, type){
	    		$(".btn-type-page[data-type-name='"+type+"']").addClass("selected");
	    	});
	    }
	    $("#search-url").val(location);
    },

    initBtnOpenMarker : function(afterLoad){
    	$(".btn-open-marker, .btn-open-marker-2").click(function(){
    		var id = $(this).data("id");
    		if(Sig.mapIsLoaded == false){
	    		searchObj.showMap(function(){
	    			Sig.openMarkerById(id); 
	    			if(afterLoad != null)
            			afterLoad();
	    		});

    		}else{
    			$("#"+searchObj.canvasId).removeClass("d-none");
            	$(".app-search").addClass("sig-open");
	    		setTimeout(function(){ 
	    			Sig.openMarkerById(id); 
	    			if(afterLoad != null)
            			afterLoad();
	    		}, 500);
    		}
        
    	});
    }


};