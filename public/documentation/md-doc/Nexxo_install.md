# Nexxo install
------

##### install mongodriver for php7 :
sudo apt-get install php-mongodb

##### install zmq for socket (chat/notifications)
//sudo apt-get install php-zmq
----------

sudo chown -R $USER $HOME/.composer
composer create-project symfony/website-skeleton nexxo

- install dependances

composer require twig
composer require annotation
composer require profiler
composer require maker-bundle
composer require form
composer require validator
composer require symfony/translation
composer require orm
composer require encore
composer require asset
composer require webpack
composer require ajv
composer require symfony/security-bundle
composer require symfony/swiftmailer-bundle
composer require symfony/var-dumper --dev
composer require symfony/dom-crawler
composer require symfony/css-selector
composer require symfony/monolog-bundle

//chat - socket
//composer require gos/web-socket-bundle
//composer remove gos/web-socket-bundle
composer require psr/log
composer require cboden/ratchet
composer require react/zmq

composer remove react/zmq
//https://ourcodeworld.com/articles/read/242/creating-an-agnostic-realtime-chat-with-php-using-sockets-in-symfony-3

//migrations
composer require doesntmattr/mongodb-migrations-bundle

- install mongodb

composer config "platform.ext-mongo" "1.6.16" && composer require "alcaeus/mongo-php-adapter"
composer require alcaeus/mongo-php-adapter
composer require doctrine/mongodb-odm-bundle
https://medium.com/@ahmetmertsevinc/symfony-4-and-doctrine-mongo-db-c9ac0f02f742

- generate assets

//install nodejs
sudo apt-get install nodejs-legacy

npm install

//install dependancies for sass
npm i -D node-sass sass-loader
npm i
npm i -D node-sass sass-loader

sudo apt install cmdtest

sudo npm cache clean -f
sudo npm install -g n
sudo n stable

./node_modules/.bin/encore dev --watch


//composer require antimattr/mongodb-migrations
//composer require antimattr/mongodb-migrations-bundle

- generate code

php bin/console make:controller Acme
php bin/console make:entity Acme
php bin/console make:form Acme

php bin/console generate:doctrine:entity

php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate

- generate documentation

	- install
	cd /nexxo/documentation
	curl -O http://get.sensiolabs.org/sami.phar

	- generate 
	cd public/documentation
	php sami.phar update config-sami.php

- generate migration
php bin/console doctrine:migrations:diff 


- generate translation
php ./bin/console translation:update --dump-messages --force fr

- alias command line (.bashrc)
alias snexxo="cd /var/www/nexxo; php -S 127.0.0.1:8002 -t public"
alias schat="cd /var/www/nexxo; php bin/console sockets:start-chat"
alias anexxo="cd /var/www/nexxo; ./node_modules/.bin/encore dev --watch"
alias docnexxo="cd /var/www/nexxo/public/documentation; php sami.phar update config-sami.php"



- openssl encrypting
sudo apt-get -y install libmcrypt-dev
sudo apt-get -y install php7.2-dev
sudo pecl install mcrypt-1.0.1
sudo apt-get -y install libmcrypt-de