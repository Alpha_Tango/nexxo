# Comments stream specifications
Collection of embeded documents "Comment"
---

class News{
	commentsStream : referenceOne (not inversed)
}

class CommentStream{
	parentId
	parentType
	stream : embedMany Comment
}

EmbedDocument
class Comment{
	author: 	ref PageUser (not inversed)
	signed: 	ref Page (not inversed)
	text: 		string
	tags:		list of string #tags auto-extracted from text
	likes: 		ref Page (not inversed)
	dislikes: 	ref Page (not inversed)
	replies: 	list of childs Comment (inversed by Comment::replyTo)
	replyTo: 	parent Comment (inversed by Comment::replies)
	created: 	date
}



## - 1 | Create the model src/Entity/Comment.php
## - 2 | Create the controller in src/Controller/CommentController.php
## - 3 | Create public/js/comment.js
## - 4 | Create the template for comments block : /templates/comments/comment-block.html.twig
## - 5 | init the button to send request 'get-comments-news'
## - 6 | implement comment.initUi()
## - 7 | Initialise jquery event with comment.initUi() 