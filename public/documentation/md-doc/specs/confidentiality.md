### Confidentiality & Preferences

---
#### Confidentiality
3 niveau de conf : privé - amis - follower - admin

Condifentiality::CONF_PRIVATE
Condifentiality::CONF_FOLLOWERS
Condifentiality::CONF_FRIENDS
Condifentiality::CONF_ADMIN

Page.isConfAuth("SHOW_GROUP", app.user.myUserPage.id)

[ ] SHOW_GROUP - visibilité de mes groupes
[ ] SHOW_FRIENDS - visibilité de mes amis
[ ] SHOW_FOLLOWERS - visibilité de mes followers

[ ] visibilité de mes info perso
	[ ] SHOW_EMAIL - email
	[ ] SHOW_BIRTHDAY - date de naissance
	[ ] SHOW_ADDRESS - addresse
	[ ] SHOW_GEOPOS - position géo (lat/lng)
	[ ] etc

Page{
	"name",
	"..."
	"confidentiality" : {
		"SHOW_GROUP" : "CONF_PRIVATE"
	}
}

//ConfidentialityAuthorization
Page::getConfAuth(confkey){
	return $this->confidentiality[confKey];
}

Page::isConfAuth(confkey, pageId){
	$conf = Page::getConfAuth(confkey);
	if($conf == "Condifentiality::CONF_FRIENDS"){
		return $this->getImFriend($pageId);
	}
}


####Preferences

TRUE / FALSE

[ ] chat
	[ ] AUTH_CHAT_SOUND Alerte sonore (activer/désactiver)
	[ ] AUTH_CHAT_FRIENDS - autoriser mes amis à me contacter
	[ ] AUTH_CHAT_FOLLOWERS - autoriser mes followers à me contacter
	[ ] AUTH_CHAT_ALL - autoriser tout les membres du réseau à me contacter

[ ] notification
	[ ] AUTH_NOTIF_SOUND - Alerte sonore (activer/désactiver)
	[ ] AUTH_NOTIF_EMAIL - recevoir mes notifications par e-mail
...


#### Tous les paramètres
##### Entity/Page.php - function getConfidentiality()
##### default values
array( "SHOW_ADMINS"     => "CONF_PRIVATE",
       "SHOW_ADMINOF"    => "CONF_PRIVATE",
       "SHOW_GROUP"      => "CONF_PRIVATE",
       "SHOW_FRIENDS"    => "CONF_PRIVATE",
       "SHOW_MEMBEROF"   => "CONF_PRIVATE",
       "SHOW_FOLLOWERS"  => "CONF_PRIVATE",
       "SHOW_FOLLOWS"    => "CONF_PRIVATE",
       "SHOW_PAGECREATED"=> "CONF_PRIVATE",
       "SHOW_BIRTHDAY"   => "CONF_PRIVATE",
       "SHOW_ADDRESS"    => "CONF_PRIVATE",
       "SHOW_GEOPOS"     => "CONF_PRIVATE",

       //INFORMATIONS
       "SHOW_INFO_EMAIL"    => "CONF_PRIVATE",
       "SHOW_INFO_WEBSITE"  => "CONF_PRIVATE",
       "SHOW_INFO_FACEBOOK" => "CONF_PRIVATE",
       "SHOW_INFO_TWITTER"  => "CONF_PRIVATE",
       "SHOW_INFO_YOUTUBE"  => "CONF_PRIVATE",
       "SHOW_INFO_DISCORD"  => "CONF_PRIVATE",

       //CHAT
       "AUTH_CHAT"       	=> "CONF_FRIENDS"

       //NEWS
       "AUTH_SENDPOST"      => "CONF_FRIENDS"
       );

##### Entity/Page.php - function getConfidentiality()
array("SHOW_ADMINS" ,"SHOW_ADMINOF","SHOW_GROUP"  ,"SHOW_FRIENDS","SHOW_MEMBEROF",
	"SHOW_FOLLOWERS","SHOW_FOLLOWS","SHOW_PAGECREATED","SHOW_BIRTHDAY","SHOW_ADDRESS","SHOW_GEOPOS",
	"SHOW_INFO_EMAIL", "SHOW_INFO_WEBSITE", "SHOW_INFO_FACEBOOK", "SHOW_INFO_TWITTER", 
	"SHOW_INFO_YOUTUBE", "SHOW_INFO_DISCORD", 
	"AUTH_CHAT", "AUTH_SENDPOST");

##### Entity/Page.php - getFormConfidentiality()


#### Fichiers dans le code :
La structure du formulaire de configuration des paramètres de confidentialité
	Entity/Page::getFormConfidentiality()

Le controller pour sauvegarder les paramètres de conf
	Controller/PageController.php
		public function saveConfidentiality($slug){

La vue du formulaire :
	templates/pages/forms/form-account-settings.html.twig

Vérifier la confidentialité avant d'afficher du contenu dans les vues :
	{% if page.isConfAuth("SHOW_MEMBEROF", app.user.myUserPage.id) %}
	Liste de tous les flags : 
		Entity/Page::getConfidentiality() ou Entity/Page::__construct()