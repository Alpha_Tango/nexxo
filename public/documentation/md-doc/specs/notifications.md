# Notifications



target : la Page qui reçoit la notification
author : la/les Page qui émet la notification (multi)
verb   : définit l'action réalisé
aboutId  : l'id de l'objet concerné par la notification
aboutType : le type de l'objet concerné par la notification

author + verb + about

Tango + a commenté + votre publication
Charly + a partagé + votre publication
Marie + aime + votre publication
Charlo + aime + votre page



[ ] POST
  [ ] partage de l'un de mes post
  [X] j'aime / j'aime pas sur un de mes post
  [X] commentaire sur un de mes post (dont je suis l'auteur)
  [X] commentaire sur un post où j'ai commenté
  [ ] post sur ma page perso

[ ] COMMENT
  [ ] like/dislike l'un de vos commentaires
  [X] réponse sur un de mes commentaires

[ ] PAGE
  [ ] nouveau follower d'une page dont je suis admin (ma page aussi)
  [ ] nouveau membre d'une page dont je suis admin (sauf ma page, puisque je valide moi-même mes amis)
  [ ] nouveau j'aime / j'aime pas sur une page dont je suis admin
  [ ] création d'une nouvelle page par un ami à moi


VERBS : like, share, comment, post, follow, member, create

ABOUT_TYPES : News, Comment, Page