# Pages relations specifications
## How to manage admin, groups, friends, followers, participants, likes
Comment gérer les relation (admins, groupes, amis/membres, followers, participants, intéressé) entre les différentes pages du réseau

---
**Page::owner -> reference Pages/type:user
Page::admins -> reference Pages/type:user (must be in Page:::friends)
Page::friends -> reference Pages/type:all
Page::followers -> reference Pages/type:user
Page::participants -> reference Pages/type:all
Page::likes -> reference Pages/type:all
Group::members -> reference Pages (must be in Page:::friends)**

---
Page::owner -> reference Pages/type:user
Page::relations -> Embedded Document Relation

Relation::admins -> reference Pages/type:user (must be in Page:::friends)
Relation::friends -> reference Pages/type:all
Relation::followers -> reference Pages/type:user
Relation::participants -> reference Pages/type:all
Relation::likes -> reference Pages/type:all

Relation::groups -> reference Group
Group::members -> reference Pages (must be in Page:::friends)

---
            {{ include("notification/allActivity.html.twig") }}

- La relation "Friend" fonctionne de la même façon pour toutes les pages, même si elle n'a pas tout à fait le même sens selon le type :
    - Pour les type "user" : friend
    - Pour les autres types : member

- Friend est une relation à 2 sens.
Pour devenir "friend" il faut envoyer une demande d'ami, et que cette demande soit validée par le/les admin(s) de la Page qui la reçoit.

- Le Owner d'une Page (le créateur) est considéré comme le superAdmin de la page (possède tout les droits). Le seul avantage d'un Owner est qu'il est le seul à pouvoir supprimer des admins.

- Le Owner peut ajouter/accepter des admin
- Le Owner (lui seul) peut supprimer des admin
- Les admins peuvent ajouter/accepter de nouveaux admins (mais ne peuvent pas en supprimer)
- Les admin peuvent accepter/ajouter/supprimer de nouveaux Friends
- Les admin peuvent créer/supprimer des Groups et gérer les membres des Groups (ajouter/supprimer/déplacer)
- Pour devenir admin, il faut être Friend & envoyer une demande d'amin OU être ajouté par un admin

- Les Followers n'ont pas besoin de la validation des admin pour devenir Followers
- Les Participants n'ont pas besoin de la validation des admin pour devenir Participants.
    - Seules les pages de type Event & Project peuvent avoir des Participants

---
#### Quelle différence entre friends, followers et participants ?
Pour la V1, nous faisons la différence entre ces différentes relations pour pouvoir gérer la portée des publications de chaque page (de même pour les notifications).

Lorsque l'on publie un message, il est possible de définir à quel groupe de personne nous souhaitons envoyer ce message.

- Certains messages peuvent être destinés uniquement aux friends/membres qui ont été validés par les admin de la page. 
- D'autres messages peuvent être destinés aux Followers (incluant les Friends) de façon à élargir le groupe de personnes autorisés à lire le message.
- D'autres messages peuvent être partagé "pour tous", de façon à être visible pour n'importe quelle personne visitant la page.
- D'autres messages pourront être partagé avec une "zone de publication", à destination de toutes les personnes géolocalisées dans la zone définie.

---

Road Map : 

- Friends
    - Envoyer une demande d'ami
    - Accepter une demande d'ami
- Groupes
    - Créer un groupe OK
    - Supprimer un groupe
    - Renommer un groupe
    - Ajouter un ami dans un groupe
    - Supprimer un member d'un groupe
    - Déplacer un membre d'un groupe dans un autre groupe
- Admin
    - Envoyer une demande d'amin
    - Accepter une demande d'admin
    - Ajouter un admin
    - Supprimer un admin
- Follower
    - Devenir follower
    - Annuler sa relation follower
- Participants
    - Devenir Participant
    - Annuler sa relation Participant

