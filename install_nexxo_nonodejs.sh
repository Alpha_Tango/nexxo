echo "Let's install Nexxo !"
sudo mkdir vendor
sudo mkdir var
echo "vendors created"
sudo chmod -R 777 vendor
sudo chmod -R 777 var
echo "chmod changed"
echo "Compose Nexxo !"
composer install
echo "Nexxo Composer : OK"
sudo chown -R $USER:$USER var/cache
sudo chown -R $USER:$USER var/log
sudo chown -R $USER:$USER var/session
echo "chown changed"
echo "init upload image"
cd public
mkdir uploads
cd uploads
mkdir imgBanner
mkdir imgBanner/min
mkdir imgProfil
cd ..
cp img/install/default-banner.png uploads/imgBanner/default.png
cp img/install/default-banner-min.png uploads/imgBanner/min/default.png
cp img/install/default-profil.png uploads/imgProfil/default.png