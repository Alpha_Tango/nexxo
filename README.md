# Nexxo
## Local Social Network

[Visite Nexxo](https://nexxociety.co)

### Description
Nexxo is PHP application based on framework Symfony4.

Nexxo is a Social-Network application, totaly free and open-source.

The goal of Nexxo is to provide new virtual tools to communicate with other people.

Actualy, Nexxo enable local-communication, based on geographical positions of his members. Just select a geographical area on the map, end click to send your post to everybody in this area !

This is the main feature. But many others complete the app, like an agenda, friend groups, real time notifications, and private chat-rooms.

All tools can be improved by anybody who want to help to build a best application ;)


---

### Requirement

- Composer (https://getcomposer.org/download/)
- PHP 7 ([install doc](https://askubuntu.com/questions/705880/how-to-install-php-7))
- Mongodb ([install doc](https://gitlab.com/Alpha_Tango/nexxo/blob/master/public/documentation/md-doc/mongodb-install.md))

> Make sure your actual version of PHP is PHP-7 before install Mongodb-driver.
> If not : install PHP7 ! ([install doc](https://askubuntu.com/questions/705880/how-to-install-php-7))
```
php -v
//must give something like this :
PHP 7.2.7-1+ubuntu16.04.1+deb.sury.org+1 (cli) [...]
```


### Install Nexxo

#### 1 -Clone the repository : 
> For this exemple, we are installing Nexxo in : /var/www
```
cd /var/www
git clone https://gitlab.com/Alpha_Tango/nexxo.git
```

### * Auto-install
Section 1 - 2 - 3 - 4 can be done by running the following script :
```
cd /var/www/nexxo
./install-nexxo.sh
```
If everything is ok, you can skip to section 5 : Run Nexxo !

If you have any trouble during auto-install, see the following instructions to achieve your install with your own hand !

### * Manual install
---
First, 3 little things :
```
sudo mkdir nexxo/vendor
sudo chmod -R 775 nexxo/vendor
sudo chmod -R 777 nexxo/var
```

Next, go to the root of the repository and run Composer : 

```
cd nexxo
composer install
```

#### 2 - Install nodejs (at the root of repository)
```
sudo apt-get install nodejs-legacy
sudo apt-get install npm
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
npm install
//npm install ajv
//npm i -D node-sass sass-loader
//npm i
//npm i -D node-sass sass-loader
//sudo apt install cmdtest
```

#### 3 - Make sure this directories are owned by your system user (not root) : 
> Symfony need to write on it
- /nexxo/var/cache
- /nexxo/var/log
- /nexxo/var/session

In this case, the owner name is "tango" : 
```
> ll /var/www/nexxo/var
drwxrwxrwx  4 tango tango 4096 août   3 20:56 cache/
drwxrwxrwx  2 tango tango 4096 juil. 13 00:10 log/
drwxrwxrwx  3 tango tango 4096 août   1 18:01 sessions/
```
If your owner is "root" instead of "tango", you need to change it.
So run this commands (replace "myUsername" by your username) 
```
sudo chown -R myUsername /var/www/nexxo/var/cache
sudo chown -R myUsername /var/www/nexxo/var/log
sudo chown -R myUsername /var/www/nexxo/var/session
```

#### 4 - Let's create upload directory
```
cd /var/www/nexxo/public
mkdir uploads
cd uploads
mkdir imgBanner
mkdir imgBanner/min
mkdir imgProfil
cp /var/www/nexxo/public/img/install/default-banner.png /var/www/nexxo/public/uploads/imgBanner/default.png
cp /var/www/nexxo/public/img/install/default-banner-min.png /var/www/nexxo/public/uploads/imgBanner/min/default.png
cp /var/www/nexxo/public/img/install/default-profil.png /var/www/nexxo/public/uploads/imgProfil/default.png

```

---
### 5 - Run Nexxo

To launch Nexxo you must run 3 services in background.
You can use the following aliases to make it easier (include it in your .bashrc file) : 

```
//start mongodb
alias smongo="sudo mongod --dbpath /var/lib/mongodb/data/db"

//start Symfony built-in server
alias snexxo="cd /var/www/nexxo; php -S 127.0.0.1:8001 -t public"

//start the socket server for chat and notification
alias schat="cd /var/www/nexxo; php bin/console sockets:start-chat"

//start Encore service for assets management
alias anexxo="cd /var/www/nexxo; ./node_modules/.bin/encore dev --watch"
```

When you want to launch Nexxo, run each command in different terminals :
```
smongo | snexxo | schat | anexxo
```
## Then you can access Nexxo !
[http://127.0.0.1:8001/](http://127.0.0.1:8001/)


---
### 6 - Generate documentation

Nexxo integrate a module to generate the full documentation based on the code.
https://github.com/FriendsOfPHP/Sami

To generate the doc, just run this command : 
```
cd /var/www/nexxo/public/documentation
php sami.phar update config-sami.php
```

Or use alias : 
```
alias docnexxo="cd /var/www/nexxo/public/documentation; php sami.phar update config-sami.php"
```
Then, you can access this documentation at : 
http://127.0.0.1:8001/documentation/build/index.html

### Want more documentation ?
#### Visit [our Wiki](https://gitlab.com/Alpha_Tango/nexxo/wikis/) !