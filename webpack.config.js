var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // uncomment to define the assets of the project
    .addEntry('js/app', ['./assets/js/app/jquery-3.2.0.min.js',
                         './assets/js/app/material.min.js',
                         //'./assets/js/app/tether.min.js',
                         './assets/js/app/webfontloader.min.js',
                         './assets/js/app/theme-plugins.js',
                         './assets/js/app/moment/moment.min.js',
                         './assets/js/app/daterangepicker.js',
                         './assets/js/app/jquery.magnific-popup.min.js',
                         './assets/js/app/selectize.min.js',
                         './assets/js/app/main.js',
                         './assets/js/imgencore.js',
                         //'./assets/js/app/nexxo.js',
                        ])

    .addStyleEntry('css/app', ['./assets/css/app/Bootstrap/dist/css/bootstrap-reboot.css',
                               './assets/css/app/Bootstrap/dist/css/bootstrap.css',
                               './assets/css/app/Bootstrap/dist/css/bootstrap-grid.css',
                               './assets/css/app/bootstrap-select.css',
                               './assets/css/app/fonts.css',
                               './assets/css/app/jquery.mCustomScrollbar.min.css',
                               './assets/css/app/daterangepicker.css',
                               './assets/css/app/theme-styles.css',
                               './assets/css/app/blocks.css',       
                               './assets/css/app/input-range.css',                               
                               './assets/css/nexxo_style.css',
                              ])


    // uncomment if you use Sass/SCSS files
    .enableSassLoader()

    // uncomment for legacy applications that require $/jQuery as a global variable
    .autoProvidejQuery()
;



module.exports = Encore.getWebpackConfig();
