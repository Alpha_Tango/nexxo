<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use App\Entity\Media;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\User;
use App\Services\Helper;

use Symfony\Component\DomCrawler\Crawler;
use Psr\Log\LoggerInterface;

class MediaController extends Controller
{    
    /**
     * Route : /mediatheque", name="mediatheque"
     * @Route("/mediatheque", name="mediatheque")
     */
    public function mediatheque(Request $request, AuthorizationCheckerInterface $authChecker){

    	$em = $this->get('doctrine_mongodb')->getManager();
    	$mediaRepo = $em->getRepository(Media::class);

    	$channels = Media::getChannels();
    	$videos = array();
    	foreach ($channels as $key => $channel) {
    		$medias = $mediaRepo->findBy(array("channelUrl" => $channel), array("created" => "DESC"), 6);
    		$videos[$channel]["videos"] = array();
    		foreach ($medias as $k => $media) {
    			$videos[$channel]["name"] = $media->getChannelName();
    			$videos[$channel]["description"] = $media->getChannelDescription();
    			$videos[$channel]["thumb"] = $media->getChannelThumb();
    			$videos[$channel]["videos"][] = $media;
    		}
    	}
    	

    	$params = array("videos" => $videos);
    	return $this->render('mediatheque/home.html.twig', $params);
    }

    /**
     * Route : /mediatheque/load/{search}/{renderPartial}", name="mediatheque-load"
     * @Route("/mediatheque/load/{search}/{renderPartial}", name="mediatheque-load")
     */
    public function load($search="", $renderPartial="false", LoggerInterface $logger, 
                                                             Request $request, 
                                                             AuthorizationCheckerInterface $authChecker){

        $channels = Media::getChannels();

        $em = $this->get('doctrine_mongodb')->getManager();
        $mediaRepo = $em->getRepository(Media::class);

        $parsed = 0;//array();
        foreach($channels as $k => $url){  $logger->info("LOAD YOUTUBE CHANNEL - ".$url); //error_log("LOAD YOUTUBE CHANNEL - ".$url);
            $html = file_get_contents($url);
            $crawler = new Crawler($html);

            //dd($crawler->html());

            $channelName = $crawler->filter("meta[name='title']")->attr("content");
            $channelDescription = $crawler->filter("meta[name='description']")->attr("content");
            $channelThumb = $crawler->filter("img.appbar-nav-avatar")->attr("src");
            //dd($crawler->html());


            $container = $crawler->filter(".channels-content-item")->each(function (Crawler $node, $i) {
                    return $node->html();
                });//->html();

            /*$parsed[$url] = array("name" => $channelName, 
                                  "description" => $channelDescription, 
                                  "thumb" => $channelThumb,
                                  "videos" => array());*/

            foreach ($container as $key => $value) {
                $crawler = new Crawler($value);
                $title = $crawler->filter(".yt-uix-tile-link")->html();
                $href = $crawler->filter(".yt-uix-tile-link")->attr("href");
                $imgSrc = $crawler->filter("img")->attr("src");
                

                $media = $mediaRepo->findOneBy(array("href" => $href));
                if($media == null){
                    $media = new Media(array( "channelName" => $channelName, 
                                              "channelDescription" => $channelDescription, 
                                              "channelThumb" => $channelThumb,
                                              "channelUrl" => $url,
                                              "title" => $title, 
                                              "href" => $href, 
                                              "imgSrc" => $imgSrc,
                                              "created" => new \Datetime()
                                        ));
                    $em->persist($media);
                    $em->flush();

                    $parsed++;

                }else{
                    break;
                }
            } 

        }
        
        return $this->json(array('error' => false, 
                                 'msg' => $parsed.' nouvelle vidéos ont été trouvées'));
        //$params = array("videos" => $parsed);
        //return $this->render('mediatheque/media-list.html.twig', $params);

    }

    /**
     * init channels for first time
     * Route : /mediatheque/init-channels/{type}", name="mediatheque-init-channels"
     * @Route("/mediatheque/init-channels/{type}", name="mediatheque-init-channels")
     */
    public function initChannels($type="youtube",  LoggerInterface $logger, 
                                                             Request $request, 
                                                             AuthorizationCheckerInterface $authChecker){
        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
            
        $parsed = 0;

        $channels = Media::getChannels();
        foreach($channels as $k => $url){  $logger->info("LOAD YOUTUBE CHANNEL - ".$url); 
            $resImport = $this->runImportChannel($url, $type, $em, false);
            $parsed = @$resImport["nmed"];
            //flush();
        } 
        
        return $this->json(array('error' => false, 
                                 'msg' => $parsed.' nouvelle vidéos ont été trouvées'));
    }


    /**
     * create new channel (proposition from user)
     * Route : /mediatheque/import-channels/{type}", name="mediatheque-import-channels"
     * @Route("/mediatheque/import-channels/{type}", name="mediatheque-import-channels")
     */
    public function importChannels($type="youtube",  LoggerInterface $logger, 
                                                     Request $request, 
                                                     AuthorizationCheckerInterface $authChecker){
       
        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
            
        $parsed = 0;

        //prend l'url passé en paramètre s'il y en a une
        //sinon effectue l'import de tous les channels
        $url = $request->query->get('url') ? $request->query->get('url') : "";
       // dump("url :".$url);
        if($url != ""){ 

            if(strpos($url, "/videos") !== strlen($url)-7)
                    $url .= "/videos";

            $pageChan = $pageRepo->findOneBy(array("informations.youtube" => $url));
            //dump("pageChan :");
            //dump($pageChan);
            if($pageChan != null){
                return $this->json(array('error' => true, 
                                         'msg' => 'This channel already exists'));
            }

            if($this->checkValideChannelUrl($url) == false){
                return $this->json(array('error' => true, 
                                         'msg' => 'This url is not a valid youtube url'));
            }

            //dump($url);
            $resImport = $this->runImportChannel($url, $type, $em);
            $parsed = @$resImport["nmed"];
            //dump($resImport);
            if($parsed === false)
                return $this->json(array('error' => true, 
                                         'msg' => 'This url does not exists'));


            return $this->json(array('error' => false, 
                                     'slug' => $resImport["channel"]->getSlug(), 
                                     'name' => $resImport["channel"]->getName(), 
                                     'msg' => 'The channel has been created with success'));

        }else{
            //$channels = Media::getChannels();
            $channels = $pageRepo->findBy(array("type" => "channel"), array("dateCheckNotif" => "ASC"), 15);
            
            foreach($channels as $k => $chan){  
                //$logger->info("LOAD YOUTUBE CHANNEL - ".$url); 
                $url = $chan->getInformations()->getYoutube();
                $resImport = $this->runImportChannel($url, $type, $em);
                $parsed += @$resImport["nmed"];

                $chan->setDateCheckNotif(new \Datetime());
                $em->flush();
                //dump("scan channel - ".$chan->getName()); flush();
            } 
        
            return $this->json(array('error' => false, 
                                     'msg' => $parsed.' new videos has been found'));
        }
    }


    private function runImportChannel($url, $type, $em, $importVideo=true){
        //error_log("LOAD YOUTUBE CHANNEL - ".$url);
            $pageRepo = $em->getRepository(Page::class);
            $userRepo = $em->getRepository(User::class);
            $superadmin = $userRepo->findOneBy(array("isSuperAdmin" => true));

            $html = @file_get_contents($url);

            if(@$html === false) return array("nmed" => false);

            $crawler = new Crawler($html);

            //dump($crawler->html());

            $channelName = $crawler->filter("meta[name='title']")->attr("content");
            $channelDescription = $crawler->filter("meta[name='description']")->attr("content");
            $channelThumb = $crawler->filter("img.appbar-nav-avatar")->attr("src");
            //$channelBanner = $crawler->filter("meta[property='og:image']")->attr("content");

            $bannerId = "#c4-header-bg-container .hd-banner-image {\n      background-image: url("; 
            
            /* find url to image banner */
            $channelBanner = $crawler->filter("img.channel-header-profile-image")->attr("src");
            
            $channel = $crawler->html();
            $start = strpos($html, $bannerId);   
            $end = strpos($html, ");", $start);
            $realStart = $start+strlen($bannerId);
            $sub = substr($html, $realStart, $end-$realStart);
            $channelBanner = "https:".$sub;
            /* find url to image banner */
            if(strpos($channelBanner, "https://") === false) $channelBanner = "default_channel.png";
           
            //dd($crawler->html());
            $channelPage = null;
            $pageChan = $pageRepo->findOneBy(array("name" => $channelName));
            if($pageChan == null){
                $channelPage = new Page();
                $channelPage->setName($channelName);
                $channelPage->setDescription($channelDescription);

                $slug = Helper::getUniqueSlug($channelPage->getName(), $pageRepo);
                $channelPage->setSlug($slug);

                $channelPage->setType(Page::TYPE_CHANNEL);
                $channelPage->setImageProfil($channelThumb);
                $channelPage->setImageBanner($channelBanner);
                $channelPage->getInformations()->setYoutube($url);
                //$channelPage->setTags(array("#", "#"));
                $channelPage->setIsActive(true);

                $channelPage->setOwner($superadmin);
                //$channelPage->setConfidentialityByKey("SHOW_GEOPOS", "CONF_NEXXO");
                //dump("create channel - ".$channelName); flush();
                $em->persist($channelPage);
            }else{
                $channelPage = $pageChan;
                //return $this->json(array('error' => true, 
                //                         'msg' => "this channel already exists"));
            }

            $date = new \Datetime();
            if($importVideo==false){
                $inter = rand(0, 180);
                $date->sub(new \DateInterval('PT'.$inter.'S'));
            }
            $channelPage->setCreated($date);
            $channelPage->setDateCheckNotif($date);
            $info = $channelPage->getInformations();
            $info->setYoutube($url);
            $channelPage->setInformations($info);

            $channelPage->getRelations()->addAdmin($superadmin->getMyUserPage());
            $superadmin->getMyUserPage()->getRelations()->addIsAdminOf($channelPage);

            $superadmin->getMyUserPage()->getRelations()->removeChatContacts($channelPage);
            
            $channelPage->setConfidentialityByKey("SHOW_FOLLOWERS", "CONF_NEXXO");
            $channelPage->setConfidentialityByKey("SHOW_FRIENDS", "CONF_NEXXO");

            //dump($inter);
            $em->flush();
            //return $this->json(array('error' => false, 
            //                         'msg' => $channelPage));

            $container = $crawler->filter(".channels-content-item")->each(function (Crawler $node, $i) {
                    return $node->html();
                });
            
            /*$parsed[$url] = array("name" => $channelName, 
                                  "description" => $channelDescription, 
                                  "thumb" => $channelThumb,
                                  "videos" => array());*/
            $nmed = 0;
            $nmax = 10;
            $total = 0; //prevent infinite loop
            if($importVideo == true)
            foreach ($container as $key => $videoHtml) { 
                if($nmed < $nmax && $total < 100){
                    $importSuccess = $this->runImportVideo($videoHtml, $channelPage, $type, $nmed, $em);
                    //dump($importSuccess);
                    if($importSuccess == false) break;
                    elseif($importSuccess === true) $nmed++;
                    $total++;
                }else{ break; }

                //dump($total."-".$nmed);
            } 


            //dump("created media : ".$parsed. " on ".$channelName);
            //dump("created media : ".$nmed. " on ".$channelName);
            //flush();

            return array("nmed" => $nmed, "channel" => $channelPage);
            /*
            return $this->json(array('error' => false, 
                                     'msg' => $channelPage));*/
    }


    private function runImportVideo($videoHtml, $channelPage, $type, $nmed, $em){
        $crawler = new Crawler($videoHtml);
        $title = $crawler->filter(".yt-uix-tile-link")->html();
        $href = $crawler->filter(".yt-uix-tile-link")->attr("href");
        $imgSrc = $crawler->filter("img")->attr("src");
        
        if($type == "youtube") $href = "https://www.youtube.com".$href;

        if($this->checkValideVideoUrl($href) == false) return -1;
        
        $mediaRepo = $em->getRepository(Media::class);
        $media = $mediaRepo->findOneBy(array("url" => $href)); 
        if($media == null){ 
            $media = array( "title"         => $title,
                            "description"   => "",
                            "image"         => $imgSrc,
                            "url"           => $href,
                            "preview"       => true
                        );
           

            //dump("create media - ".$title);

            $news = new News();
            //$myUserPage = $this->getUser()->getMyUserPage();
            
            $news->setText($href);

            $news->setMedias(array($media));
            $news->setAuthor($channelPage);
            $news->setSigned($channelPage);
            $news->setTarget($channelPage);
            $news->setScopeType("all");

            //Tag::registerTags($news->getTags(), $this->get('doctrine_mongodb')->getManager(), true);

            //$isSharable = $news->getIsSharable() == "true" ? true : false; 
           
            $news->setIsSharable(true);
            $date = new \Datetime();
            $inter = rand(60 * $nmed, (60 * ($nmed+1)));
            $date->sub(new \DateInterval('PT'.$inter.'S'));
            $news->setCreated($date);

            $mediaObj = new Media($media);
            
            $em->persist($mediaObj);
            $em->persist($news);
            $em->flush();

             //dump("add media - ".$nmed. " -".$inter."-");
            return true;

        }else{ //dump("break media - ".$title);
            return false;
        }

        return true;
    }

    private function checkValideChannelUrl($url){

        if(strpos($url, "https://www.youtube.com/channel") !== false) return true;
        if(strpos($url, "https://www.youtube.com/user") !== false) return true;

        return false;
    }
    private function checkValideVideoUrl($url){
        if(strpos($url, "https://www.youtube.com/watch") !== false) return true;
        return false;
    }
}
