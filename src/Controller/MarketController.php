<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;

use App\Form\PageType;
use App\Entity\User;
use App\Entity\Page;
use App\Entity\Group;
use App\Entity\News;
use App\Entity\CommentStream;
use App\Entity\Comment;
use App\Entity\Notification;
use App\Entity\Report;
use App\Entity\Tag;
use App\Form\CommentType;

use App\Services\Helper;

class MarketController extends Controller
{

    /**
     * Route : /market/{search}/{renderPartial}", name="market"
     * @Route("/market/{search}/{renderPartial}", name="market")
     */
    public function market($search="", $renderPartial="false", 
                               Request $request, AuthorizationCheckerInterface $authChecker)
    {

        //if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        if($search == "-") $search = "";
        $search = preg_replace("/></", "#", $search);
        
        $myUser = $this->getUser();
        if($myUser != null) $myPosition = $myUser->getMyPosition();
        else $myPosition = [2.6806640625, 47.1897124644842];

        $myUserPage = new Page(); $myUserPage->setIsActive(true);
        if($myUser != null) $myUserPage = $this->getUser()->getMyUserPage();

        $aroundCoordinates = null;
        if($request->query->get('lat') != null && $request->query->get('lon') != null)
        $aroundCoordinates = array(floatval($request->query->get('lat')), 
                                    floatval($request->query->get('lon')));


        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        $allRes = $pageRepo->getGlobalSearch($search,  
                                          Page::TYPE_CLASSIFIED, //$request->query->get('types'), 
                                          $aroundCoordinates,
                                          floatval($request->query->get('radius'))
                                        );
        $res = $allRes["res"];
        $totalCount = $allRes["totalCount"];

        if($aroundCoordinates != null && $renderPartial!="json"){
            foreach ($res as $key => $page) {
                if(!$page->isConfAuth("SHOW_GEOPOS", $myUserPage))
                    unset($res[$key]);
            }
        }
    
        $jsonRes = $this->getJsonRes($res, $myUserPage);
        $params = array(
                "results" => $res,
                "totalCount" => $totalCount,
                'jsonRes' => $jsonRes,
                "yourSearch" => $search,
                'myPosition' => $myPosition,
                'typeSearch' => Page::getTypeMainSearch()
            );

        $params["typesSearch"] = Page::TYPE_CLASSIFIED;
        if($aroundCoordinates != null)              $params["coordinatesSearch"] = $aroundCoordinates;
        if(!empty($request->query->get('radius')))  $params["radiusSearch"] = floatval($request->query->get('radius'));
       
        if ($authChecker->isGranted('ROLE_USER')){
            $params["extends"] = "theme/base-app.html.twig";
            $params["userPage"] = $myUserPage;
        }else{
            $params["extends"] = "theme/base-app-offline.html.twig";
            $myUserPage = new Page(); $myUserPage->setIsActive(true);
            $params["userPage"] = $myUserPage;
        }
         
        //dd($jsonRes);
        if($renderPartial=="false"){
            return $this->render('market/search.html.twig', $params);
        }elseif($renderPartial=="json"){
            return $this->json(array('jsonRes' => $jsonRes));
        }else{
            return $this->render('market/blocks/search-res-list.html.twig', $params);
        }
    }

    /**
     * Route : /create-classified/{classifiedId}, name="create-classified"
     * @Route("/create-classified/{classifiedId}", name="create-classified")
     */
    public function createClassified($classifiedId=null, 
                                     Request $request, 
                                     AuthorizationCheckerInterface $authChecker){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUser = $this->getUser();

        $newPage = new Page();
        if($classifiedId != null){
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $newPage = $pageRepo->findOneBy(array("id" => $classifiedId));
        }
        $isAdminOf = $this->getUser()->getMyUserPage()->getRelations()->getIsAdminOf()->toArray();
       
        $form = $this->createForm(PageType::class, $newPage, 
                                    array("adminPage"=>$isAdminOf, "pageType"=>Page::TYPE_CLASSIFIED));
        
        $oldSlug = $newPage->getSlug();

        $em = $this->get('doctrine_mongodb')->getManager();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newPage->setOwner($myUser);
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            
            $slug = Helper::slugify($newPage->getName());
            /* MUST CHANGE THE SLUG IN PRIVATE_MESSAGES */
            //dump($oldSlug);
            //dump($slug);
            if($oldSlug != $slug){
                $pageRepo = $em->getRepository(Page::class);
                $slug = Helper::getUniqueSlug($newPage->getName(), $pageRepo);
                $newPage->setSlug($slug);
                $newPage->refactorSlugChatMessage($oldSlug, $em);
            }
            
            if($newPage->getLongitude() != null && $newPage->getLatitude() != null)
            $newPage->setCoordinates([(float)$newPage->getLongitude(), (float)$newPage->getLatitude()]);

            $newPage->setCreated(new \Datetime());
            $newPage->setIsActive(true);
            $newPage->setType(Page::TYPE_CLASSIFIED);

            $currentConf = $newPage->getConfidentiality();
            $currentConf["SHOW_ADDRESS"] = "CONF_NEXXO";
            $currentConf["SHOW_GEOPOS"] = "CONF_NEXXO";
            $currentConf["AUTH_CHAT"] = "CONF_NEXXO";
            $currentConf["AUTH_SENDPOST"] = "CONF_NEXXO";
            $newPage->setConfidentiality($currentConf);

            //add the current user page in admins
            $newPage->getRelations()->addAdmin($myUser->getMyUserPage());
            $userPage = $myUser->getMyUserPage();
            $userPage->getRelations()->addIsAdminOf($newPage);

            // Enregistre la page dans la bdd
            $em = $this->get('doctrine_mongodb')->getManager();
            if($classifiedId == null) $em->persist($newPage);
            $em->flush();

            //if the page is not private, send a notification to all my realFriends
            if($newPage->isPrivate() == false){
                /* SEND NOTIFICATION */
                $notifObj = Notification::sendNotification(
                                $this->getUser()->getMyUserPage(), Notification::VERB_CREATE_PAGE,
                                $newPage->getId(), "page", null, null, $em, $this);
            }

            return $this->redirectToRoute('page', array("slug"=>$newPage->getSlug()));
        }else{
            //dd($form->getErrors(true));
        }

        $view = 'market/create-classified.html.twig';
               
        return $this->render($view, 
            array('form' => $form->createView(),
                  'classified' => $newPage,
                  'action' => 'create-classified'));
    }

     /**
     * Convert search result objects to Json<br>
     * (to use datas in javascript, mainly for SIG to display datas on map)<br>
     * if you want your page appear in result, set excludeMe to false
     * @return array
     */
    private function getJsonRes($res, $userPage, $excludeMe=true){ 
        $jsonRes = array();
        $myPageId = $this->getUser() != null ? $this->getUser()->getMyUserPage()->getId() : 0;
        foreach ($res as $key => $value) {
            if($excludeMe == false || $value->getId() != $myPageId)
            $jsonRes[] = $value->getJson($userPage);        
        }
        return $jsonRes;
    }
}