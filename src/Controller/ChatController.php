<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use App\Form\GroupType;
use App\Entity\User;
use App\Entity\Page;
use App\Entity\Group;
use App\Entity\ChatMessage;

class ChatController extends Controller
{
    /**
     * @Route("/chat/get-history/{slug}", name="chat-get-history")
     */
    public function getHistory($slug, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        
        $mySlug = $this->getuser()->getMyUserPage()->getSlug();

        $chatMessageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(ChatMessage::class);
        //$res = $chatMessageRepo->getHistory($slug, $mySlug);

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));
        
        if($page != null && $page->getType() == Page::TYPE_USER){
          $res = $chatMessageRepo->findBy(array('$or' => array(
                                            array('$and' => array(array('senderSlug' => $slug),
                                                                  array('receiverSlug' => $mySlug)),
                                                              ),
                                            array('$and' => array(array('senderSlug' => $mySlug),
                                                                  array('receiverSlug' => $slug)),
                                                              ),
                                            ))
                                            , array("created" => "DESC"), 8);
        }else{          
          $res = $chatMessageRepo->findBy(array('receiverSlug' => $slug),
                                          array("created" => "DESC"), 8);
        }

        //dump($res);
        $json = array();
        foreach (array_reverse($res) as $key => $msg) {
          $json[] = $msg->getJson();
          if($msg->getSenderSlug() != $mySlug){
            $msg->setToRead(false);
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush();
          }
        }
        return $this->json($json);
    }

    /**
     * @Route("/chat/get-more-history/{slug}/{timestamp}", name="chat-get-more-history")
     */
    public function getMoreHistory($slug, $timestamp, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        
        $date = \DateTime::createFromFormat("U.u", $timestamp / 1000);
        
        $mySlug = $this->getuser()->getMyUserPage()->getSlug();

        $chatMessageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(ChatMessage::class);
        //$res = $chatMessageRepo->getHistory($slug, $mySlug);

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if($page->getType() == Page::TYPE_USER){
          $res = $chatMessageRepo->findBy(array('$or' => array(
                                            array('$and' => array(array('senderSlug' => $slug),
                                                                  array('receiverSlug' => $mySlug)),
                                                              ),
                                            array('$and' => array(array('senderSlug' => $mySlug),
                                                                  array('receiverSlug' => $slug)),
                                                              ),
                                            ),
                                            "created" => array('$lt' => $date)),
                                            array("created" => "DESC"), 30);
        }else{          
          $res = $chatMessageRepo->findBy(array('receiverSlug' => $slug,
                                                "created" => array('$lt' => $date)),
                                          array("created" => "DESC"), 30);
        }

        //dump($res);
        $json = array();
        foreach ($res as $key => $msg) {
          $json[] = $msg->getJson();
          $msg->setToRead(false);
          $em = $this->get('doctrine_mongodb')->getManager();
          $em->flush();
        }
        return $this->json($json);
    }


    /**
     * @Route("/chat/set-read-history/{slug}", name="set-read-history")
     */
    public function setReadHistory($slug, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        $mySlug = $this->getuser()->getMyUserPage()->getSlug();
        $chatMessageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(ChatMessage::class);
        //$res = $chatMessageRepo->getHistory($slug, $mySlug);

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if($page->getType() == Page::TYPE_USER){
          $res = $chatMessageRepo->findBy(array('$or' => array(
                                            array('$and' => array(array('senderSlug' => $slug),
                                                                  array('receiverSlug' => $mySlug)),
                                                              ),
                                            array('$and' => array(array('senderSlug' => $mySlug),
                                                                  array('receiverSlug' => $slug)),
                                                              ),
                                            ),
                                            "toRead" => true),
                                            array("created" => "DESC"), 30);
        }else{          
          $res = $chatMessageRepo->findBy(array('receiverSlug' => $slug,
                                                "toRead" => true),
                                          array("created" => "DESC"), 30);
        }

        //dump($res);
        $json = array("error"=>false);
        foreach ($res as $key => $msg) {
          //$json[] = $msg->getJson();
          $msg->setToRead(false);
          $em = $this->get('doctrine_mongodb')->getManager();
          $em->flush();
        }
        return $this->json($json);
    }


    /**
     * @Route("/chat/send-message/", name="chat-send-message")
     */
    public function sendMessage(Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        //$mySlug = $this->getuser()->getMyUserPage()->getSlug();

        $json = array("error"=>true);
        //dd(@$_POST['message']);
        if(isset($_POST['message'])){
          $em = $this->container->get('doctrine_mongodb')->getManager();
          $chatMsg = new ChatMessage($_POST['message']);
          $em->persist($chatMsg);
          $em->flush();

          $json = array("error"=>false, "message" => $chatMsg->getJson());
          return $this->json($json);
        }

        return $this->json($json);

    }


    /**
     * @Route("/chat/delete-message/{id}", name="chat-delete-message")
     */
    public function deleteMessage($id, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $mySlug = $this->getuser()->getMyUserPage()->getSlug();

        $em = $this->get('doctrine_mongodb')->getManager();
        $chatMessageRepo = $em->getRepository(ChatMessage::class);
        $msg = $chatMessageRepo->findOneBy(array("id" => $id));
        

        $json = array("error"=>true);
        if($msg->getSenderSlug() == $mySlug){
          $em->remove($msg);
          $em->flush();
          $json = array("error"=>false);

        }else{
          $json = array("error"=>true, 
                        "errorMsg"=>"Your are not the sender of this message. Mother fucker !");
        }

        return $this->json($json);
    }


    /**
     * @Route("/chat/get-toread/{slug}", name="chat-get-toread")
     */
    public function getToRead($slug, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $mySlug = $this->getuser()->getMyUserPage()->getSlug();

        $chatMessageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(ChatMessage::class);
        //$res = $chatMessageRepo->getHistory($slug, $mySlug);

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if($page->getType() == Page::TYPE_USER){
          $res = $chatMessageRepo->findBy(array('$or' => array(
                                            array('$and' => array(array('receiverSlug' => $mySlug),
                                                                  array("toRead" => true)),
                                                              ),
                                           

                                            ))
                                            , array("created" => "DESC"), 50);
        }else{          
          $res = $chatMessageRepo->findBy(array('receiverSlug' => $slug, 
                                                "toRead" => true,
                                                "senderSlug" => array('$ne' => $mySlug)
                                                ),
                                          array("created" => "DESC"), 50);
        }

        //dump($res);
        $json = array();
        foreach (array_reverse($res) as $key => $msg) {
          $json[] = $msg->getJson();
          if($msg->getSenderSlug() != $mySlug){
            $msg->setToRead(false);
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush();
          }
        }
        return $this->json($json);
    }



    /**
     * @Route("/chat/change-my-chat-status/{status}", name="change-my-chat-status")
     */
    public function changeMyChatStatus($status, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        if($status == "online" || $status == "offline" || $status == "away"){
          $myPage = $this->getuser()->getMyUserPage();
          $myPage->setChatStatus($status);

          $em = $this->get('doctrine_mongodb')->getManager();
          $em->flush($myPage);

          return $this->json(array("error"=>false));
        }
        
        return $this->json(array("error"=>true));
    }


    /**
     * @Route("/chat/start-chat", name="start-chat")
     */
    /*public function startChat(Request $request){
       
      $ex = exec("php ../bin/console sockets:start-chat &");
      dd($ex);
      return $this->json(array("error"=>false));
    }*/
}