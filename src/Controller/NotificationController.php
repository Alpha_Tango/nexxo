<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Form\UserType;
use App\Form\NewsType;
use App\Entity\User;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\ChatMessage;
use App\Entity\Notification;

class NotificationController extends Controller
{
    /**
     * Route : /get-all-activity, name="get-all-activity"
     * @Route("/get-all-activity", name="get-all-activity")
     */
    public function getAllActivity(Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        //------------------------------
        //RECUP NOTIFICATIONS
        //------------------------------

        /* get MY personnal friends requests */
        $myUserPage = $this->getUser()->getMyUserPage();
        $myUserPage->setDateOnline(new \Datetime());
        $this->get('doctrine_mongodb')->getManager()->flush($myUserPage);

        $fReq = $myUserPage->getRelations()->getFriendsRequests();
        $friendsRequests = array();
        foreach($fReq as $key => $req){
        	$friendsRequests[] = array("pageRef" => $myUserPage, "pageReq" =>  $req, "reqType" => "friend");
        }
        /* get member request on Page where I'm admin */
        $adminOf = $myUserPage->getRelations()->getIsAdminOf();
        $memberRequests = array();
        foreach($adminOf as $key => $page){
            if($page->getId() != $myUserPage->getId())
            foreach($page->getRelations()->getFriendsRequests() as $key => $req)
                $memberRequests[] = array("pageRef" => $page, "pageReq" =>  $req, "reqType" => "member");
        }
        /* get admin request on Page where I'm admin */
        $adminRequests = array();
        foreach($adminOf as $key => $page){
            foreach($page->getRelations()->getAdminsRequests() as $key => $req)
                $adminRequests[] = array("pageRef" => $page, "pageReq" =>  $req, "reqType" => "admin");
        }

        //dd($adminOf);

        $notifRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Notification::class);
        $notCheckedNotifs = $notifRepo->findBy(array('targets.$id' => new \MongoId($myUserPage->getId()),
                                                     'lastAuthor.$id' => array('$ne'=> new \MongoId($myUserPage->getId())),
                                                     'updated' => array('$gt' => $myUserPage->getDateCheckNotif())
                                                    ), 
                                                array("updated" => "DESC"));
        
        foreach ($notCheckedNotifs as $key => $notif) {
            if($notif->getLastAuthor()->getIsActive() == false && 
                $notif->getLastAuthor()->getReport() != null ){
                unset($notCheckedNotifs[$key]);
            }
        }

        $notCheckedNotifsCount = count($notCheckedNotifs);
        $limit = $notCheckedNotifsCount < 10 ? 10 : $notCheckedNotifsCount;
        
        $notifs = $notifRepo->findBy(array('targets.$id' => new \MongoId($myUserPage->getId()),
                                           'lastAuthor.$id' => array('$ne'=> new \MongoId($myUserPage->getId()))), 
                                     array("updated" => "DESC"), $limit);
        
        foreach ($notifs as $key => $notif) {
            //set aboutObj before to send notifications to the view
            $class = Notification::getClass($notif->getAboutType());
            $repo = $this->get('doctrine_mongodb')->getManager()->getRepository($class);
            $aboutObj = $repo->findOneBy(array("id" => $notif->getAboutId()));
            $notif->setAboutObj($aboutObj);

            //do not show notification send by a loked page
            if($notif->getLastAuthor()->getIsActive() == false && 
                $notif->getLastAuthor()->getReport() != null ){
                unset($notifs[$key]);
            }else{
                //set whatObject before to send notifications to the view
                if($notif->getWhatId() != null){
                    //case of comment about news
                    if($notif->getWhatType() == "comment" && $notif->getAboutType() == "news" && $aboutObj != null){
                        //search for whatId (commentId) in commentStream of the news
                        $cs = $aboutObj->getCommentStream(); 
                        $comment = $cs->getCommentById($notif->getWhatId());
                        if($comment != null)
                            $notif->setWhatObj($comment);
                        /*foreach ($cs->getComments() as $key => $comment) {
                            foreach ($cs->getComments() as $key => $comment) {
                                if($comment->getId() == $notif->getWhatId()){
                                    $notif->setWhatObj($comment);
                                    break;
                                }

                            if($comment->getId() == $notif->getWhatId()){
                                $notif->setWhatObj($comment);
                                break;
                            }
                        }*/
                    }
                    if($notif->getWhatType() == "page"){
                        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
                        $whatObj = $pageRepo->findOneBy(array('id' => new \MongoId($notif->getWhatId())));
                        $notif->setWhatObj($whatObj);
                        //dd($whatObj->getName());
                        
                    }
                }                
            }
        }

        $data = array("requests" => array(
        					"friendsRequests" => $friendsRequests,
                            "memberRequests" => $memberRequests,
                            "adminRequests" => $adminRequests,
        					"requestsCount" => count($friendsRequests) + count($memberRequests) + count($adminRequests)
        				),
    				  "tchat" => array("newMessages" => array(),
    				  				   "newMessagesCount" => 0
    				  	),
    				  "notifications" => array("notifications" => $notifs,
    				  				   		   "notificationsCount" => $notCheckedNotifsCount)//count($notifs))
    				  );
        //dd($data);

        $htmlNotif = $this->renderView('notification/all-activity.html.twig', $data);

        //------------------------------
        //RECUP INFO MESSAGE TOREAD CHAT
        //------------------------------
        $chatContactsSlug = $this->objsToSlug($myUserPage->getRelations()->getChatContacts());
        
        $chatRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(ChatMessage::class);
        $messages = $chatRepo->findBy(array('$or' => array(
                                            array('$and' => array(array('receiverSlug' => $myUserPage->getSlug()),
                                                                  array("toRead" => true)),
                                                              ),
                                            /*array('$and' => array(array('receiverSlug' => array('$in'=>$chatContactsSlug)),
                                                                  array("toRead" => true),
                                                                  array("senderSlug" =>  array('$ne'=>$myUserPage->getSlug()))
                                                                    ),
                                                              ),*/
                                            ))
                                            , array("created" => "DESC"), 100);
        $messagesToRead = array();
        //dd($messages);
        if($messages != null and !empty($messages)){
            foreach ($messages as $key => $value) {
                $senderSlug = $value->getSenderSlug();
                $receiverSlug = $value->getReceiverSlug();

                $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

                $sender = $pageRepo->findOneBy(array('slug' => $senderSlug));
                $receiver = $pageRepo->findOneBy(array('slug' => $receiverSlug));
                
                $destSlug = $senderSlug; $dest = $sender;
                if($receiver->getType() != Page::TYPE_USER){
                    $destSlug = $receiverSlug; $dest = $receiver;
                }
                
                if(!isset($messagesToRead[$destSlug])){
                    if($dest != null){
                        $myUserPage->getRelations()->addChatContacts($dest);
                        $this->get('doctrine_mongodb')->getManager()->flush($myUserPage);

                        $messagesToRead[$destSlug] = array('count' => 1,
                                                             'senderSlug' => $dest->getSlug(),
                                                             'senderUsername' => $dest->getName(),
                                                             'senderStatus' => $dest->getChatStatus() ,
                                                             'senderThumb' => $dest->getImageProfil() );
                    }
                }
                else{
                    $messagesToRead[$destSlug]['count'] = $messagesToRead[$destSlug]['count'] + 1;
                }
            }
        }else{
            $messagesToRead = array();
        }

        $status = array();
        $contacts = $myUserPage->getRelations()->getChatContacts();
        foreach ($contacts as $key => $contact) {
            $status[$contact->getSlug()] = $contact->getChatStatus();
        }
        //$status[$myUserPage->getSlug()] = $myUserPage->getChatStatus();

        //------------------------------
        //SEND RESULT
        //------------------------------
        $json = array("htmlNotif" => $htmlNotif,
                      "messagesToRead" => $messagesToRead,
                      "status" => $status);

        return $this->json($json);
        //return $this->json(array('error' => !$res));
    }


    private function objsToSlug($array){
      $res = array();
      foreach($array as $key => $val)  { $res[] = $val->getSlug(); }
      return $res;
    }
}
