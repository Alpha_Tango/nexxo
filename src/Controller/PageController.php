<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\SerializerInterface;

use App\Form\PageType;
use App\Form\PageInfoType;
use App\Form\ImageProfilType;
use App\Form\ImageBannerType;
use App\Form\NewsType;
use App\Form\GroupType;
use App\Form\UserPwdType;
use App\Entity\User;
use App\Entity\Page;
use App\Entity\Relation;
use App\Entity\News;
use App\Entity\Group;
use App\Entity\Report;
use App\Entity\Notification;

use App\Services\Helper;

class PageController extends Controller
{    
    /**
     * Route : /search/{search}/{renderPartial}/{startDate}/{endDate}", name="search"
     * @Route("/search/{search}/{renderPartial}/{startDate}/{endDate}", name="search")
     */
    public function search($search="", $renderPartial="false", $startDate="false", $endDate="false", 
                            Request $request, AuthorizationCheckerInterface $authChecker)
    {

        //if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        if($search == "-") $search = "";
        $search = preg_replace("/></", "#", $search);
        
        $myUser = $this->getUser();
        if($myUser != null) $myPosition = $myUser->getMyPosition();
        else $myPosition = [2.6806640625, 47.1897124644842];

        $myUserPage = new Page(); $myUserPage->setIsActive(true);
        if($myUser != null) $myUserPage = $this->getUser()->getMyUserPage();

        $aroundCoordinates = null;
        if($request->query->get('lat') != null && $request->query->get('lon') != null)
        $aroundCoordinates = array(floatval($request->query->get('lat')), 
                                    floatval($request->query->get('lon')));

        $myAgenda = $request->query->get('myagenda') != null ? $request->query->get('myagenda') : false;
        $myEventsLinks = array();
        if($myAgenda == true){
            $myEventsLinks = array();
            $participates = $myUserPage->getRelations()->getParticipateTo()->toArray();
            $myEventsLinks = array_merge($myEventsLinks, $participates);
            $memberOf = $myUserPage->getRelations()->getFriends()->toArray();
            $myEventsLinks = array_merge($myEventsLinks, $memberOf);
            $adminOf = $myUserPage->getRelations()->getIsAdminOf()->toArray();
            $myEventsLinks = array_merge($myEventsLinks, $adminOf);
            $myEventsLinks = $this->objsToId($myEventsLinks);
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        $allRes = $pageRepo->getGlobalSearch($search,  
                                          $request->query->get('types'), 
                                          $aroundCoordinates,
                                          floatval($request->query->get('radius')),
                                          $startDate, 
                                          $endDate,
                                          $myAgenda,
                                          $myEventsLinks
                                        );
        $res = $allRes["res"];
        $totalCount = $allRes["totalCount"];

        if($aroundCoordinates != null && $renderPartial!="json"){
            foreach ($res as $key => $page) {
                if(!$page->isConfAuth("SHOW_GEOPOS", $myUserPage))
                    unset($res[$key]);
            }
        }

        foreach ($res as $key => $page) {
            if($page->getParentPage() != null 
            and $page->getParentPage()->getType() != "user") 
                unset($res[$key]);
        }

        $currentUserPage = new Page();
        $currentUserPage->setCoordinates(array(2.377594901489258, 46.13417004624326));
        if ($authChecker->isGranted('ROLE_USER')){
            $currentUserPage = $this->getUser()->getMyUserPage();
        }
    
        $jsonRes = $this->getJsonRes($res, $myUserPage);
        $params = array(
                "results" => $res,
                "totalCount" => $totalCount,
                'jsonRes' => $jsonRes,
                "yourSearch" => $search,
                'myPosition' => $myPosition,
                'typeSearch' => Page::getTypeMainSearch(),
                'currentUserPage' => $currentUserPage
            );

        if(!empty($request->query->get('types')))   $params["typesSearch"] = $request->query->get('types');
        if($aroundCoordinates != null)              $params["coordinatesSearch"] = $aroundCoordinates;
        if(!empty($request->query->get('radius')))  $params["radiusSearch"] = floatval($request->query->get('radius'));
       
        $params["currentUserPage"] = $currentUserPage;
        if ($authChecker->isGranted('ROLE_USER')){
            $params["extends"] = "theme/base-app.html.twig";
        }else{
            $params["extends"] = "theme/base-app-offline.html.twig";
        }
         
        //dd($jsonRes);
        if($renderPartial=="false"){
            return $this->render('page/search.html.twig', $params);
        }elseif($renderPartial=="json"){
            return $this->json(array('jsonRes' => $jsonRes));
        }else{
            return $this->render('page/blocks/search-res-list.html.twig', $params);
        }
    }

    /**
     * Route : /agora/{search}/{renderPartial}/{startDate}/{endDate}", name="agora" 
     * @Route("/agora/{search}/{renderPartial}/{startDate}/{endDate}", name="agora")
     */
    public function agora($search="", $renderPartial="false", $startDate="false", $endDate="false", 
                            Request $request, AuthorizationCheckerInterface $authChecker)
    {

        //if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        if($search == "-") $search = "";
        $search = preg_replace("/></", "#", $search);
        
        $currentUserPage = new Page();
        $currentUserPage->setCoordinates(array(2.377594901489258, 46.13417004624326));
        if ($authChecker->isGranted('ROLE_USER')){
            $currentUserPage = $this->getUser()->getMyUserPage();
        }

        $myPosition = $currentUserPage->getCoordinates();
        
        $aroundCoordinates = null;
        if($request->query->get('lat') != null && $request->query->get('lon') != null)
        $aroundCoordinates = array(floatval($request->query->get('lat')), 
                                    floatval($request->query->get('lon')));


        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        $allRes = $pageRepo->getGlobalSearch($search,  
                                          "agora", 
                                          $aroundCoordinates,
                                          floatval($request->query->get('radius'))
                                        );
        $res = $allRes["res"];
        $totalCount = $allRes["totalCount"];
        
        if($aroundCoordinates != null && $renderPartial!="json"){
            foreach ($res as $key => $page) {
                if(!$page->isConfAuth("SHOW_GEOPOS", $currentUserPage))
                    unset($res[$key]);
            }
        }
    
        $jsonRes = $this->getJsonRes($res, $currentUserPage);
        $params = array(
                "results" => $res,
                "totalCount" => $totalCount,
                'jsonRes' => $jsonRes,
                "yourSearch" => $search,
                'myPosition' => $myPosition,
                'typeSearch' => false,
                'forceUnikType' => 'agora'
            );

        if(!empty($request->query->get('types')))   $params["typesSearch"] = "agora";
        if($aroundCoordinates != null)              $params["coordinatesSearch"] = $aroundCoordinates;
        if(!empty($request->query->get('radius')))  $params["radiusSearch"] = floatval($request->query->get('radius'));
       
        $params["currentUserPage"] = $currentUserPage;
        if ($authChecker->isGranted('ROLE_USER')){
            $params["extends"] = "theme/base-app.html.twig";
        }else{
            $params["extends"] = "theme/base-app-offline.html.twig";
        }
         
        //dd($jsonRes);
        if($renderPartial=="false"){
            return $this->render('page/search.html.twig', $params);
        }elseif($renderPartial=="json"){
            return $this->json(array('jsonRes' => $jsonRes));
        }else{
            return $this->render('page/blocks/search-res-list.html.twig', $params);
        }
    }

    /**
     * Route : /channels/{search}/{renderPartial}/{startDate}/{endDate}", name="channels" 
     * @Route("/channels/{search}/{renderPartial}/{startDate}/{endDate}", name="channels")
     */
    public function channels($search="", $renderPartial="false", $startDate="false", $endDate="false", 
                            Request $request, AuthorizationCheckerInterface $authChecker)
    {

        //if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        $currentUserPage = new Page();
        //$currentUserPage->setCoordinates(array(2.377594901489258, 46.13417004624326));
        if ($authChecker->isGranted('ROLE_USER')){
            $currentUserPage = $this->getUser()->getMyUserPage();
        }


        if($search == "-") $search = "";
        $search = preg_replace("/></", "#", $search);
        
        $myUser = $this->getUser();
        $myPosition = $myUser->getMyPosition();
        
        $aroundCoordinates = null;
        if($request->query->get('lat') != null && $request->query->get('lon') != null)
        $aroundCoordinates = array(floatval($request->query->get('lat')), 
                                    floatval($request->query->get('lon')));


        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        $allRes = $pageRepo->getGlobalSearch($search,  
                                          "channel", 
                                          $aroundCoordinates,
                                          floatval($request->query->get('radius'))
                                        );
        $res = $allRes["res"];
        $totalCount = $allRes["totalCount"];
        
        if($aroundCoordinates != null && $renderPartial!="json"){
            foreach ($res as $key => $page) {
                if(!$page->isConfAuth("SHOW_GEOPOS", $currentUserPage))
                    unset($res[$key]);
            }
        }
    
        $jsonRes = $this->getJsonRes($res, $currentUserPage);
        $params = array(
                "results" => $res,
                "totalCount" => $totalCount,
                'jsonRes' => $jsonRes,
                "yourSearch" => $search,
                'myPosition' => $myPosition,
                'typeSearch' => false,
                'forceUnikType' => 'channel'
            );

        if(!empty($request->query->get('types')))   $params["typesSearch"] = "channels";
        if($aroundCoordinates != null)              $params["coordinatesSearch"] = $aroundCoordinates;
        if(!empty($request->query->get('radius')))  $params["radiusSearch"] = floatval($request->query->get('radius'));
       
        $params["currentUserPage"] = $currentUserPage;
        if ($authChecker->isGranted('ROLE_USER')){
            $params["extends"] = "theme/base-app.html.twig";
        }else{
            $params["extends"] = "theme/base-app-offline.html.twig";
        }
         
        //dd($jsonRes);
        if($renderPartial=="false"){
            return $this->render('page/search.html.twig', $params);
        }elseif($renderPartial=="json"){
            return $this->json(array('jsonRes' => $jsonRes));
        }else{
            return $this->render('page/blocks/search-res-list.html.twig', $params);
        }
    }


    /**
     * Route : /assembly/{search}/{renderPartial}/{startDate}/{endDate}", name="assembly" 
     * @Route("/assembly/{search}/{renderPartial}/{startDate}/{endDate}", name="assembly")
     */
    public function assembly($search="", $renderPartial="false", $startDate="false", $endDate="false", 
                            Request $request, AuthorizationCheckerInterface $authChecker)
    {

        //if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        $currentUserPage = new Page();
        $currentUserPage->setCoordinates(array(2.377594901489258, 46.13417004624326));
        if ($authChecker->isGranted('ROLE_USER')){
            $currentUserPage = $this->getUser()->getMyUserPage();
        }

        $myPosition = $currentUserPage->getCoordinates();

        if($search == "-") $search = "";
        $search = preg_replace("/></", "#", $search);
        
        $aroundCoordinates = null;
        if($request->query->get('lat') != null && $request->query->get('lon') != null)
        $aroundCoordinates = array(floatval($request->query->get('lat')), 
                                    floatval($request->query->get('lon')));


        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        $allRes = $pageRepo->getGlobalSearch($search,  
                                          "assembly", 
                                          $aroundCoordinates,
                                          floatval($request->query->get('radius'))
                                        );
        $res = $allRes["res"];
        $totalCount = $allRes["totalCount"];
        
        if($aroundCoordinates != null && $renderPartial!="json"){
            foreach ($res as $key => $page) {
                if(!$page->isConfAuth("SHOW_GEOPOS", $myUser->getMyUserPage()))
                    unset($res[$key]);
            }
        }
    
        $jsonRes = $this->getJsonRes($res, $currentUserPage);
        $params = array(
                "results" => $res,
                "totalCount" => $totalCount,
                'jsonRes' => $jsonRes,
                "yourSearch" => $search,
                'myPosition' => $myPosition,
                'typeSearch' => false,
                'forceUnikType' => 'assembly'
            );

        if(!empty($request->query->get('types')))   $params["typesSearch"] = "assembly";
        if($aroundCoordinates != null)              $params["coordinatesSearch"] = $aroundCoordinates;
        if(!empty($request->query->get('radius')))  $params["radiusSearch"] = floatval($request->query->get('radius'));

        $params["currentUserPage"] = $currentUserPage;
        if ($authChecker->isGranted('ROLE_USER')){
            $params["extends"] = "theme/base-app.html.twig";
        }else{
            $params["extends"] = "theme/base-app-offline.html.twig";
        }

        //dd($jsonRes);
        if($renderPartial=="false"){
            return $this->render('page/search.html.twig', $params);
        }elseif($renderPartial=="json"){
            return $this->json(array('jsonRes' => $jsonRes));
        }else{
            return $this->render('page/blocks/search-res-list.html.twig', $params);
        }
    }


    /**
     * Route : /my-page, name="my-page"<br>
     * Shortcut to redirect to the /page/{slug} of the user connected
     * @Route("/my-page", name="my-page")
     */
    public function mypage(AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUser = $this->getUser();
        $myPage = $myUser->getMyUserPage();
        if($myPage->getFirstStep() > 0){
            return $this->redirectToRoute('first-step');
        }

        return $this->redirectToRoute('page', array("slug"=>$myPage->getSlug()));
    }

    /**
     * Route : /page/quickview/{slug}, name="my-quickview"<br>
     * Display a resume of main information of a Page, pushed in #modal-quickview<br>
     * call with AJAX
     * @Route("/page/quickview/{slug}", name="page-quickview")
     */
    public function quickview($slug, AuthorizationCheckerInterface $authChecker){
        /*if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }*/

        $myUser = $this->getUser();
        $myUserPage = new Page(); 
        $myUserPage->setIsActive(true); 
        $myUserPage->setId(0);
        if($myUser != null) $myUserPage = $myUser->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        $myPosition = $page->getCoordinates();
        
        $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
        $posts = $newsRepo->getPostsPage($page, $myUserPage, 'all', 10);

        return $this->render('page/quickview.html.twig', [
            "page" => $page,
            "posts" => $posts,
            "myPosition" => $myPosition
        ]);
    }
/**
     * Route : /page-online, name="page-online"<br>
     * show the list of users online (currently connected)
     * @Route("/page-online", name="page-online")
     */
    public function online(AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        $date = new \Datetime();
        $date->sub(new \DateInterval('PT60S'));
        
        $pages = $pageRepo->findBy(array("dateOnline" => array('$gt' => $date),
                                         "type" => "user"));
        

        $myUser = $this->getUser();
        $myUserPage = new Page(); 
        $myUserPage->setIsActive(true); 
        $myUserPage->setId(0);
        if($myUser != null) $myUserPage = $myUser->getMyUserPage();

        $jsonRes = $this->getJsonRes($pages, $myUserPage);
        
        return $this->render('page/online.html.twig', [
            "results" => $pages,
            "jsonRes" => $jsonRes,
            "yourSearch"  => "",
            "totalCount" => count($pages),
            "myPosition" => $myUserPage->getCoordinates()
        ]);
    }

    /**
     * Route : /page/{slug}/{view}, name="page"
     * @Route("/page/{slug}/{view}", name="page")
     */
    public function page($slug, $view="main", AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {

            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array("slug" => $slug));

            if($page->isPrivate() == true)
                throw new AccessDeniedException();
            //else
            //    return $this->render('page/page-offline.html.twig', array("page" => $page));
        }

        $currentUserPage = new Page();
        if ($authChecker->isGranted('ROLE_USER')){
            $currentUserPage = $this->getUser()->getMyUserPage();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if($page == null){
            return $this->render('page/page-not-found.html.twig');
        }

        $report = $page->getReport();
        if($report != null && $report->getStatus() == Report::STATUS_DISABLED){
            return $this->render('page/page-disabled.html.twig', array("page"=>$page));
        }

        $groupRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Group::class);
        $groups = $groupRepo->findBy(array("owner" => $page));
       
        $news = new News();
        $formNews = $this->createForm(NewsType::class, $news, array("attr"=>$groups));

        $myPosition = $page->getCoordinates();
        
        $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
        $posts = $newsRepo->getPostsPage($page, $currentUserPage);

        //inc number of view of each post
        $em = $this->get('doctrine_mongodb')->getManager();
        foreach ($posts as $key => $post) $post->incViewCount();
        $em->flush();


        $params = array("page" => $page,
                        "view" => $view,
                        "subview" => "");

        if($view == "main"){
            $params["posts"] = $posts;
            $params["formNews"] = $formNews->createView();
            $params["myPosition"] = $myPosition;
            $params['filter'] = "all";
                
        }

        if($view == "community"){
            $group = new Group();
            $form = $this->createForm(GroupType::class, $group);
            $params["form"] = $form->createView();
        }

        $params["currentUserPage"] = $currentUserPage;
        if ($authChecker->isGranted('ROLE_USER')){
            $params["extends"] = "theme/base-app.html.twig";
        }else{
            $params["extends"] = "theme/base-app-offline.html.twig";
        }

        $view = 'page/page.html.twig';
        if($page->getType() == Page::TYPE_CLASSIFIED) $view = 'market/page.html.twig';
        if($page->getType() == Page::TYPE_MD_DOC) $view = 'page/md-doc.html.twig';
        return $this->render($view, $params);
    }


    /**
     * Route : /create-page/{type}, name="create-page"
     * @Route("/create-page/{type}", name="create-page")
     */
    public function createPage($type="", Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUser = $this->getUser();

        $newPage = new Page();
        $isAdminOf = $this->getUser()->getMyUserPage()->getRelations()->getIsAdminOf()->toArray();
       
        $form = $this->createForm(PageType::class, $newPage, 
                                    array("adminPage"=>$isAdminOf, "pageType"=>$type));
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newPage->setOwner($myUser);
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $slug = Helper::getUniqueSlug($newPage->getName(), $pageRepo);
            $newPage->setSlug($slug);
            
            if($newPage->getLongitude() != null && $newPage->getLatitude() != null)
            $newPage->setCoordinates([(float)$newPage->getLongitude(), (float)$newPage->getLatitude()]);

            $newPage->setCreated(new \Datetime());
            $newPage->setIsActive(true);

            //add the current user page in admins
            $newPage->getRelations()->addAdmin($myUser->getMyUserPage());
            $userPage = $myUser->getMyUserPage();
            $userPage->getRelations()->addIsAdminOf($newPage);

            if($newPage->getType() == Page::TYPE_ASSEMBLY){
                $newPage->setImageBanner("default_assembly.png");
                $newPage->setImageProfil("default_assembly.png");
            }

            // Enregistre la page dans la bdd
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($newPage);
            $em->flush();

            //if the page is not private, send a notification to all my realFriends
            if($newPage->isPrivate() == false){
                /* SEND NOTIFICATION */
                $notifObj = Notification::sendNotification(
                                $this->getUser()->getMyUserPage(), Notification::VERB_CREATE_PAGE,
                                $newPage->getId(), "page", null, null, $em, $this);
            }

            return $this->redirectToRoute('page', array("slug"=>$newPage->getSlug()));
        }else{
            //dd($form->getErrors(true));
        }

        $view = 'page/create-page.html.twig';
        if($type == Page::TYPE_EVENT)
            $view = 'agenda/create-event.html.twig';
        
        return $this->render($view, 
            array('form' => $form->createView(),
                  'action' => 'create-page'));
    }


    /**
     * Route : /create-md-doc/{parentSlug}/{docId}, name="create-md-doc"
     * @Route("/create-md-doc/{parentSlug}/{docId}", name="create-md-doc")
     */
    public function createMDDoc($parentSlug, $docId=null, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();

        $myUser = $this->getUser();
        $pageRepo = $em->getRepository(Page::class);
        $parentPage = $pageRepo->findOneBy(array("slug" => $parentSlug));

        $doctext = $request->request->get('textmd');
        $docTitle = $request->request->get('title');

        $newPage = new Page();
        if($docId != null){
            $pageRepo = $em->getRepository(Page::class);
            $newPage = $pageRepo->findOneBy(array("id" => $docId));

            if(!$this->getUser()->getMyUserPage()->getRelations()->getImMember($newPage->getParentPage()->getSlug()))
                return $this->redirect($this->generateUrl('page', array("slug" => $newPage->getSlug())));
                //return $this->json(array('res' => false, 'error'=> "You are not authorised to edit this document"));
        }

                

        //dump($md);
        if($doctext == null){
            $view = 'page/blocks/create-md-doc.html.twig';
            return $this->render($view, array('parentPage' => $parentPage, 'mddoc' => $newPage));
        }else{


            $newPage->setName($docTitle);
            $newPage->setDescription($doctext);
            if($docId == null){
                $newPage->setOwner($myUser);
                $newPage->setType(Page::TYPE_MD_DOC);
                            
                $newPage->setParentPage($parentPage);
                    
                $slug = Helper::getUniqueSlug($newPage->getName(), $pageRepo);
                $newPage->setSlug($slug);
                    
                $newPage->setCreated(new \Datetime());
                $newPage->setIsActive(true);

                //add the current user page in admins
                $newPage->getRelations()->addAdmin($myUser->getMyUserPage());
                $userPage = $myUser->getMyUserPage();
                $userPage->getRelations()->addIsAdminOf($newPage);

                // Enregistre la page dans la bdd
                $em->persist($newPage);
                $em->flush();

                $newPage->setSlug($newPage->getId());
                //dd($newPage->getId());
                
            }

            $em->flush();

            //if the page is not private, send a notification to all my realFriends
            if($newPage->isPrivate() == false){
                /* SEND NOTIFICATION */
                $notifObj = Notification::sendNotification(
                                $this->getUser()->getMyUserPage(), Notification::VERB_CREATE_PAGE,
                                $newPage->getId(), "page", null, null, $em, $this);
            }

            return $this->json(array('res' => true, 'newPage'=> $newPage, 'docId' => $newPage->getId()));
        
        }

        

    }


    /**
     * Route : /page-settings/{slug}/{subview}, name="page-settings"
     * @Route("/page-settings/{slug}/{subview}", name="page-settings")
     */
    public function settings($slug, $subview="form-main-info", Request $request, AuthorizationCheckerInterface $authChecker, 
                                                                UserPasswordEncoderInterface $passwordEncoder){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $currentUserPage = $this->getUser()->getMyUserPage();

        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if(!$this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($page->getSlug()))
            return $this->redirect($this->generateUrl('page', array("slug" => $slug)));

        
        $form = null;
        if($subview=="form-main-info" || $subview=="form-address-geopos"){
            $isAdminOf = $this->getUser()->getMyUserPage()->getRelations()->getIsAdminOf()->toArray();
            $form = $this->createForm(PageType::class, $page, array("adminPage"=>$isAdminOf, "pageType"=>$page->getType()));
            $action = 'page-settings';
        }

        if($subview=="form-perso-info"){
            $form = $this->createForm(PageInfoType::class, $page->getInformations());
            $action = 'page-settings';
        }

        if($subview=="form-preferences"){;
            $action = 'page-settings';
        }

        if($subview=="form-confidentiality"){
            $page->initConfidentiality();
            $action = 'page-settings';
        }

        /*if($subview=="form-interests"){
            $form = $this->createForm(ImageProfilType::class, $page);
            $action = 'page-settings';
        }

        if($subview=="form-lifepath"){
            $form = $this->createForm(ImageProfilType::class, $page);
            $action = 'page-settings';
        }*/
        
        if($subview=="form-change-password"){
            $user = new User();
            $form = $this->createForm(UserPwdType::class, $user);
            $action = 'page-settings';
        }

        $oldSlug = $page->getSlug();

        if($form != null){
            $form->handleRequest($request); 
            //if($form->isSubmitted()) dd($form->isValid());
            if ($form->isSubmitted() && $form->isValid()) { 
                if($subview=="form-main-info"){
                    $slug = Helper::slugify($page->getName());
                    /* MUST CHANGE THE SLUG IN PRIVATE_MESSAGES */
                    if($oldSlug != $slug){
                        $pageRepo = $em->getRepository(Page::class);
                        $slug = Helper::getUniqueSlug($page->getName(), $pageRepo);
                        $page->setSlug($slug);
                        $page->refactorSlugChatMessage($oldSlug, $em);
                    }
                    /* MUST CHANGE THE SLUG IN PRIVATE_MESSAGES */
                    if($page->getLongitude() != null && $page->getLatitude() != null)
                    $page->setCoordinates([(float)$page->getLongitude(), (float)$page->getLatitude()]);
                }
                elseif($subview=="form-address-geopos"){
                    if($page->getLongitude() != null && $page->getLatitude() != null)
                    $page->setCoordinates([(float)$page->getLongitude(), (float)$page->getLatitude()]);
                }
                elseif($subview=="form-perso-info"){
                    $page->setInformations($form->getData());                
                }
                elseif($subview=="form-change-password"){
                    $passwordValid = $passwordEncoder->isPasswordValid($this->getUser(), $user->getPassword());
                    
                    if($passwordValid == true){
                        $password = $passwordEncoder->encodePassword($this->getUser(), $user->getPlainPassword());
                        $this->getUser()->setPassword($password);
                        $em = $this->get('doctrine_mongodb')->getManager();
                        $em->flush();

                        $request->getSession()->getFlashBag()
                            ->add('success', "Votre mot de passe a été modifié avec succès. Reconnectez-vous dès maintenant pour continuer à naviguer sur Nexxo.");
                        return $this->redirectToRoute('homepage');
                    }else{
                        $request->getSession()->getFlashBag()
                            ->add('error', "Erreur : mot de passe actuel incorrect");
                
                    }
                }

                $em = $this->get('doctrine_mongodb')->getManager();
                $em->flush();

                if($page->getFirstStep() > 0 && $page->getType() == Page::TYPE_USER){
                    $page->incFirstStep();
                    $em->flush($page);
                    return $this->redirectToRoute('first-step');
                }
                else{
                    $params = array('slug' => $slug);
                    return $this->redirectToRoute('page', array("slug" => $slug));
                }
            }else{
                if($form->isSubmitted() && $form->isValid() == false){
                    $request->getSession()->getFlashBag()->add('error', "Erreur : formulaire invalide");
                }
            }
        }

        $params = array(
                  'action' => $action,
                  'page' => $page,
                  'view' => 'settings',
                  'subview' => $subview,
                  'currentUserPage' => $currentUserPage,
                  'extends' => "theme/base-app.html.twig");

        if($form != null){
            $params['form'] = $form->createView();
        }else{
            $params['form'] = false;
        }
        return $this->render('page/page.html.twig', $params);
    }

     /**
     * Route : /save-confidentiality/{slug},  name="save-confidentiality"
     * @Route("/save-confidentiality/{slug}", name="save-confidentiality")
     */
    public function saveConfidentiality($slug, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if(!$this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($page->getSlug()))
            return $this->json(array('error' => true,
                                 'html'  => "Vous devez être admin de cette page pour modifier les paramètres de condidentialité"));

        $conf = $request->request->get('confidentiality');
        $currentConf = $page->getConfidentiality();
        $currentConf[$conf["confkey"]] = $conf["confval"];

        $page->setConfidentiality($currentConf);
        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush();

        //dump($request->request->get('confidentiality'));
        return $this->json(array('error' => false,
                                 'html'  => "Enregistré !"));
    }
     /**
     * Route : /save-preferences/{slug},  name="save-preferences"
     * @Route("/save-preferences/{slug}", name="save-preferences")
     */
    public function savePreferences($slug, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if(!$this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($page->getSlug()))
            return $this->json(array('error' => true,
                                 'html'  => "Vous devez être admin de cette page pour modifier les préférences"));

        $conf = $request->request->get('preferences');
        $currentConf = $page->getPreferences();
        $currentConf[$conf["confkey"]] = $conf["confval"];

        $page->setPreferences($currentConf);
        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush();

        //dump($request->request->get('confidentiality'));
        return $this->json(array('error' => false,
                                 'html'  => "Preference save : ".$conf["confkey"]. " = ".$conf["confval"]));
    }


     /**
     * Route : /save-position/{slug},  name="save-position"
     * @Route("/save-position/{slug}", name="save-position")
     */
    public function savePosition($slug, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if(!$this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($page->getSlug()))
            return $this->json(array('error' => true,
                                 'html'  => "Vous devez être admin de cette page pour modifier sa position"));

        $country = $request->request->get('country');
        $city = $request->request->get('city');
        $streetAddress = $request->request->get('streetAddress');
        $coordinates = $request->request->get('coordinates');
        
        $page->setCountry($country);
        $page->setCity($city);
        $page->setStreetAddress($streetAddress);
        $page->setCoordinates([(float)$coordinates[0], (float)$coordinates[1]]);
        //$page->setCoordinates($coordinates);

        $em->flush();

        //dump($request->request->get('confidentiality'));
        return $this->json(array('error' => false));
    }



    /**
     * Route : /cancel-auth-notification-mail/{token}/{id},  name="cancel-auth-notification-mail"
     * @Route("/cancel-auth-notification-mail/{token}/{id}", name="cancel-auth-notification-mail")
     */
    public function cancelAuthNotificationMail($token, $id, Request $request, AuthorizationCheckerInterface $authChecker){
        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("id" => $id));
        if($page->getOwner()->getToken() == $token){

            $currentConf = $page->getPreferences();
            $currentConf["AUTH_NOTIF_EMAIL"] = "false";

            $page->setPreferences($currentConf);
            $em->flush();

            return $this->render('page/sections/cancel-auth-notification-mail.html.twig', array(
               'page' => $page
            ));

        }else{
            return $this->redirect($this->generateUrl('homepage'));
        }
    }

    


    /**
     * Route : /remove-page/{slug},  name="remove-page"
     * @Route("/remove-page/{slug}", name="remove-page")
     */
    public function removePage($slug, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if(!$this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($page->getSlug()))
            return $this->json(array('error' => true,
                                 'html'  => "Vous devez être admin de cette page pour supprimer cette page"));

        $pageType = $page->getType();

        $res = $page->cleanRemove($em);

        if($pageType == Page::TYPE_USER)
            return $this->redirect($this->generateUrl('logout'));
        else
            return $this->redirect($this->generateUrl('my-page'));
        //return $this->json(array('res' => $res));
        /*
        if($res == true)
            return $this->redirect($this->generateUrl('home'));
        else
            return $this->json(array('res' => $res));*/
    }
    /**
     * Route : /remove-page-block-view/{slug},  name="remove-page-block-view"
     * @Route("/remove-page-block-view/{slug}", name="remove-page-block-view")
     */
    public function removePageBlockView($slug, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        if(!$this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($page->getSlug()))
            return $this->json(array('error' => true,
                                 'html'  => "Vous devez être admin de cette page pour supprimer cette page"));

        return $this->render('page/sections/delete-page.html.twig', array(
           'page' => $page

        ));
    }


     /**
     * Route : /upload-image-profil/{slug}, name="page-upload-image-profil"
     * @Route("/upload-image-profil/{slug}", name="page-upload-image-profil")
     */
    public function uploadImageProfil($slug, Request $request, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        //if its the first image stored, need to create the directory to put the new image
        if(strpos($page->getImageProfil(), "default") !== false){ 
            $output=exec("mkdir ".$this->getParameter('img_profil_directory')."/".$page->getId());
        }

        $form = $this->createForm(ImageProfilType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $page->getImageProfil();

            $fileName = $page->getId()."/".$this->generateUniqueFileName().'.png';

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            //dd($data);
            file_put_contents($this->getParameter('img_profil_directory').'/'.$fileName, $data);
           
            $page->setImageProfil($fileName);
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush();

            return $this->redirect($this->generateUrl('page', array("slug" => $slug)));
        }

        return $this->render('page/forms/form-image-profil.html.twig', array(
            'form' => $form->createView(), 'page' => $page

        ));
    }

     /**
     * Route : /upload-image-banner/{slug}, name="page-upload-image-banner"
     * @Route("/upload-image-banner/{slug}", name="page-upload-image-banner")
     */
    public function uploadImageBanner($slug, Request $request, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("slug" => $slug));

        //prevent access if the current user is not admin of the page requested
        if($page->getRelations()->getIsAdmin($this->getUser()->getMyUserPage()->getSlug()) == false){
            return $this->redirect($this->generateUrl('page', array("slug" => $page->getSlug())));
        }

        //if its the first image stored, need to create the directory to put the new image
        if($page->getImageBanner() == "default.png"){ 
            $output=exec("mkdir ".$this->getParameter('img_banner_directory')."/".$page->getId());
            $output=exec("mkdir ".$this->getParameter('img_banner_directory')."/min/".$page->getId());
        }

        $form = $this->createForm(ImageBannerType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $page->getImageBanner();
            $fileName    = $page->getId()."/".$this->generateUniqueFileName().'.png';
            $fileNameMin = "/min/".$fileName;

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
 
            file_put_contents($this->getParameter('img_banner_directory').'/'.$fileName, $data);
            Helper::saveImageMin($data, $this->getParameter('img_banner_directory').'/'.$fileNameMin);

            $page->setImageBanner($fileName);

            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush();
            return $this->redirect($this->generateUrl('page', array("slug" => $slug)));
        }

        $view = 'page/forms/form-image-banner.html.twig';
        if($page->getType() == Page::TYPE_CLASSIFIED) 
            $view = 'market/forms/form-image-banner.html.twig';

        return $this->render($view, array(
            'form' => $form->createView(), 'page' => $page

        ));
    }

    /**
     * Route : /save-date-check-notif, name="save-date-check-notif"
     * @Route("/save-date-check-notif", name="save-date-check-notif")
     */
    public function saveDateCheckNotif(Request $request, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();
        $myUserPage->setDateCheckNotif(new \DateTime());
        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush($myUserPage);

        return $this->json(array('error' => false));
    }

    /**
     * Generate a name for Images base one md5 hash
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }


    /**
     * Convert search result objects to Json<br>
     * (to use datas in javascript, mainly for SIG to display datas on map)<br>
     * if you want your page appear in result, set excludeMe to false
     * @return array
     */
    private function getJsonRes($res, $userPage, $excludeMe=true){ 
        $jsonRes = array();
        $myPageId = $this->getUser() != null ? $this->getUser()->getMyUserPage()->getId() : 0;
        foreach ($res as $key => $value) {
            if($excludeMe == false || $value->getId() != $myPageId)
            $jsonRes[] = $value->getJson($userPage);        
        }
        return $jsonRes;
    }


    private function objsToId($array){
      $res = array();
      foreach($array as $key => $val)  { $res[] = new \MongoId($val->getId()); }
      return $res;
    }
}
