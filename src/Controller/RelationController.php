<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Form\UserType;
use App\Form\NewsType;
use App\Entity\User;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\Notification;


class RelationController extends Controller
{
    
    /******************* ADMINS ******************/

    /**
     * Route : /relation/send-request-admin/{slugAskForPage}/{confirmed}, name="send-request-admin"
     * @Route("/relation/send-request-admin/{slugAskForPage}/{confirmed}", name="send-request-admin")
     */
    public function sendRequestAdmin($slugAskForPage, $confirmed="false", Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        //dd($confirmed);
        $myUserPage = $this->getUser()->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));
        
        if($confirmed == "false"){
            if($myUserPage->getRelations()->getImAdminOf($askForPage->getSlug()) == true){
                return $this->render('/page/modals/modal-impossible-request.html.twig', 
                                    array("requestType" => "admin",
                                          "pageRepo" => $pageRepo,
                                          "askForPage" => $askForPage));
            }else{
                return $this->render('/page/modals/modal-confirm-request.html.twig', 
                                    array("requestType" => "admin",
                                          "pageRepo" => $pageRepo,
                                          "askForPage" => $askForPage));
            }
        }

        //dd($askForUserPage);
        $res = $askForPage->getRelations()->addAdminRequest($myUserPage);
        $notif = "";
        if($res == true){
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush();

            /* SEND NOTIFICATION */
            $notifObj = Notification::sendNotification($myUserPage, Notification::VERB_REQUEST_ADMIN,
                                                      $askForPage->getId(), "page", null, null, $em, $this);

            $notif = array("id"=>$notifObj->getId(),
                           "html"=> $this->renderView('notification/item-request.html.twig', 
                                                    array("request" => 
                                                        array("pageRef" => $askForPage, 
                                                              "pageReq" =>  $myUserPage, 
                                                              "reqType" => "admin"), 
                                                          "reqType" => "admin")
                                                    )
                           );

        }

        return $this->json(array('error' => !$res,
                                 'notif' => $notif));
    }

    /**
     * Route : /relation/respond-admin-request/{slugAskForPage}/{response}/{slugPage}, name="respond-admin-request"
     * @Route("/relation/respond-admin-request/{slugAskForPage}/{response}/{slugPage}", name="respond-admin-request")
     */
    public function respondAdminRequest($slugAskForPage, $response, $slugPage=false, 
                                        Request $request, AuthorizationCheckerInterface $authChecker){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        if($slugPage != false) $page = $pageRepo->findOneBy(array("slug" => $slugPage));
        else $page = $this->getUser()->getMyUserPage();

        $myUserPage = $this->getUser()->getMyUserPage();
        
        $requests = $page->getRelations()->getAdminsRequests()->toArray();
        foreach($requests as $key => $request){
            if($request->getSlug() == $slugAskForPage){
                if($response == "true"){
                    //add the userPage who want to be admin to the admin list of the page
                    $page->getRelations()->addAdmin($request);
                    //add the page in adminOf list of the userPage who requested
                    $request->getRelations()->addIsAdminOf($page);
                    //remove the request
                    $page->getRelations()->removeAdminRequest($request);
                    //save it in db
                    $em = $this->get('doctrine_mongodb')->getManager();
                    $em->flush($page);
                    $em->flush($request);

                    /* SEND NOTIFICATION */
                    $notifObj = Notification::sendNotification($myUserPage, Notification::VERB_REQUEST_ADMIN_ACCEPTED,
                                                               $page->getId(), "page", $request->getId(), "page", $em, $this);

                    $notif = array("id"=>$notifObj->getId(),
                                   "html"=> $this->renderView('notification/item-notification.html.twig', 
                                                                array('notif' => $notifObj)
                                                            )
                                   );

                    return $this->json(array('error' => false,
                                             'notif' => $notif,
                                             'msg' => 'ok, admin request accepted'));
                }

                $page->getRelations()->removeAdminRequest($request);
                $em = $this->get('doctrine_mongodb')->getManager();
                $em->flush();
                return $this->json(array('error' => false,
                                         'msg' => 'ok, admin request rejected'));
            }
        }
        return $this->json(array('error' => true,
                                 'msg' => 'this admin request not exists'));

    }

    /******************* FRIENDS / MEMBERS ******************/

    /**
     * Route : /relation/send-request-friend/{slugAskForPage}/{confirmed}/{slugPage}, name="send-request-friend"
     * @Route("/relation/send-request-friend/{slugAskForPage}/{confirmed}/{slugPage}", name="send-request-friend")
     */
    public function sendRequestFriend($slugAskForPage, $confirmed="false", $slugPage="", 
                    Request $request, AuthorizationCheckerInterface $authChecker){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        //dd($confirmed);
        $myUserPage = $this->getUser()->getMyUserPage();


        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));


        if($slugPage != "")
          $myUserPage = $pageRepo->findOneBy(array("slug" => $slugPage));

        if($confirmed == "false"){
            if($askForPage->getRelations()->getImFriend($myUserPage->getSlug()) == true){
                return $this->render('/page/modals/modal-impossible-request.html.twig', 
                                    array("requestType" => "friend",
                                          "pageRepo" => $pageRepo,
                                          "askForPage" => $askForPage,
                                          "originPage" => $myUserPage,
                                          "originSlug" => $slugPage));
            }else{
                return $this->render('/page/modals/modal-confirm-request.html.twig', 
                                    array("requestType" => "friend",
                                          "pageRepo" => $pageRepo,
                                          "askForPage" => $askForPage));
            }
        }

        //dd($askForUserPage);
        $res = $askForPage->getRelations()->addFriendRequest($myUserPage);
        $notif = "";

        if($res == true){
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush();

            /* SEND NOTIFICATION */
            $notifObj = Notification::sendNotification($myUserPage, Notification::VERB_REQUEST_FRIEND,
                                                      $askForPage->getId(), "page", null, null, $em, $this);

            $reqType = $askForPage->getType() == Page::TYPE_USER ? "friend" : "member";
            
            $notif = array("id"=>$notifObj->getId(),
                           "html"=> $this->renderView('notification/item-request.html.twig', 
                                                    array("request" => 
                                                        array("pageRef" => $askForPage, 
                                                              "pageReq" =>  $myUserPage, 
                                                              "reqType" => $reqType), 
                                                          "reqType" => $reqType)
                                                    )
                           );
        }

        return $this->json(array('error' => !$res,
                                 'notif' => $notif));
    }

    /**
     * Route : /relation/respond-friend-request/{slugAskForPage}/{response}/{slugPage}, name="respond-friend-request"
     * @Route("/relation/respond-friend-request/{slugAskForPage}/{response}/{slugPage}", name="respond-friend-request")
     */
    public function respondFriendRequest($slugAskForPage, $response, $slugPage=false, 
                                        Request $request, AuthorizationCheckerInterface $authChecker){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

    		if($slugPage != false) $page = $pageRepo->findOneBy(array("slug" => $slugPage));
        else $page = $this->getUser()->getMyUserPage();
    		
        $myUserPage = $this->getUser()->getMyUserPage();

        $requests = $page->getRelations()->getFriendsRequests()->toArray();
        foreach($requests as $key => $request){
        	if($request->getSlug() == $slugAskForPage){
        		if($response == "true"){
	        		$page->getRelations()->addFriend($request);
              $request->getRelations()->addFriend($page);

        			$page->getRelations()->removeFriendRequest($request);
           
              $em = $this->get('doctrine_mongodb')->getManager();
              $em->flush($page);
              $em->flush($request);

              /* SEND NOTIFICATION */
              $notifObj = Notification::sendNotification($myUserPage, Notification::VERB_REQUEST_FRIEND_ACCEPTED,
                                                         $page->getId(), "page", $request->getId(), "page", $em, $this);

              $notif = array("id"=>$notifObj->getId(),
                             "html"=> $this->renderView('notification/item-notification.html.twig', 
                                                          array('notif' => $notifObj)
                                                      ));

	        		return $this->json(array('error' => false,
                                             'notif' => $notif,
                                             'pageType' => $page->getType(),
	        								 'msg' => 'ok, friend request accepted'));
	        	}
        		$page->getRelations()->removeFriendRequest($request);
        		$em = $this->get('doctrine_mongodb')->getManager();
	        	$em->flush();
        		return $this->json(array('error' => false,
                                         'pageType' => $page->getType(),
        								 'msg' => 'ok, friend request rejected'));
        	}
        }
        return $this->json(array('error' => true,
        						 'msg' => 'this friend request not exists'));

    }


    /******************* FOLLOWS ******************/

    /**
     * Route : /relation/send-request-follow/{slugAskForPage}/{confirmed}, name="send-request-follow"
     * @Route("/relation/send-request-follow/{slugAskForPage}/{confirmed}", name="send-request-follow")
     */
    public function sendRequestFollow($slugAskForPage, $confirmed, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));

        if($confirmed == "false"){
            if($myUserPage->getRelations()->inFollows($askForPage->getSlug()) == true){
                return $this->render('/page/modals/modal-impossible-request.html.twig', 
                                    array("requestType" => "follow",
                                          "pageRepo" => $pageRepo,
                                          "askForPage" => $askForPage));
            }else{
            return $this->render('/page/modals/modal-confirm-request.html.twig', array( "requestType" => "follow",
                                                                                      "pageRepo" => $pageRepo,
                                                                                      "askForPage" => $askForPage));
            }
        }

        $action = "";
        if($myUserPage->getRelations()->inFollows($askForPage->getSlug()) == false){
            $res = $askForPage->getRelations()->addFollower($myUserPage);//dump($res);
            $res = $myUserPage->getRelations()->addFollows($askForPage);//dd($res);
            $action = "add";
            
        }else{
            $res = $askForPage->getRelations()->removeFollower($myUserPage);//dump($res);
            $res = $myUserPage->getRelations()->removeFollow($askForPage);//dd($res);
            $action = "remove";
            
        }

        //dd($res);
        $notif = "";
        if($action == "add"){
            /* SEND NOTIFICATION */
            $notifObj = Notification::sendNotification($myUserPage, Notification::VERB_BECOME_FOLLOWER,
                                                      $askForPage->getId(), "page", null, $askForPage->getType(), $em, $this);

            $notif = array("id"=>$notifObj->getId(),
                           "newTotal"=>count($notifObj->getAboutObj()->getRelations()->getFollowers()),
                           "html"=> $this->renderView('notification/item-notification.html.twig',
                                                      array('notif' => $notifObj)));
        }
        
        //add my page in followers of the page
        if($res == true){
            $em->flush($myUserPage);
            $em->flush($askForPage);
        }

        return $this->json(array('error' => !$res, 'action' => $action, 'notif' => $notif));
    }


    /******************* PARTICIPATE ******************/

    /**
     * Route : /relation/send-request-participate/{slugAskForPage}/{confirmed}, name="send-request-participate"
     * @Route("/relation/send-request-participate/{slugAskForPage}/{confirmed}", name="send-request-participate")
     */
    public function sendRequestParticipate($slugAskForPage, $confirmed, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));

        if($confirmed == "false"){
            if($askForPage->getRelations()->getInParticipants($myUserPage->getSlug()) == true){
                return $this->render('page/modals/modal-impossible-request.html.twig', 
                                    array("requestType" => "participate",
                                          "pageRepo" => $pageRepo,
                                          "askForPage" => $askForPage));
            }else{
            return $this->render('page/modals/modal-confirm-request.html.twig', array( "requestType" => "participate",
                                                                                      "pageRepo" => $pageRepo,
                                                                                      "askForPage" => $askForPage));
            }
        }
        //dd($askForUserPage);
        //add my page in followers of the page
       
        if($myUserPage->getRelations()->getInParticipants($askForPage->getSlug()) == false){
            $res = $askForPage->getRelations()->addParticipant($myUserPage);
            $res = $myUserPage->getRelations()->addParticipateTo($askForPage);
            $action = "add";
        }else{
            $res = $askForPage->getRelations()->removeParticipant($myUserPage);
            $res = $myUserPage->getRelations()->removeParticipateTo($askForPage);
            $action = "remove";
        }
        

        //dd($action);
        $notif = "";
        if($action == "add"){
            /* SEND NOTIFICATION */
            $notifObj = Notification::sendNotification($myUserPage, Notification::VERB_BECOME_PARTICIPANT,
                                                      $askForPage->getId(), "page", null, null, $em, $this);

            $notif = array("id"=>$notifObj->getId(),
                           "newTotal"=>count($notifObj->getAboutObj()->getRelations()->getParticipants()),
                           "html"=> $this->renderView('notification/item-notification.html.twig',
                                                      array('notif' => $notifObj)));
        }
        
        if($res == true){
            $em->flush($myUserPage);
            $em->flush($askForPage);
        }
        

        return $this->json(array('error' => !$res, 
                                 'notif' => $notif,
                                 'action' => $action ));
    }



    /******************* LIKES ******************/

    /**
     * Route : /relation/send-request-like/{slugAskForPage}/{confirmed}, name="send-request-like"
     * @Route("/relation/send-request-like/{slugAskForPage}/{confirmed}", name="send-request-like")
     */
    public function sendRequestLike($slugAskForPage, $confirmed, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));

        if($confirmed == "false"){
            if($askForPage->getRelations()->getILike($myUserPage->getSlug()) == true){
                return $this->render('page/modals/modal-impossible-request.html.twig', 
                                    array("requestType" => "like",
                                          "pageRepo" => $pageRepo,
                                          "askForPage" => $askForPage));
            }else{
            return $this->render('page/modals/modal-impossible-request.html.twig', array( "requestType" => "like",
                                                                                          "pageRepo" => $pageRepo,
                                                                                          "askForPage" => $askForPage));
            }
        }
        //dd($askForUserPage);
        //add my page in likers of the page
        //dd($askForPage->getRelations()->getILike($myUserPage->getSlug()));
        $action = "";

        //dump("add or remove ?");
        if($myUserPage->getRelations()->getILike($askForPage->getSlug()) == false){
            $res = $askForPage->getRelations()->addLiker($myUserPage);//dump($res);
            $res = $myUserPage->getRelations()->addLike($askForPage);//dd($res);
            $action = "add";
            
        }else{
            $res = $askForPage->getRelations()->removeLiker($myUserPage);
            $res = $myUserPage->getRelations()->removeLike($askForPage);
            $action = "remove";
            
        }

        $notif = "";

        if($res == true){
            //add the page in my likes
            //$res = $myUserPage->getRelations()->addLike($askForPage);
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush($myUserPage);
            $em->flush($askForPage);

            if($action == "add"){
                /* SEND NOTIFICATION */
                $notifObj = Notification::sendNotification($myUserPage, Notification::VERB_LIKE,
                                                          $askForPage->getId(), "page", null, null, $em, $this);
                $notif = array("id"=>$notifObj->getId(),
                               "newTotal"=>count($notifObj->getAboutObj()->getRelations()->getLikers()),
                               "html"=> $this->renderView('notification/item-notification.html.twig',
                                                          array('notif' => $notifObj)));
            }
        }

        return $this->json(array('error' => !$res, 
                                 'action' => $action, 
                                 'notif' => $notif));
    }

    


    /******************* CANCEL ******************/


    /**
     * Route : /relation/cancel-admin/{slugAskForPage}, name="cancel-admin"
     * @Route("/relation/cancel-admin/{slugAskForPage}", name="cancel-admin")
     */
    public function cancelAdmin($slugAskForPage, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));

        if($myUserPage->getRelations()->getImAdminOf($askForPage->getSlug()) == false)
          return $this->json(array('error' => true, 'errorMsg' => "You are not allowed to do that"));


        $askForPage->getRelations()->removeAdmin($myUserPage);
        $myUserPage->getRelations()->removeIsAdminOf($askForPage);

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush();

        return $this->json(array('error' => false));
    }    

    /**
     * Route : /relation/cancel-friend/{slugAskForPage}/{slugPage}, name="cancel-friend"
     * @Route("/relation/cancel-friend/{slugAskForPage}/{slugPage}", name="cancel-friend")
     */
    public function cancelFriend($slugAskForPage, $slugPage, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));



        if($slugPage != ""){
          if($myUserPage->getRelations()->getImAdminOf($slugPage) == false)
            return $this->json(array('error' => true, 'errorMsg' => "You are not allowed to do that"));

          $myUserPage = $pageRepo->findOneBy(array("slug" => $slugPage));
        }else{
          if($myUserPage->getRelations()->getImAdminOf($askForPage->getSlug()) == false)
            return $this->json(array('error' => true, 'errorMsg' => "You are not allowed to do that"));
        }

        $askForPage->getRelations()->removeFriend($myUserPage);
        $myUserPage->getRelations()->removeFriend($askForPage);

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush();

        return $this->json(array('error' => false));
    }    

    /**
     * Route : /relation/cancel-follow/{slugAskForPage}, name="cancel-follow"
     * @Route("/relation/cancel-follow/{slugAskForPage}", name="cancel-follow")
     */
    public function cancelFollow($slugAskForPage, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));

        $askForPage->getRelations()->removeFollower($myUserPage);
        $myUserPage->getRelations()->removeFollow($askForPage);

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush();

        return $this->json(array('error' => false));
    }    

    /**
     * Route : /relation/cancel-participate/{slugAskForPage}, name="cancel-participate"
     * @Route("/relation/cancel-participate/{slugAskForPage}", name="cancel-participate")
     */
    public function cancelParticipate($slugAskForPage, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));

        $askForPage->getRelations()->removeParticipant($myUserPage);
        $myUserPage->getRelations()->removeParticipateTo($askForPage);

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush();

        return $this->json(array('error' => false));
    }    

    /**
     * Route : /relation/cancel-like/{slugAskForPage}, name="cancel-like"
     * @Route("/relation/cancel-like/{slugAskForPage}", name="cancel-like")
     */
    public function cancelLike($slugAskForPage, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));

        $askForPage->getRelations()->removeLiker($myUserPage);
        $myUserPage->getRelations()->removeLike($askForPage);

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush();

        return $this->json(array('error' => false));
    }   

    /**
     * Route : /relation/add-chat-contact/{slugAskForPage}, name="add-chat-contact"
     * @Route("/relation/add-chat-contact/{slugAskForPage}", name="add-chat-contact")
     */
    public function addChatContact($slugAskForPage, Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $askForPage = $pageRepo->findOneBy(array("slug" => $slugAskForPage));

        $res = false;
        if($askForPage->isConfAuth("AUTH_CHAT", $myUserPage)){
            $res = $myUserPage->getRelations()->addChatContacts($askForPage);
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush();
        }

        return $this->json(array('error' => !$res));
    }    


    /**
     * Route : /relation/bw-list/{pageId}/{bwType}, name="bw-list"
     * @Route("/relation/bw-list/{pageId}/{bwType}", name="bw-list")
     */
    public function bwList($pageId, $bwType, Request $request, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUserPage = $this->getUser()->getMyUserPage();

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        $page = $pageRepo->findOneBy(array("id" => $pageId));

        $removed = false;
        $myRelation = $myUserPage->getRelations();
                    
        if($bwType == "whitelist"){
          if(!$myRelation->getInWhiteList($page->getSlug())){
            $myRelation->addWhiteList($page);
          }
          else{
            $myRelation->removeWhiteList($page); $removed = true;
          }
        }elseif($bwType == "blacklist"){
          if(!$myRelation->getInBlackList($page->getSlug())){
            $myRelation->addBlackList($page); 
          }
          else{
            $myRelation->removeBlackList($page);  $removed = true;
          }
        }

        $myUserPage->setRelations($myRelation);

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush($myUserPage);

        return $this->json(array('error' => false, 'removed' =>  $removed));
    }


}