<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;

use App\Form\GroupType;
use App\Entity\User;
use App\Entity\Page;
use App\Entity\Group;

class GroupController extends Controller
{
    /**
     * @Route("/my-groups", name="my-groups")
     */
    public function index(Request $request, AuthorizationCheckerInterface $authChecker){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $currentUserPage = $this->getUser()->getMyUserPage();
        $myPage = $this->getUser()->getMyUserPage();
        $group = new Group();
        $form = $this->createForm(GroupType::class, $group);
        
        return $this->render('group/group.html.twig',
            array('form' => $form->createView(),
                  'page' => $myPage,
                  'currentUserPage' => $currentUserPage));
    }

    /**
     * @Route("/group/{id}", name="group")
     */
    public function group($id, Request $request, AuthorizationCheckerInterface $authChecker, TranslatorInterface $translator){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myUser = $this->getUser();
        $myPage = $myUser->getMyUserPage();
        
        $groupRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Group::class);
        $group = $groupRepo->findOneBy(array("id" => $id));
        
        //dd($group->getOwner());
        
        if($group != null && $this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($group->getOwner()->getSlug())){
            return $this->render('group/group-content.html.twig', array('group' => $group));
        }else{
            return $this->json(array('error' => true,
                                     'errorMsg' => $translator->trans('An error occured', array(), "group")));
        }
        
    }

    /**
     * @Route("/create-group/{slugOwner}", name="create-group")
     */
    public function createGroup($slugOwner, Request $request, AuthorizationCheckerInterface $authChecker, TranslatorInterface $translator){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $myPage = $this->getUser()->getMyUserPage();
        
        $group = new Group();
        $form = $this->createForm(GroupType::class, $group);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $pageOwner = $pageRepo->findOneBy(array("slug" => $slugOwner));
            $group->setOwner($pageOwner);
            
            // Enregistre le groupe dans la bdd
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($group);
            $em->flush();

            $view = $this->renderView('group/blocks/block-group.html.twig', array('group' => $group));

            return $this->json(array('error' => false,
                                     'html' => $view,
                                     'errorMsg' => $translator->trans('Group created', array(), "group")));

        }

        return $this->json(array('error' => true,
                                 'errorMsg' => $translator->trans('An error occured creating the group', array(), "group")));
    }

    /**
     * @Route("group/add-members/{groupId}/{strIdPages}", name="group-add-members")
     */
    public function addMembers($groupId, $strIdPages, Request $request, 
                                AuthorizationCheckerInterface $authChecker, TranslatorInterface $translator){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        
        $strIdPages = str_replace(' ', '', $strIdPages);
        $idPages = explode(",", $strIdPages);
        
        $myUser = $this->getUser();
        $myPage = $myUser->getMyUserPage();
        
        $groupRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Group::class);
        $group = $groupRepo->findOneBy(array("id" => $groupId));

        $pageOwnGroup = $group->getOwner();
        
         if($myPage->getRelations()->getImAdminOf($pageOwnGroup->getSlug()) == false && 
           $pageOwnGroup->getSlug() != $myPage->getSlug()){
            return $this->json(array('error' => true,
                                     'errorMsg' => 
                                        $translator->trans('Sorry, you are not authorized to manage this group !', array(), "group")));
        }


        foreach ($idPages as $key => $id) {
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array("id" => $id));

            //check if the page id is referenced in friends list of the group page owner
            if($pageOwnGroup->getRelations()->getImFriend($page->getSlug()) == true){
                $group->addMember($page);
            }
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush();

        $view = $this->renderView('group/blocks/block-group.html.twig', array('group' => $group));

        return $this->json(array('error' => false,
                                 'html' => $view,
                                 'errorMsg' => $translator->trans('Member(s) added !', array(), "group")));
    }

    /**
     * @Route("group/delete-members/{groupId}/{strIdPages}", name="group-delete-members")
     */
    public function deleteMembers($groupId, $strIdPages, Request $request, 
                                    AuthorizationCheckerInterface $authChecker, TranslatorInterface $translator){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        
        $strIdPages = str_replace(' ', '', $strIdPages);
        $idPages = explode(",", $strIdPages);
        
        $myUser = $this->getUser();
        $myPage = $myUser->getMyUserPage();
        
        $groupRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Group::class);
        $group = $groupRepo->findOneBy(array("id" => $groupId));

        $pageOwnGroup = $group->getOwner();
        
         if($pageOwnGroup->getRelations()->getImAdminOf($myPage->getSlug()) == false && 
           $pageOwnGroup->getSlug() != $myPage->getSlug()){
            return $this->json(array('error' => true,
                                     'errorMsg' => 
                                        $translator->trans('Sorry, you are not authorized to manage this group !', array(), "group")));
        }


        foreach ($idPages as $key => $id) {
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $page = $pageRepo->findOneBy(array("id" => $id));

            //check if the page id is referenced in friends list of the group page owner
            if($pageOwnGroup->getRelations()->getImFriend($page->getSlug()) == true){
                $group->removeMember($page);
            }
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->flush();

        $view = $this->renderView('group/blocks/block-group.html.twig', array('group' => $group));

        return $this->json(array('error' => false,
                                 'html' => $view,
                                 'errorMsg' => $translator->trans('Member(s) added !', array(), "group")));
    }

    /**
     * @Route("group/remove-group/{groupId}", name="group-remove-group")
     */
    public function removeGroup($groupId, Request $request, AuthorizationCheckerInterface $authChecker, TranslatorInterface $translator){
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
                
        $myUser = $this->getUser();
        $myPage = $myUser->getMyUserPage();
        
        $groupRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Group::class);
        $group = $groupRepo->findOneBy(array("id" => $groupId));

        $pageOwnGroup = $group->getOwner();
        
        if($pageOwnGroup->getRelations()->getImAdminOf($myPage->getSlug()) == false){
            return $this->json(array('error' => true,
                                     'errorMsg' => $translator->trans('Sorry, you are not authorized to manage this group !', array(), "group")));
        }

        $pageOwnGroup->removeGroup($group);

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->remove($group);
        $em->flush();

        return $this->json(array('error' => false,
                                 'errorMsg' => $translator->trans('Group removed !', array(), 'group')));
    }

}
