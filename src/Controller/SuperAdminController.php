<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use App\Entity\Page;
use App\Entity\Group;
use App\Entity\News;
use App\Entity\Report;
use App\Entity\Contact;
use App\Entity\CommentStream;
use App\Entity\Notification;
use App\Entity\AdminEmail;

use App\Services\Mailer;
use App\Form\GlobalMailType;

class SuperAdminController extends Controller
{
    /**
     * @Route("/superadmin", name="superadmin")
     */
    public function index(AuthorizationCheckerInterface $authChecker)
    {
    	if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }else{
        	if($this->getUser()->isSuperAdmin() == true){
               
                $myPage = $this->getUser()->getMyUserPage();
                
                //PAGE COUNT
                $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
                $pageTypes = $myPage->getTypeAvailables();
                //count total of all pages
                $datas = array("pageCounts" => 
                            array("total"=> 
                                array("count" => $pageRepo->getCountPage(""), 
                                      "color" => "", 
                                      "icon" => "circle")
                        ));

                //total of pages by type
                foreach ($pageTypes as $t) {
                   $datas["pageCounts"][$t["type"]]["count"] = $pageRepo->getCountPage($t["type"]);
                   $datas["pageCounts"][$t["type"]]["color"] = $t["color"];
                   $datas["pageCounts"][$t["type"]]["icon"] = $t["icon"];
                }


                $datas["userCountActive"]["count"] = $pageRepo->getCountUserActive();
                $datas["userCountActive"]["color"] = "green";
                $datas["userCountActive"]["icon"] = "user";

                $firstSteps = $pageRepo->getCountFirstStep();
                $datas["userCountFirstStep"]["count"] = count($firstSteps);
                $datas["userCountFirstStep"]["color"] = "green";
                $datas["userCountFirstStep"]["icon"] = "user";
                $datas["userCountFirstStep"]["details"] = array();
                
                foreach ($firstSteps as $key => $page) {
                  if(!isset($datas["userCountFirstStep"]["details"][$page->getFirstStep()]))
                    $datas["userCountFirstStep"]["details"][$page->getFirstStep()] = 0;


                  $datas["userCountFirstStep"]["details"][$page->getFirstStep()]++;
                }
                //dd($datas["userCountFirstStep"]);

                //NEWS COUNT
                $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
                $datas["newsCounts"]["total"]["count"] = $newsRepo->getCount("");
                foreach (array("localised", "friends", "followers", "groups") as $scopeType) {
                   $datas["newsCounts"][$scopeType]["count"] = $newsRepo->getCount($scopeType);
                   $datas["newsCounts"][$scopeType]["color"] = "blue";
                   $datas["newsCounts"][$scopeType]["icon"] = "newspaper-o";
                }

                $or = array(array('status'=>Report::STATUS_OPEN), 
                            array('status'=>Report::STATUS_VOTE_OPEN));
                //REPORT PAGES
                $reportRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Report::class);
                $reports = $reportRepo->findBy(array('$or'=>$or, "aboutType"=>"page"));
                foreach($reports as $key => $report){
                  $repo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
                  $datas["reportsPages"][] = $repo->findOneBy(array("id" => $report->getAboutId()));
                }
                //REPORT NEWS
                $reportRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Report::class);
                $reports = $reportRepo->findBy(array('$or'=>$or, "aboutType"=>"news", "commentId"=>array('$exists'=>false)));
                foreach($reports as $key => $report){
                  $repo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
                  $datas["reportsNews"][] = $repo->findOneBy(array("id" => $report->getAboutId()));
                }
                //REPORT COMMENTS
                $reportRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Report::class);
                $reports = $reportRepo->findBy(array('$or'=>$or, "commentId"=>array('$exists'=>true)));
                foreach($reports as $key => $report){
                  $repo = $this->get('doctrine_mongodb')->getManager()->getRepository(CommentStream::class);
                  $cs = $repo->findOneBy(array("parentId" => $report->getAboutId(), 
                                               "parentType" => $report->getAboutType()));
                    $datas["reportsComments"][] = $cs->getCommentById($report->getCommentId());
                  
                }

                $date = new \Datetime();
                $date->sub(new \DateInterval('P7D'));
                $newWeekUsers = $pageRepo->findBy(array('type'=> Page::TYPE_USER ,
                                                        'created' => array('$gte' => $date) ));

                $newWeekClassified = $pageRepo->findBy(array('type'=> Page::TYPE_CLASSIFIED ,
                                                             'created' => array('$gte' => $date) ));

                $newWeekEvent = $pageRepo->findBy(array('type'=> Page::TYPE_EVENT ,
                                                        'created' => array('$gte' => $date) ));

                $newWeekNews = $newsRepo->findBy(array( 'created' => array('$gte' => $date) ));

                $newWeekNewsGeo = $newsRepo->findBy(array( 'scopeType' => 'localised',
                                                           'created' => array('$gte' => $date) ));


                $datas["newWeekUsers"] = $newWeekUsers;
                $datas["newWeekClassified"] = $newWeekClassified;
                $datas["newWeekEvent"] = $newWeekEvent;
                $datas["newWeekNews"] = $newWeekNews;
                $datas["newWeekNewsGeo"] = $newWeekNewsGeo;

                //MESSAGES CONTACTS
                $contactRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Contact::class);
                $contacts = $contactRepo->findBy(array('toRead'=>true));
                //$datas["contactNotRead"] = $reportRepo->findBy(array('toRead'=>true));
                $datas["contactNotReadCount"] = count($contacts);

	        	return $this->render('superadmin/index.html.twig', $datas);
          }
	        else{
	        	return $this->redirectToRoute('my-page');
          }
        }
        

    }

    /**
     * @Route("/superadmin-contacts", name="superadmin-contacts")
     */
    public function superAdminContacts(AuthorizationCheckerInterface $authChecker)
    {
      //dd("not1 loged");
      if (!$authChecker->isGranted('ROLE_USER')) {
          //dd("not loged");
          throw new AccessDeniedException();
      }else{
        //dd("loged");
        if($this->getUser()->isSuperAdmin() == true){
         //dd("admin");
          
          
          $contactRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Contact::class);
          $contacts = $contactRepo->findBy(array(), array("created"=>"DESC"), 100);

          return $this->render('superadmin/contacts.html.twig', array("contacts" => $contacts));

        }
        else{
          return $this->redirectToRoute('my-page');
        }
      }

    }

    /**
     * @Route("/superadmin/set-contact-read/{id}", name="superadmin-set-contact-read")
     */
    public function superadminSetContactRead($id, AuthorizationCheckerInterface $authChecker)
    {
      if (!$authChecker->isGranted('ROLE_USER')) {
          throw new AccessDeniedException();
      }else{
        if($this->getUser()->isSuperAdmin() == true){
          
          $contactRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Contact::class);
          $contact = $contactRepo->findOneBy(array("id"=>$id));

          if($contact != null){
            $contact->setToRead(false);
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->flush();

            return $this->json(array('error' => false));
          }else{
            return $this->json(array('error' => true, 
                                     'msg' => 'contact not found'));
          }
        }
        else{
          return $this->json(array('error' => true, 
                                   'msg' => 'You are not administrator (mother fucker !)'));
        }
      }

    }

    /**
     * @Route("/check-moderation", name="check-moderation")
     */
    public function checkModeration(AuthorizationCheckerInterface $authChecker)
    {
      if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }else{
          if($this->getUser()->isSuperAdmin() == true){
           
            $date = new \Datetime();
            //dump($date);
            $date->sub(new \DateInterval('P3D'));
            //dump($date);
            $reportRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Report::class);
            $reports = $reportRepo->findBy(array("status"=>"OPEN", 
                                                 "created"=>array('$lte' => $date) 
                                          ));

            return $this->json(array('error' => false, 
                                     'msg' => count($reports).' élément signalés doivent être checkés'));

          }
        }
    }

    /**
     * @Route("/clean-inactive-pages", name="clean-inactive-pages")
     */
    public function cleanInactivePages(AuthorizationCheckerInterface $authChecker)
    {
      if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }else{
          if($this->getUser()->isSuperAdmin() == true){
           
            $date = new \Datetime();
            $date->sub(new \DateInterval('P7D'));
            
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $pages = $pageRepo->findBy(array("isActive"=>false, 
                                             "created"=>array('$lte' => $date) 
                                          ));

            foreach ($pages as $key => $page) {
             $page->cleanRemove($this->get('doctrine_mongodb')->getManager());
            }

            return $this->json(array('error' => false, 
                                     'msg' => count($pages).' pages ont été supprimées'));

          }
        }
    }


    /**
     * @Route("/form-global-mail", name="form-global-mail")
     */
    
      public function formGlobalMail(Request $request, Mailer $mailer,  AuthorizationCheckerInterface $authChecker, TranslatorInterface $translator){
        
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }else{
          if($this->getUser()->isSuperAdmin() == true){

            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $users = $pageRepo->findBy(array( "type"=>"user" ));

            $contacts = array();
            foreach ($users as $key => $user) {
              $authNotifMail = $user->isPrefAuth("AUTH_NOTIF_EMAIL");
              if($authNotifMail === true || $authNotifMail == "true"){
                //dump($authNotifMail." ".$user->getName());
                $contacts[] = $user;
              }
            }

            $email = new AdminEmail();
            $globalMailForm = $this->createForm(GlobalMailType::class, $email);

            //dd($contacts);
            return $this->render('superadmin/form/global-mail.html.twig', array("contacts" => $contacts, 
                                                                                "form"=>$globalMailForm->createView()));
          }
        }

      }


    /**
     * @Route("/send-global-mail", name="send-global-mail")
     */
    
      public function sendGlobalMail(Request $request, Mailer $mailer,  AuthorizationCheckerInterface $authChecker, TranslatorInterface $translator){
        
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }else{
          if($this->getUser()->isSuperAdmin() == true){

            $email = new AdminEmail();
            $form = $this->createForm(GlobalMailType::class, $email);
            $form->handleRequest($request); 
            
            if ($form->isSubmitted() && $form->isValid()) {
              return $this->render('superadmin/confirm-sent-global-mail.html.twig', 
                                    array("email" => $email,
                                          "userPage" => $this->getUser()->getMyUserPage()));
            }else{

              return $this->render('superadmin/form/global-mail.html.twig', array("contacts" => array(), 
                                                                                "form"=>$form->createView()));
              //return $this->json(array('error' => true, 
              //                       'msg' => ''));
            }

          }


            //dd($contacts);
            return $this->render('superadmin/form/global-mail.html.twig', array("contacts" => array(), 
                                                                                "form"=>$form->createView()));
        }

      }


    /**
     * @Route("/send-mail-notif", name="send-mail-notif")
     */
    
      public function sendMailNotif(Request $request, Mailer $mailer,  
              AuthorizationCheckerInterface $authChecker, TranslatorInterface $translator){
        
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }else if($this->getUser()->isSuperAdmin() == true){

            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            //$pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Notification::class);
            $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
            
            $users = $pageRepo->findBy(array( "type"=>"user" ));

            $contacts = array();
            $ctrl = $this;
            foreach ($users as $key => $user) {
              $authNotifMail = $user->isPrefAuth("AUTH_NOTIF_EMAIL");
              if($authNotifMail === true || $authNotifMail == "true"){
                //dump($authNotifMail." ".$user->getName());
                $contacts[] = $user;

                $allNotifs = $this->getNotificationsByUser($user);

                $date = new \Datetime();
                $date->sub(new \DateInterval('P7D'));
                $newWeekUsers = $pageRepo->findBy(array('type'=> Page::TYPE_USER ,'created' => array('$gte' => $date) ));
                $newWeekClassified = $pageRepo->findBy(array('type'=> Page::TYPE_CLASSIFIED ,'created' => array('$gte' => $date) ));
                $newWeekEvent = $pageRepo->findBy(array('type'=> Page::TYPE_EVENT ,'created' => array('$gte' => $date) ));
                $newWeekNews = $newsRepo->findBy(array('created' => array('$gte' => $date) ));

                $totalNotif = count($allNotifs["notifs"]) + $allNotifs["requestsCount"];

                //return $this->render('notification/mail/notif-mail.html.twig', 
                $bodyMail = $mailer->createBodyMail('notification/mail/notif-mail.html.twig', 
                                    array(//"email" => $email,
                                          "userPage" => $user,
                                          "notifs" => $allNotifs["notifs"],
                                          "friendsRequests" => $allNotifs["friendsRequests"],
                                          "memberRequests" => $allNotifs["memberRequests"],
                                          "adminRequests" => $allNotifs["adminRequests"],
                                          "totalNotif" => $totalNotif,
                                          "newWeekUsers" => $newWeekUsers,
                                          "newWeekClassified" => $newWeekClassified,
                                          "newWeekEvent" => $newWeekEvent,
                                          "newWeekNews" => $newWeekNews));
       

                  $mailer->sendMessage('postmaster@nexxociety.co', "mr.alpha.tango@protonmail.com", //$user->getEmail(), 
                                       $translator->trans('Nexxo : '.$totalNotif.' notifications', array(), "mail"), $bodyMail);
                  //dd($user);
                  return $this->json(array('error' => false, 
                                            'bodyMail' => $bodyMail));
              }
            }

            return $this->render('notification/mail/notif-mail.html.twig', 
                                    array(//"email" => $email,
                                          "userPage" => $this->getUser()->getMyUserPage(),
                                          "totalNotif" => 0,
                                          "totalRequest" => 0));
            
        }

      }

      private function getNotificationsByUser($userPage){
        $notifRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Notification::class);
        $notCheckedNotifs = $notifRepo->findBy(array('targets.$id' => new \MongoId($userPage->getId()),
                                                     'lastAuthor.$id' => array('$ne'=> new \MongoId($userPage->getId())),
                                                     'updated' => array('$gt' => $userPage->getDateCheckNotif())
                                                    ), 
                                                array("updated" => "DESC"));
        
        foreach ($notCheckedNotifs as $key => $notif) {
            if($notif->getLastAuthor()->getIsActive() == false && 
                $notif->getLastAuthor()->getReport() != null ){
                unset($notCheckedNotifs[$key]);
            }
        }

        $notCheckedNotifsCount = count($notCheckedNotifs);
        $limit = $notCheckedNotifsCount < 10 ? 10 : $notCheckedNotifsCount;
        
        $notifs = $notCheckedNotifs;
        // $notifRepo->findBy(array('targets.$id' => new \MongoId($userPage->getId()),
        //                                   'lastAuthor.$id' => array('$ne'=> new \MongoId($userPage->getId()))), 
        //                             array("updated" => "DESC"), $limit);
        
        foreach ($notifs as $key => $notif) {
            //set aboutObj before to send notifications to the view
            $class = Notification::getClass($notif->getAboutType());
            $repo = $this->get('doctrine_mongodb')->getManager()->getRepository($class);
            $aboutObj = $repo->findOneBy(array("id" => $notif->getAboutId()));
            $notif->setAboutObj($aboutObj);

            //do not show notification send by a loked page
            if($notif->getLastAuthor()->getIsActive() == false && 
                $notif->getLastAuthor()->getReport() != null ){
                unset($notifs[$key]);
            }else{
                //set whatObject before to send notifications to the view
                if($notif->getWhatId() != null){
                    //case of comment about news
                    if($notif->getWhatType() == "comment" && $notif->getAboutType() == "news" && $aboutObj != null){
                        //search for whatId (commentId) in commentStream of the news
                        $cs = $aboutObj->getCommentStream(); 
                        $comment = $cs->getCommentById($notif->getWhatId());
                        if($comment != null)
                            $notif->setWhatObj($comment);
                    }
                    if($notif->getWhatType() == "page"){
                        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
                        $whatObj = $pageRepo->findOneBy(array('id' => new \MongoId($notif->getWhatId())));
                        $notif->setWhatObj($whatObj);
                        //dd($whatObj->getName());                        
                    }
                }                
            }
        }

        /* get MY personnal friends requests */
        $myUserPage = $this->getUser()->getMyUserPage();
        $myUserPage->setDateOnline(new \Datetime());
        $this->get('doctrine_mongodb')->getManager()->flush($myUserPage);

        $fReq = $myUserPage->getRelations()->getFriendsRequests();
        $friendsRequests = array();
        foreach($fReq as $key => $req){
          $friendsRequests[] = array("pageRef" => $myUserPage, "pageReq" =>  $req, "reqType" => "friend");
        }
        /* get member request on Page where I'm admin */
        $adminOf = $myUserPage->getRelations()->getIsAdminOf();
        $memberRequests = array();
        foreach($adminOf as $key => $page){
            if($page->getId() != $myUserPage->getId())
            foreach($page->getRelations()->getFriendsRequests() as $key => $req)
                $memberRequests[] = array("pageRef" => $page, "pageReq" =>  $req, "reqType" => "member");
        }
        /* get admin request on Page where I'm admin */
        $adminRequests = array();
        foreach($adminOf as $key => $page){
            foreach($page->getRelations()->getAdminsRequests() as $key => $req)
                $adminRequests[] = array("pageRef" => $page, "pageReq" =>  $req, "reqType" => "admin");
        }

        return array( "notifs" => $notifs,
                      "friendsRequests" => $friendsRequests,
                      "memberRequests" => $memberRequests,
                      "adminRequests" => $adminRequests,
                      "requestsCount" => count($friendsRequests) + count($memberRequests) + count($adminRequests));
      }

    /**
     * @Route("/test-mail", name="test-mail")
     */
    /*
      public function testMail(Request $request, Mailer $mailer,  AuthorizationCheckerInterface $authChecker,
                            TokenGeneratorInterface $tokenGenerator, TranslatorInterface $translator){
        
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }else{
          if($this->getUser()->isSuperAdmin() == true){

            // on utilise le service Mailer créé précédemment
            $bodyMail = $mailer->createBodyMail('theme/account/resetting/mail.html.twig', [
                'user' => $this->getUser()
            ]);
            

            $mailer->sendMessage('postmaster@nexxociety.co', 'mr.alpha.tango@protonmail.com', 
                                 'test', $bodyMail);
            // $to      = $user->getEmail();//'nobody@example.com';
            // $subject = 'the subject';
            // $message = 'Hello';
            // $headers = array(
            //     'From' => 'mr.alpha.tango@protonmail.com',
            //     'Reply-To' => 'mr.alpha.tango@protonmail.com',
            //     'X-Mailer' => 'PHP/' . phpversion()
            // );

            // mail($to, $subject, $message, $headers);

            $request->getSession()->getFlashBag()->add('success', 
                "Un mail va vous être envoyé afin que vous puissiez renouveller votre mot de passe. ".
                "Le lien que vous recevrez sera valide 24h.");

            //return $this->redirectToRoute("superadmin");
            return $this->json(array('error' => false, 
                                     'msg' => ''));
          }
        }

        return $this->json(array('error' => true, 
                                 'msg' => ''));
    }*/
 


   /* private function removePage(Page $page, AuthorizationCheckerInterface $authChecker){
        
        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $newsRepo = $em->getRepository(News::class);
        $groupRepo = $em->getRepository(Group::class);
        $chatMessageRepo = $em->getRepository(ChatMessage::class);
        
        $owner = $page->getOwner();

        // RELATIONS

        foreach ($page->getRelations()->getAdmins() as $key => $pageRel) {
          $pageRel->getRelations()->removeIsAdminOf($page);
        }
        
        foreach ($page->getRelations()->getIsAdminOf() as $key => $pageRel) {
          $pageRel->getRelations()->removeAdmin($page);
        }
              
        foreach ($page->getRelations()->getFriends() as $key => $pageRel) {
          $pageRel->getRelations()->removeFriend($page);
        }
        
        foreach ($page->getRelations()->getFollowers() as $key => $pageRel) {
          $pageRel->getRelations()->removeFollow($page);
        }
        
        foreach ($page->getRelations()->getFollows() as $key => $pageRel) {
          $pageRel->getRelations()->removeFollower($page);
        }

        foreach ($page->getRelations()->getParticipants() as $key => $pageRel) {
          $pageRel->getRelations()->removeParticipateTo($page);
        }

        foreach ($page->getRelations()->getParticipateTo() as $key => $pageRel) {
          $pageRel->getRelations()->removeParticipant($page);
        }

        foreach ($page->getRelations()->getLikers() as $key => $pageRel) {
          $pageRel->getRelations()->removeLike($page);
        }

        foreach ($page->getRelations()->getLikes() as $key => $pageRel) {
          $pageRel->getRelations()->removeLiker($page);
        }

        foreach ($page->getRelations()->getChatContacts() as $key => $pageRel) {
          

          $pageRel->getRelations()->removeChatContacts($page);
        }

        // CHAT MESSAGE
        
        foreach ($page->getGroups() as $key => $groupRel) {
          //$em->remove($groupRel);
          //$em->flush();
        }
        // GROUPS
        
        foreach ($page->getGroups() as $key => $groupRel) {
          //$em->remove($groupRel);
          //$em->flush();
        }

        /*foreach ($page->getInGroups() as $key => $groupRel) {
          $groupRel->removeMember($page);
        }* /



        //change parentPage of all children
        foreach ($page->getChildrenPages() as $key => $pageRel) {
          # code...
        }


        //AND FINALY NOTIFICATIONS !!
    }*/
}