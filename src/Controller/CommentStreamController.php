<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;

use App\Form\CommentType;
use App\Entity\User;
use App\Entity\Page;
use App\Entity\Group;
use App\Entity\News;
use App\Entity\CommentStream;
use App\Entity\Comment;
use App\Entity\Notification;
use App\Entity\Report;

use App\Sockets\Chat;


class CommentStreamController extends Controller
{
    /**
	 * Route : /comment-stream/get-comments/{parentType}/{parentId}", name="get-comments"
	 * @Route("/comment-stream/get-comments/{parentType}/{parentId}", name="get-comments")
	 */
	public function getComments($parentType, $parentId, Request $request, AuthorizationCheckerInterface $authChecker)
	{
 		if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $parentObj = null;
        if($parentType == "news"){
          $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
          $parentObj = $newsRepo->findOneBy(array("id" => new \MongoId($parentId)));
        }


        $commentStreamRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(CommentStream::class);
        $commentStream = $commentStreamRepo->findOneBy(array("parentType" => $parentType,
        											  		                         "parentId" => $parentId));

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        //dd($parentType);
        $showFormBlock = true;
        if($parentType == "report") $showFormBlock = false;
        return $this->render('comment/comment-block.html.twig',
                				array('commentStream' => $commentStream!=null ? $commentStream->getComments(): array(),
                  					  'commentForm' => $form->createView(),
                  					  'parentType' => $parentType,
                  					  'parentId' => $parentId,
                              'parentObj' => $parentObj,
                              'showFormBlock' => $showFormBlock));  

	}


    /**
     * if (commentId != null) ==> answer to parentComment
     * Route : /comment-stream/add-comment/{parentType}/{parentId}/{commentId}", name="add-comment"
     * @Route("/comment-stream/add-comment/{parentType}/{parentId}/{commentId}", name="add-comment")
     */
    public function addComment($parentType, $parentId, $commentId=null, 
                                    Request $request, AuthorizationCheckerInterface $authChecker,
                                    TranslatorInterface $translator)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $commentStreamRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(CommentStream::class);
        $coStream = $commentStreamRepo->findOneBy(array("parentType" => $parentType,
                                                        "parentId" => $parentId) );
        $parent = false;
        if($parentType == "news"){
            $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
            $parent = $newsRepo->findOneBy(array('id' => new \MongoId($parentId)) );

            if($parent == null)
              return $this->json(array('error' => 'true',
                                       'errorMsg' =>$translator->trans("Sorry, this publication no longer exists in our database. Impossible to add your comment. The post will self-destruct in 3 seconds.", array(), "comment")));
            
            if($parent->getIsActive() == false && $parent->getReport()->getClosedByType() == Report::CLOSED_BY_ADMIN)
              return $this->json(array('error' => 'true',
                                       'errorMsg' => $translator->trans("Sorry, this publication has been locked by an admin. Comments are no longer accepted. The post will self-destruct in 3 seconds.", array(), "comment")));

            if($parent->getIsActive() == false && $parent->getReport()->getClosedByType() == Report::CLOSED_BY_CO_MODERATION)
              return $this->json(array('error' => 'true',
                                       'errorMsg' => $translator->trans("Sorry, this publication has been locked by collective moderation. Comments are no longer accepted. The post will self-destruct in 3 seconds.", array(), "comment")));
        }

        if($parentType == "report"){
            $reportRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Report::class);
            $parent = $reportRepo->findOneBy(array('id' => new \MongoId($parentId)) );
        }
        
        
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request); 
        if ($form->isSubmitted() && $form->isValid()) {
            
            $myUserPage = $this->getUser()->getMyUserPage();
            $comment->setCreated(new \Datetime());
            $comment->setAuthor($myUserPage);
            $comment->setSigned($myUserPage);
            $comment->setId(new \MongoId());
            
            if($coStream == null) $coStream = new CommentStream();

            // Enregistre le comment dans la bdd
            $em = $this->get('doctrine_mongodb')->getManager();
            
            if($commentId == null){
                $coStream->addComment($comment);
                $coStream->setParentType($parentType);
                $coStream->setParentId($parentId);
                $parent->setCommentStream($coStream);
                $em->persist($coStream);
            }else{
                $parentComment = $coStream->getCommentById($commentId);
                $parentComment->addAnswer($comment);
                $em->persist($parentComment);
            }

            $em->flush();
       
            if($parentType != "report"){
              /* SEND NOTIFICATION */
              $notifObj = Notification::sendNotification($myUserPage, Notification::VERB_COMMENT,
                                                        $parentId, $parentType, $comment->getId(), "comment", $em, $this);
              $notifObj->setWhatObj($comment);
              $notif = array("id"=>$notifObj->getId(),
                             "html"=> $this->renderView('notification/item-notification.html.twig',
                                                        array('notif' => $notifObj)));
            }
        }else{
            return $this->json(array('error' => $translator->trans("Security error. Please reload your page before to send a new comment.", array(), "comment")));
        }
                             
        $html = $this->renderView('comment/comment-single.html.twig',
                                array('comment' => $comment,
                                      'commentForm' => $form->createView(),
                                      'parentType' => $parentType,
                                      'parentId' => $parentId));  

        return $this->json(array('notif' => @$notif,
                                 'html' => $html));

    }


    /**
     * @Route("/comment/send-like/{parentId}/{parentType}/{commentId}/{likeType}", name="comment-send-like")
     */
    public function sendLike($parentId, $parentType, $commentId, $likeType, 
                             Request $request, AuthorizationCheckerInterface $authChecker,
                             TranslatorInterface $translator){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();

        $csRepo = $em->getRepository(CommentStream::class);
        $cs = $csRepo->findOneBy(array('parentId' => $parentId,
                                           'parentType' => $parentType) 
                                      );
        
        $comment = $cs->getCommentById($commentId);
        $addOk = false; $newTotal = 0;
        if($likeType == "likes"){
            $addOk = $comment->addLike($this->getUser()->getMyUserPage());
            //$msg = $translator->trans("You like it", array(), "comment");
            if($addOk == false){ //si j'aime déjà, je retire mon like
                $removeOk = $comment->removeLike($this->getUser()->getMyUserPage());
                //$msg = $translator->trans("You do not like it anymore", array(), "comment");
            }
        }
        if($likeType == "dislikes"){
            $addOk = $comment->addDislike($this->getUser()->getMyUserPage());
            //$msg = $translator->trans("You dislike it", array(), "comment");
            if($addOk == false){ //si je dislike déjà, je retire mon dislike
                $removeOk = $comment->removeDislike($this->getUser()->getMyUserPage());
                //$msg = $translator->trans("You do not dislike it anymore", array(), "comment");
            }
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $em->persist($comment);
        $em->flush();

        $verb = $likeType == "likes" ? Notification::VERB_LIKE : Notification::VERB_DISLIKE;

        $notif = "null";
        if($parentType != "report"){
          $notifObj = Notification::sendNotification($this->getUser()->getMyUserPage(), $verb,
                                                    $parentId, $parentType, $commentId, "comment", $em, $this);
          
          $notifObj->setWhatObj($comment);
          $notif = array("id"=>$notifObj->getId(),
                         "html"=> ($addOk == true) ? $this->renderView('notification/item-notification.html.twig',
                                                                          array('notif' => $notifObj)) : ""
                         );
        }
        
        //$msg = "";
        return $this->json(array('error' => !$addOk,
                                 //'errorMsg' => $msg,
                                 'newTotal' => array("likes" => count($comment->getLikes()), 
                                                     "dislikes" => count($comment->getDislikes())
                                                    ), 
                                 'notif' => $notif)
                           );
        
    }

    /**
     * @Route("/comment/edit/{parentId}/{parentType}/{commentId}", name="comment-edit")
     */
    public function edit($parentId, $parentType, $commentId, 
                         Request $request, AuthorizationCheckerInterface $authChecker,
                         TranslatorInterface $translator){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();

        $csRepo = $em->getRepository(CommentStream::class);
        $cs = $csRepo->findOneBy(array('parentId' => $parentId,
                                       'parentType' => $parentType) 
                                      );
        
        $comment = $cs->getCommentById($commentId);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request); 
        if ($form->isSubmitted() && $form->isValid() && 
            $this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($comment->getSigned()->getSlug())) {
            $em->flush();
        }else{
          return $this->json(array('error' => true,
                                   'errorMsg' => $translator->trans("Error : your comment has not been edited.", array(), "comment")
                                 )
                           );
        }
        
        return $this->json(array('error' => false,
                                 'errorMsg' => $translator->trans("Your comment has been edited.", array(), "comment"),
                                 'newText' => $comment->getTextHtml()
                                 )
                           );
    }

    /**
     * @Route("/comment/delete/{parentId}/{parentType}/{commentId}", name="comment-delete")
     */
    public function delete($parentId, $parentType, $commentId, 
                            Request $request, AuthorizationCheckerInterface $authChecker,
                            TranslatorInterface $translator){

        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();

        $csRepo = $em->getRepository(CommentStream::class);
        $cs = $csRepo->findOneBy(array('parentId' => $parentId,
                                       'parentType' => $parentType) 
                                      );
        
        $comment = $cs->getCommentById($commentId);
        $removed = true;

        $parentObj = null;
        if($parentType == "news"){
          $newsRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(News::class);
          $parentObj = $newsRepo->findOneBy(array("id" => new \MongoId($parentId)));
        }

        //dd($comment);
        if($comment != null){
          //si je suis l'admin du commentaire ou l'admin de la news parent
            if($this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($comment->getSigned()->getSlug()) ||
               ($this->getUser()->isSuperAdmin() && $comment->getReport() != null)||
               ($parentType == "news" && $parentObj != null &&
               $this->getUser()->getMyUserPage()->getRelations()->getImAdminOf($parentObj->getSigned()->getSlug()))
              ){
              $removed = $cs->removeComment($comment);
              if($removed == true)
                  $em->flush($cs);

              return $this->json(array('error' => !$removed,
                                       'errorMsg' => $translator->trans("Your comment has been deleted.", array(), "comment"),
                                       ) );
            }else{
              return $this->json(array('error' => true,
                                       'errorMsg' => $translator->trans("Sorry, you are not authorised to delete this comment.", array(), "comment"),
                                       ) );
            }
        }


    }
}
