<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Cookie;

use App\Form\UserType;
use App\Form\NewsType;
use App\Form\ContactType;
use App\Form\CommentType;
use App\Form\PageType;
use App\Form\PageInfoType;
use App\Entity\User;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\CommentStream;
use App\Entity\Comment;
use App\Entity\Tag;
use App\Entity\Contact;

use App\Services\Helper;
use App\Services\InteropTranslator;

use Unirest;

//echo `pwd`; exit;

class InteropController extends Controller
{
    /**
     * Route : /in-transition/{search}/{renderPartial}", name="search-interop"
     * @Route("/in-transition/{search}/{renderPartial}", name="search-interop")
     */
    public function searchInterop($search="", $renderPartial="false",
                            Request $request, AuthorizationCheckerInterface $authChecker)
    {

        //if (!$authChecker->isGranted('ROLE_USER')) { throw new AccessDeniedException(); }

        if($search == "-") $search = "";
        $search = preg_replace("/></", "", $search);
        $search = preg_replace("/ /", ",", $search);
        
        $currentUserPage = new Page();
        $currentUserPage->setCoordinates(array(2.377594901489258, 46.13417004624326));
        if ($authChecker->isGranted('ROLE_USER')){
            $currentUserPage = $this->getUser()->getMyUserPage();
        }

        //$myUser = $this->getUser();
        $myPosition = $currentUserPage->getCoordinates();
        
        $aroundCoordinates = null;
        if($request->query->get('lat') != null && $request->query->get('lon') != null)
        $aroundCoordinates = array(floatval($request->query->get('lat')), 
                                    floatval($request->query->get('lon')));

        $myAgenda = $request->query->get('myagenda') != null ? $request->query->get('myagenda') : false;
        

        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);

        /*$allRes = $pageRepo->getGlobalSearch($search,  
                                          $request->query->get('types'), 
                                          $aroundCoordinates,
                                          floatval($request->query->get('radius')),
                                          $startDate, 
                                          $endDate,
                                          $myAgenda,
                                          array()
                                        );
        $res = $allRes["res"];*/
        $bounds = $request->query->get('bounds');
        //dump($bounds);
        $headers = array('Accept' => 'application/json');
        $params = array("tags" => $search, "limit" => 1000); //dd($params);
        $type = $request->query->get('types');
        if($type == "") $type = "organization";
        //$firstRes = Unirest\Request::get('http://presdecheznous.fr/api/elements.json?limit=100&bounds='.$bounds.'&token=5c6d1af4f1ba7');
        $firstRes = Unirest\Request::get('http://presdecheznous.fr/api/elements.json?limit=500&categories='.$search.'&bounds='.$bounds.'&token=5c6d1af4f1ba7');

        //$firstRes = Unirest\Request::get('https://transiscope.gogocarto.fr/api/elements.json?limit=500&categories='.$search.'&bounds='.$bounds.'');
        
        //AMAP 539
        //Producteur/Artisan 538
        //Marché 533

        //$firstRes = Unirest\Request::get('https://communecter.org/api/'.$type.'/get', $headers, $params);
        //$firstRes = Unirest\Request::get('http://sct1.scrutari.net/sct/geosse/json?flt-bbox=-5.4534286,41.2632185,9.8678344,51.268318&fieldvariant=geosse&type=geojson&lang=fr&origin=js-geosse&warnings=1&version=3');


        //dd($firstRes->body->data);
        //dd($firstRes->body->features);
        //
        //$firstRes = json_decode($firstRes->raw_body);
        $firstRes = $firstRes->body->data;

        $typeNxo = "freegroup";
        $allRes = array(); $allTags = array();
        $i=0; $iMax = 1000;
        foreach ($firstRes as $key => $res) { if($key > $iMax) break;
            //$newPage = InteropTranslator::translate("communecter", $res, $typeNxo);
            //dump($res); exit; return;
            $newPage = InteropTranslator::translate("presdecheznous", $res, $typeNxo);
            if($newPage != null) {
                $allRes[] = $newPage;
                foreach ($newPage->getTags() as $key => $tag) {
                    if(!array_key_exists($tag, $allTags)) $allTags[$tag] = 1;
                    else $allTags[$tag]++;
                }
            }
        }

        $endAllTags = array("AMAP"=>539, "Producteur/Artisan"=>538, "Marché"=>533, "La Ruche qui dit oui"=>536, "Éco-construction"=>562,
                            "Énergie renouvelables"=>566, "Jardins partagés"=>575, "Grainothèque"=>576, "Friperie"=>613, "Produits en vrac"=>828, "Point de collecte"=>812, "Collectifs citoyens" => 802 );
        /*foreach ($allTags as $tag => $count) {
            if($count > 5) $endAllTags[] = $tag;
        }*/
        //dump($endAllTags);
        //dd($allRes);//["meta"]["entities"]);

        $totalCount = 0; //count($allRes);//["totalCount"];

        $jsonRes = $this->getJsonRes($allRes, $currentUserPage);
        $params = array(
                "results" => $allRes,
                "totalCount" => $totalCount,
                'jsonRes' => $jsonRes,
                "yourSearch" => $search,
                'myPosition' => $myPosition,
                'typeSearch' => Page::getTypeInteropSearch(),
                'endAllTags' => $endAllTags
            );

        if(!empty($request->query->get('types')))   $params["typesSearch"] = $request->query->get('types');
        if($aroundCoordinates != null)              $params["coordinatesSearch"] = $aroundCoordinates;
        if(!empty($request->query->get('radius')))  $params["radiusSearch"] = floatval($request->query->get('radius'));
       
         
        $params["currentUserPage"] = $currentUserPage;
        if ($authChecker->isGranted('ROLE_USER')){
            $params["extends"] = "theme/base-app.html.twig";
        }else{
            $params["extends"] = "theme/base-app-offline.html.twig";
        }

        //dd($jsonRes);
        if($renderPartial=="false"){
            return $this->render('interop/search.html.twig', $params);
        }elseif($renderPartial=="json"){
            return $this->json(array('jsonRes' => $jsonRes));
        }else{
            return $this->render('interop/search-res-list.html.twig', $params);
        }
    }




    /**
     * Convert search result objects to Json<br>
     * (to use datas in javascript, mainly for SIG to display datas on map)<br>
     * if you want your page appear in result, set excludeMe to false
     * @return array
     */
    private function getJsonRes($res, $userPage, $excludeMe=true){ 
        $jsonRes = array();
        $myPageId = $this->getUser() != null ? $this->getUser()->getMyUserPage()->getId() : 0;
        foreach ($res as $key => $value) {
            if($excludeMe == false || $value->getId() != $myPageId)
            $jsonRes[] = $value->getJson($userPage);        
        }
        return $jsonRes;
    }


    private function objsToId($array){
      $res = array();
      foreach($array as $key => $val)  { $res[] = new \MongoId($val->getId()); }
      return $res;
    }



    
    /**
     * Route : /import-json, name="import-json"
     * @Route("/import-json", name="import-json")
     */
    public function importJson(Request $request, AuthorizationCheckerInterface $authChecker)
    {
        if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
        
        /* remove all assemblies * /
        $assemblies = $pageRepo->findBy(array("type" => "assembly"));

        foreach ($assemblies as $key => $ass) {
            $em->persist($ass);
        }
        $em->flush();
        / * remove all assemblies */
        
        $string = file_get_contents("ateliers_citoyens_3.geojson");
        $json = json_decode($string, true);
        
        //dd($json);
        foreach ($json["features"] as $key => $value) {

            $page = new Page();

            //si la page existe déjà, on 
            //
            $pageRepo = $this->get('doctrine_mongodb')->getManager()->getRepository(Page::class);
            $assembly = $pageRepo->findOneBy(array("type" => "assembly", 
                                                   "name" => $value["properties"]["Name"]));
            if($assembly != null)
                $page = $assembly;

            //echo $value["properties"]["Name"];
            $page->setName($value["properties"]["Name"]);
            $page->setDescription($value["properties"]["Description"]);

            $slug = Helper::getUniqueSlug($value["properties"]["Name"], $pageRepo);
            $page->setSlug($slug);

            $page->setType(Page::TYPE_ASSEMBLY);

            $tags = $this->extractTags($page);
            //dump($tags);
            $page->setTagsStr($tags);
            $page->setCoordinates($value["geometry"]["coordinates"]);
            $page->setIsActive(true);
            $page->setImageBanner("default_assembly.png");
            $page->setImageProfil("default_assembly.png");
            $page->setCreated(new \Datetime());

            $parentPage = $pageRepo->findOneBy(array("slug" => "culture-ric"));
            $adminPage = $pageRepo->findOneBy(array("slug" => "mike-tdi"));

            $page->setOwner($this->getUser());
            $page->setParentPage($parentPage);

            $myUser = $this->getUser();         
            //add superadmin as admin
            $userPage = $myUser->getMyUserPage();
            $page->getRelations()->addAdmin($userPage);
            $userPage->getRelations()->removeChatContacts($page);
            $userPage->getRelations()->addIsAdminOf($page);

            //add mike as admin
            $page->getRelations()->addAdmin($adminPage);
            $adminPage->getRelations()->removeChatContacts($page);
            $adminPage->getRelations()->addIsAdminOf($page);
            

            $page->setConfidentialityByKey("SHOW_GEOPOS", "CONF_NEXXO");
            //dump($assembly);

            if($assembly == null){
                $em->persist($page);
                $em->flush($page);
            }else{                
                $em->flush();
            }

        }


        

        return $this->json(array('jsonRes' => "ok"));
    }

    private function extractTags($page){

        $defaultTags = array("citoyen"=>"#assembléeCitoyenne",
                             "Citoyen"=>"#assembléeCitoyenne",
                             "RIC"=>"#assembléeRIC",
                             "constitu"=>"#assembléeConstituante",
                             "Constitu"=>"#assembléeConstituante",
                             "GJ"=>"#assembléeGJ",
                             "Gilets Jaunes"=>"#assembléeGJ",
                             "gilets jaunes"=>"#assembléeGJ",
                             "Gilet Jaune"=>"#assembléeGJ",
                             "gilet jaune"=>"#assembléeGJ",
                                );


        $name = $page->getName();
        $desc = $page->getDescription();
        $tagsStr = "";

        //$regex = '/^#([a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/';
        //preg_match_all($regex, $string, $matches);

        foreach ($defaultTags as $key => $dtag) { 
            if(strpos($name, $key) !== false) $tagsStr .= $dtag." ";
            else if(strpos($desc, $key) !== false) $tagsStr .= $dtag." ";
        }
        
        return $tagsStr;

    }

}