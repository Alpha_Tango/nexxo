<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Form\UserType;
use App\Form\NewsType;
use App\Form\CommentType;
use App\Form\PageType;
use App\Form\PageInfoType;
use App\Entity\User;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\Media;
use App\Entity\Relation;
use App\Entity\CommentStream;
use App\Entity\Comment;
use App\Entity\ChatMessage;
use App\Entity\Group;
use App\Entity\Notification;
use App\Entity\Report;
use App\Entity\Tag;

use App\Services\Helper;
//echo `pwd`; exit;

class InitController extends Controller
{
	 /**
     * @Route("/init-app", name="init-app")
     */
    public function initApplication(AuthorizationCheckerInterface $authChecker, UserPasswordEncoderInterface $passwordEncoder)
    {
    	if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }else{
          if($this->getUser()->isSuperAdmin() == true){

            $superAdmin = $this->getUser()->getMyUserPage();
            
            //return; //locked for production
          	$em = $this->get('doctrine_mongodb')->getManager();
  	        $em->getDocumentCollection(ChatMessage::class)->remove(array());
  	        $em->getDocumentCollection(CommentStream::class)->remove(array());
  	        $em->getDocumentCollection(Group::class)->remove(array());
            $em->getDocumentCollection(News::class)->remove(array());
            $em->getDocumentCollection(Media::class)->remove(array());
  	        $em->getDocumentCollection(Notification::class)->remove(array());
            $em->getDocumentCollection(Report::class)->remove(array());
            $em->getDocumentCollection(Tag::class)->remove(array());
  	        $em->getDocumentCollection(Page::class)->remove(array("slug"=>array('$ne'=>$superAdmin->getSlug())));
  	        $em->getDocumentCollection(User::class)->remove(array("isSuperAdmin"=>array('$ne'=>true)));

          	//dd($superAdmin);
            if($superAdmin == null){
              $superAdmin =
              $this->createPage(array("name"=>"Super Admin",
                                      "type"=>Page::TYPE_USER,
                                      "owner"=>"superadmin@mail.com"));
            }

          	//remise à zéro de toutes les relations du super-admin
          	$superAdmin->setRelations(new Relation($superAdmin));
            $superAdmin->getRelations()->addAdmin($superAdmin);
            $superAdmin->getRelations()->addIsAdminOf($superAdmin);
            $em->flush($superAdmin); 
           	
           	/* CREATE USERS */
           	$pipster =
           	$this->createUserAndPage(array("username"=>"Pipster",
           								   "email"=>"pipster@mail.com"), 
           									$passwordEncoder);

           	$jango =
           	$this->createUserAndPage(array("username"=>"Jango",
           								   "email"=>"jango@mail.com"), 
           									$passwordEncoder);

           	$nestor =
           	$this->createUserAndPage(array("username"=>"Nestor",
           								   "email"=>"nestor@mail.com"), 
           									$passwordEncoder);
            $nestor->setFirstStep(0);
           	$irina =
           	$this->createUserAndPage(array("username"=>"Irina",
           								   "email"=>"irina@mail.com"), 
           									$passwordEncoder);

            $mike =
            $this->createUserAndPage(array("username"=>"Mike TDI",
                             "email"=>"mike@mail.com"), 
                            $passwordEncoder);
            $mike->setFirstStep(0);
            
            /* CREATE PAGES */
           	$groupe1 =
           	$this->createPage(array("name"=>"Groupe 1",
           							"type"=>Page::TYPE_FREEGROUP,
           							"owner"=>"pipster@mail.com"));
            
           	$project1 =
           	$this->createPage(array("name"=>"Project 1",
           							"type"=>Page::TYPE_PROJECT,
           							"owner"=>"jango@mail.com"));
            
			      $asso1 =
           	$this->createPage(array("name"=>"Asso 1",
           							"type"=>Page::TYPE_ASSOCIATION,
           							"owner"=>"nestor@mail.com"));
            
           	$event1 =
            $this->createPage(array("name"=>"Evenement 1",
                        "type"=>Page::TYPE_EVENT,
                        "owner"=>"nestor@mail.com"));
            
            $cultureRIC =
            $this->createPage(array("name"=>"Culture RIC",
                        "type"=>Page::TYPE_FREEGROUP,
                        "owner"=>"mike@mail.com"));
            
            /*$event2 =
            $this->createPage(array("name"=>"Evenement 2",
                        "type"=>Page::TYPE_EVENT,
                        "owner"=>"nestor@mail.com"));
            
            $em->flush($event2);*/

            $news1 =
           	$this->createNews(array("text"=>"Le premier message de Nexxo !",
           							"author"=>$pipster,
           							"signed"=>$pipster,
           							"target"=>$pipster,
           							"scopeType"=>"followers",
           							"isSharable"=>false));
            
            //superAdmin follow pipster
           	$superAdmin->getRelations()->addFollows($pipster);
           	$pipster->getRelations()->addFollower($superAdmin);
           	$em->flush($superAdmin); 
           	$em->flush($pipster);

            //superAdmin become friend with jango (without confirmation from jango)
            $superAdmin->getRelations()->addFriend($jango);
            $jango->getRelations()->addFriend($superAdmin);
            $em->flush($superAdmin); 
            $em->flush($jango);

            //superAdmin become friend with jango (without confirmation from jango)
            $superAdmin->getRelations()->addFriend($nestor);
            $nestor->getRelations()->addFriend($superAdmin);
            $em->flush($superAdmin); 
            $em->flush($nestor);

            //superAdmin like irina's page
           	$superAdmin->getRelations()->addLike($irina);
           	$irina->getRelations()->addLiker($superAdmin);
           	$em->flush($superAdmin); 
           	$em->flush($irina);

            //superAdmin participate to event1
            $superAdmin->getRelations()->addParticipateTo($event1);
            $event1->getRelations()->addParticipant($superAdmin);
            $em->flush($superAdmin); 
            $em->flush($event1);

            //superAdmin become admin to asso1
            $superAdmin->getRelations()->addIsAdminOf($asso1);
            $asso1->getRelations()->addAdmin($superAdmin);
            $em->flush($superAdmin); 
            $em->flush($asso1);

       		 return $this->redirectToRoute('superadmin');
          }
        }

        return $this->redirectToRoute('login');
    }

    private function createUserAndPage($params, UserPasswordEncoderInterface $passwordEncoder){
    	
    	$em = $this->get('doctrine_mongodb')->getManager();
		  $pageRepo = $em->getRepository(Page::class);
        
		  //USER
    	$user = new User();
    	$user->setUsername(@$params["username"]);
    	$user->setEmail(@$params["email"]);

    	$password = $passwordEncoder->encodePassword($user, "memoire");
      $user->setPassword($password);
      $user->setPlainPassword("");
        
 		 //PAGE
    	$page = new Page();
    	$page->setSlug(Helper::getUniqueSlug($user->getRealUserName(), $pageRepo));
    	$page->setName($user->getRealUserName());
      $page->setOwner($user);
      $page->setType("user");
    	$page->setIsActive(@$params["isActive"] ? @$params["isActive"] : true);
      $page->setIsPrivate(@$params["isPrivate"] ? @$params["isPrivate"] : false);
      $page->getRelations()->addAdmin($page);
      $page->getRelations()->addIsAdminOf($page);
      $page->setCreated(new \Datetime());

      $user->addPage($page);

      $em->persist($user);
      $em->persist($page);
      $em->flush();

      return $page;
    }

    private function createPage($params){
    	
    	$em = $this->get('doctrine_mongodb')->getManager();
		  $pageRepo = $em->getRepository(Page::class);
      $userRepo = $em->getRepository(User::class);
      $owner = $userRepo->findOneBy(array("email"=>@$params["owner"]));  

		  //PAGE
  	  $page = new Page();
  	  $page->setSlug(Helper::getUniqueSlug(@$params["name"], $pageRepo));
  	  $page->setName(@$params["name"]);
      $page->setOwner($owner);
      $page->setType(@$params["type"]);
  	  $page->setIsActive(@$params["isActive"] ? @$params["isActive"] : true);
      $page->setIsPrivate(@$params["isPrivate"] ? @$params["isPrivate"] : false);
      $page->getRelations()->addAdmin($owner->getMyUserPage());
      $owner->getMyUserPage()->getRelations()->addIsAdminOf($page);
      $page->setCreated(new \Datetime());

      $owner->addPage($page);

      $em->persist($page);
      $em->flush();
      $em->flush($owner);

      return $page;
    }

    private function createNews($params){
    	
    	$em = $this->get('doctrine_mongodb')->getManager();
		  //$pageRepo = $em->getRepository(Page::class);
      //$userRepo = $em->getRepository(User::class);
      //$owner = $userRepo->findOneBy(array("email"=>@$params["owner"]));  

 		  //PAGE
    	$news = new News();
    	$news->setText(@$params["text"]);
      $news->setAuthor(@$params["author"]);
      $news->setSigned(@$params["signed"]);
      $news->setTarget(@$params["target"]);
      $news->setScopeType(@$params["scopeType"]);
      $news->setIsSharable(@$params["isSharable"]);
      $news->setCreated(new \Datetime());

      $em->persist($news);
      $em->flush();

      return $news;
    }

    /**
     * @Route("/drop-all-collections", name="drop-all-collections")
     */
    public function dropAllCollections(AuthorizationCheckerInterface $authChecker)
    {
    	if (!$authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }else{
          	if($this->getUser()->isSuperAdmin() == true){
            //return; //locked for production
            //use with caution
		       	$em = $this->get('doctrine_mongodb')->getManager();
		        $em->getDocumentCollection(ChatMessage::class)->remove(array());
		        $em->getDocumentCollection(CommentStream::class)->remove(array());
		        $em->getDocumentCollection(Group::class)->remove(array());
		        $em->getDocumentCollection(News::class)->remove(array());
		        $em->getDocumentCollection(Notification::class)->remove(array());
		        $em->getDocumentCollection(Report::class)->remove(array());
		        $em->getDocumentCollection(Page::class)->remove(array("slug"=>array('$ne'=>"super-admin")));
		        $em->getDocumentCollection(User::class)->remove(array("isSuperAdmin"=>array('$ne'=>true)));


        		return $this->redirectToRoute('init-app');
        		//return $this->redirectToRoute('init-first-user');
	        }
	    }
        return $this->redirectToRoute('login');
    }

    /**
     * @Route("/init-first-user", name="init-first-user")
     */
    public function initFirstUser(AuthorizationCheckerInterface $authChecker, UserPasswordEncoderInterface $passwordEncoder)
    {
    	//dd("drop collection ok !");
      //return; //locked for production
    	$em = $this->get('doctrine_mongodb')->getManager();
      $pageRepo = $em->getRepository(Page::class);
      $page = $pageRepo->findOneBy(array("isSuperAdmin"=>true));

      if($page == null){
      	//USER
      	$fUser = new User();
      	$fUser->setUsername("Super Admin");
      	$fUser->setIsSuperAdmin(true);
      	$fUser->setEmail("superadmin@mail.com");

      	$password = $passwordEncoder->encodePassword($fUser, "memoire");
        $fUser->setPassword($password);
        $fUser->setPlainPassword("");
          
          
   		  //PAGE
      	$fUserPage = new Page();
      	$fUserPage->setSlug(Helper::getUniqueSlug($fUser->getRealUserName(), $pageRepo));
      	$fUserPage->setName($fUser->getRealUserName());
        $fUserPage->setOwner($fUser);
        $fUserPage->setType("user");
        $fUserPage->setFirstStep(0);
    	  $fUserPage->setIsActive(true);
        $fUserPage->setIsPrivate(false);
        $fUserPage->getRelations()->addAdmin($fUserPage);
        $fUserPage->getRelations()->addIsAdminOf($fUserPage);
        $fUserPage->setCreated(new \Datetime());

        $fUser->addPage($fUserPage);

        $em->persist($fUser);
        $em->persist($fUserPage);
        $em->flush();
      }

      return $this->redirectToRoute('login');
    }

}