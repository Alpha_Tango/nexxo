<?php

namespace App\Controller;

use App\Entity\User;
use App\Services\Mailer;


use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;


use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use App\Form\ResettingType;

/**
 * @Route("/reset-password")
 */
class ResettingController extends Controller
{
    /**
     * @Route("/{userId}/{token}", name="resetting")
     */
    public function resetting($userId, $token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $em = $this->get('doctrine_mongodb')->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array("id" => $userId));

        // interdit l'accès à la page si:
        // le token associé au membre est null
        // le token enregistré en base et le token présent dans l'url ne sont pas égaux
        // le token date de plus de 10 minutes
        if ($user->getToken() === null || $token !== $user->getToken() || !$this->isRequestInTime($user->getPasswordRequestedAt()))
        {
            throw new AccessDeniedHttpException();
        }

        $form = $this->createForm(ResettingType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // réinitialisation du token et de la date de sa création à NULL
            $user->setToken(null);
            $user->setPasswordRequestedAt(null);

            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($user);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', "Votre mot de passe a été renouvelé.");

            return $this->redirectToRoute('login');

        }

        return $this->render('theme/account/resetting/index.html.twig', [
            'form' => $form->createView()
        ]);
        
    }

    

    /**
     * @Route("/requete", name="request_resetting")
     */
    public function request(Request $request, Mailer $mailer,  
                            TokenGeneratorInterface $tokenGenerator, TranslatorInterface $translator){
        
        // création d'un formulaire "à la volée", afin que l'internaute puisse renseigner son mail
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, [
                'label' => $translator->trans("E-mail address", array(), "form"),
                'constraints' => [
                    new Email(),
                    new NotBlank()
                ]
            ])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->get('doctrine_mongodb')->getManager();
            $user = $em->getRepository(User::class)->findOneBy(array("email" => $form->getData()['email']));

            // aucun email associé à ce compte.
            if (!$user) {
                $request->getSession()->getFlashBag()->add('warning', $translator->trans("This e-mail does not exists.", array(), "form"));
                return $this->redirectToRoute("request_resetting");
            } 

            // création du token
            $user->setToken($tokenGenerator->generateToken());
            // enregistrement de la date de création du token
            $user->setPasswordRequestedAt(new \Datetime());
            $em->flush();

            // on utilise le service Mailer créé précédemment
            $bodyMail = $mailer->createBodyMail('theme/account/resetting/mail.html.twig', [
                'user' => $user
            ]);
            

            $mailer->sendMessage('postmaster@nexxociety.co', $user->getEmail(), 
                                 $translator->trans('Nexxo : forgot your password', array(), "mail"), $bodyMail);

            $request->getSession()->getFlashBag()->add('success', 
                $translator->trans("An e-mail will be sent to you so that you can renew your password. The link you will receive will be valid 24h.", array(), "mail"));

            return $this->redirectToRoute("login");
        }

        return $this->render('theme/account/resetting/request.html.twig', [
            'form' => $form->createView()
        ]);
    }
 


    // si supérieur à 10min, retourne false
    // sinon retourne false
    private function isRequestInTime(\Datetime $passwordRequestedAt = null)
    {
        if ($passwordRequestedAt === null)
        {
            return false;        
        }
        
        $now = new \DateTime();
        $interval = $now->getTimestamp() - $passwordRequestedAt->getTimestamp();

        $daySeconds = 60 * 10;
        $response = $interval > $daySeconds ? false : $reponse = true;
        return $response;
    }

}
