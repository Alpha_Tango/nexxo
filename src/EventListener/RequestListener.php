<?php 
namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Cookie;

class RequestListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
		/*$request = $event->getRequest();

        dump($request->getPreferredLanguage());
        dd($request->getSession()->get('_locale'));

        $userPrefLang = $request->getPreferredLanguage();
        $currentLocale = $request->getSession()->get('_locale');

        if($currentLocale == null && $userPrefLang == "fr"){
            $request->setLocale('fr');
            $request->getSession()->set('_locale', "fr");
        }*/

    }
    public function onKernelResponse(FilterResponseEvent $event)
    {
        /*$response = $event->getResponse();
        $request = $event->getRequest();

        //$userPrefLang = $request->getPreferredLanguage();
        $currentLocale = $request->getSession()->get('_locale');
        dump("curent ".$currentLocale);
        //$myResult = $request->attributes->get('my_result');
        //$cookie = new Cookie();
        //dd($request->cookies->get('_locale') );
        //if($request->cookies->get('_locale') == null){
            $cookie = new Cookie('_locale', $currentLocale, (time() + 3600 * 24 * 356), '/');
            $response->headers->setCookie($cookie);
       // }else{

       // }*/

    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered after the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 15)),
        );
    }
}