<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


use App\Entity\Group;
use App\Entity\CommentStream;
use App\Services\Helper;


/**
 * @MongoDB\Document(repositoryClass="App\Repository\NewsRepository")
 */
class Report
{
	const STATUS_OPEN  = "OPEN";
	const STATUS_VOTE_OPEN  = "VOTE_OPEN";
	const STATUS_ENABLED  = "ENABLED";
	const STATUS_DISABLED  = "DISABLED";

	const CLOSED_BY_ADMIN = "ADMIN";
	const CLOSED_BY_CO_MODERATION = "CO_MODERATION";

	/**
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

	/**
     * @MongoDB\Field(type="string")
     */
    private $aboutId;

    /**
     * @MongoDB\Field(type="string")
     */
    private $aboutType;
    

    /** @MongoDB\NotSaved */
    private $aboutObj;
    
	/**
     * @MongoDB\Field(type="string")
     */
    private $commentId;

    /** 
     * Reference the Pages (user) who like the News
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $authors;

    /** 
     * Reference the Pages (user) who vote up
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $voteUp;

    /** 
     * Reference the Pages (user) who vote down
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $voteDown;

    /**
     * Status : open -disabled - enabled
     * @MongoDB\Field(type="string")
     */
    private $status;

    /**
     * Date initialized when the News is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $created;

    /**
     * Date initialized when the News is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $dateStartVote;

    /**
     * Date initialized when the News is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $dateEndVote;

    /** 
     * CLOSED_BY_ADMIN || CLOSED_BY_CO_MODERATION
     * @MongoDB\Field(type="string")
     */
    private $closedByType;

    /** 
     * Reference the Pages (user) admin who closed the report
     * @MongoDB\ReferenceOne(targetDocument="Page") 
     */
    private $closedByUser;

    /** 
     * Reference the commentStream linked to this News
     * @MongoDB\ReferenceOne(targetDocument="CommentStream") 
     */
    private $commentStream;


    public function __construct($aboutId, $aboutType, $commentId, $author){
		
		$this->aboutId = $aboutId;
        $this->aboutType = $aboutType;
        $this->commentId = $commentId;

        $this->authors = array();
		$this->voteUp = array();
        $this->voteDown = array();

		$this->addAuthor($author);

        $this->status = Report::STATUS_OPEN;
        $this->created = new \Datetime();
	}


    /**
     * Number of different report needed to open a collective moderation procedure
     * @return int
     */
    public function getMaxReport(){
        return 10; // always keep thesame value
    }
    public static function getNbMaxReport(){
        return 10; // always keep thesame value
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getAboutId()
    {
        return $this->aboutId;
    }

    /**
     * @param mixed $aboutId
     *
     * @return self
     */
    public function setAboutId($aboutId)
    {
        $this->aboutId = $aboutId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAboutType()
    {
        return $this->aboutType;
    }

    /**
     * @param mixed $aboutType
     *
     * @return self
     */
    public function setAboutType($aboutType)
    {
        $this->aboutType = $aboutType;

        return $this;
    }


    /**
     * Get aboutObj
     */
    public function getAboutObj()
    {
        return $this->aboutObj;
    }

    /**
     * Set aboutObj
     */
    public function setAboutObj($aboutObj)
    {
        $this->aboutObj = $aboutObj;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getCommentId()
    {
        return $this->commentId;
    }

    /**
     * @param mixed $commentId
     *
     * @return self
     */
    public function setCommentId($commentId)
    {
        $this->commentId = $commentId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @param mixed $authors
     *
     * @return self
     */
    public function addAuthor(Page $author)
    {
    	if(!$this->inAuthors($author->getSlug())){
	        $this->authors[] = $author;
	        $this->addVoteDown($author);
		}
        return $this;
    }

    /**
     * remove voteUp
     */
    public function removeAuthor(Page $author)
    {   
        $this->authors->removeElement($author);
    }

    public function inAuthors($slug){
    	foreach ($this->authors as $key => $author) {
    		if($author->getSlug() == $slug) return true;
    	}
    	return false;
    }

    /**
     * @return mixed
     */
    public function getCountVoteUp()
    {
        return count($this->getVoteUp());
    }

    /**
     * @return mixed
     */
    public function getVoteUp()
    {
        return $this->voteUp;
    }

    
    /**
     * @param mixed $voteUp
     *
     * @return self
     */
    public function addVoteUp(Page $page)
    {
    	if(!$this->inVoteUp($page->getSlug()))
        $this->voteUp[] = $page;
    	$this->removeVoteDown($page);
        return $this;
    }

    public function inVoteUp($slug){
    	foreach ($this->voteUp as $key => $page) {
    		if($page->getSlug() == $slug) return true;
    	}
    	return false;
    }

    /**
     * remove voteUp
     */
    public function removeVoteUp(Page $page)
    {   
        if($this->inVoteUp($page->getSlug()))
        $this->voteUp->removeElement($page);
    }

    /**
     * @return mixed
     */
    public function getCountVoteDown()
    {
        return count($this->getVoteDown());
    }

    
    /**
     * @return mixed
     */
    public function getVoteDown()
    {
        return $this->voteDown;
    }

    
    /**
     * @param mixed $voteUp
     *
     * @return self
     */
    public function addVoteDown(Page $page)
    {
    	if(!$this->inVoteDown($page->getSlug()))
        $this->voteDown[] = $page;
    	$this->removeVoteUp($page);

        return $this;
    }

    public function inVoteDown($slug){
    	foreach ($this->voteDown as $key => $page) {
    		if($page->getSlug() == $slug) return true;
    	}
    	return false;
    }

    /**
     * remove voteDown
     */
    public function removeVoteDown(Page $page)
    {   
    	if($this->inVoteDown($page->getSlug()))
        $this->voteDown->removeElement($page);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

	/**
     * @return mixed
     */
    public function getClosedByType()
    {
        return $this->closedByType;
    }

    /**
     * @param mixed $closedByType
     *
     * @return self
     */
    public function setClosedByType($closedByType)
    {
        $this->closedByType = $closedByType;

        return $this;
    }


	/**
     * @return mixed
     */
    public function getClosedByUser()
    {
        return $this->closedByUser;
    }

    /**
     * @param mixed $closedByUser
     *
     * @return self
     */
    public function setClosedByUser(Page $closedByUser)
    {
        $this->closedByUser = $closedByUser;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getDateStartVote()
    {
        return $this->dateStartVote;
    }

    /**
     * @param mixed $dateStartVote
     *
     * @return self
     */
    public function setDateStartVote($dateStartVote)
    {
        $this->dateStartVote = $dateStartVote;
        return $this;
    }




    /**
     * @return mixed
     */
    public function getDateEndVote()
    {
        return $this->dateEndVote;
    }

    /**
     * @param mixed $dateEndVote
     *
     * @return self
     */
    public function setDateEndVote($dateEndVote)
    {
        $this->dateEndVote = $dateEndVote;
        return $this;
    }    

    public static function getClass($type){
        if($type == "news")     return News::class;
        if($type == "comment")  return Comment::class;
        if($type == "page")     return Page::class;

        return ""; 
    }


    /**
     * @return mixed
     */
    public function getCommentStream()
    {
        return empty($this->commentStream) ? new CommentStream() : $this->commentStream;
    }

    /**
     * @param mixed $commentStream
     * @return self
     */
    public function setCommentStream(CommentStream $commentStream)
    {
        $this->commentStream = $commentStream;
        return $this;
    }

    
    public function getTotalVote(){
        
        return count($this->voteUp) + count($this->voteDown); 
    }

    public function getVoteResult(){
        if(count($this->voteUp) >= count($this->voteDown)) return "up"; else return "down";
    }


}