<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use App\Entity\Relation;
use App\Entity\CommentStream;
use App\Entity\Comment;
use App\Entity\Report;
use App\Entity\PageInfo;
use App\Services\Helper;
use App\Services\Country;

/**
 * Pages represent multiple type of data (defined by attribute "type")<br>
 * user - event - project - association - business - ngo - freegroup - institution<br><br>
 * When a new user register, a Page is automaticaly created for him<br>
 * A Page typed by "user" can't change his type
 *
 * @MongoDB\Document(repositoryClass="App\Repository\PageRepository")
 * @MongoDB\Index(keys={"coordinates"="2d"})
 */

class Page
{
    const TYPE_USER         = "user";
    const TYPE_ASSOCIATION  = "association";
    const TYPE_BUSINESS     = "business";
    const TYPE_FREEGROUP    = "freegroup";
    const TYPE_NGO          = "ngo";
    const TYPE_INSTITUTION  = "institution";
    const TYPE_PROJECT      = "project";
    const TYPE_EVENT        = "event";
    const TYPE_AGORA        = "agora";
    const TYPE_CHANNEL      = "channel";
    const TYPE_ASSEMBLY     = "assembly";
    const TYPE_CLASSIFIED   = "classified";
    const TYPE_MD_DOC       = "mddoc";

    const CONF_PRIVATE = "CONF_PRIVATE";
    const CONF_ADMINS = "CONF_ADMINS";
    const CONF_FRIENDS = "CONF_FRIENDS";
    const CONF_FOLLOWERS = "CONF_FOLLOWERS";
    const CONF_NEXXO = "CONF_NEXXO";
    const CONF_ALL = "CONF_ALL";

    /** @MongoDB\NotSaved */
    private $TYPES_AVAILABLES;

	/**
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

	/**
     * @MongoDB\Field(type="string")
     */
	private $name;

	/**
     * Slug is the short (and unique) ID use to access a page via URL
     * @MongoDB\Field(type="string")
     */
	private $slug;

	/**
     * Types available for Pages are define in : $this->TYPES_AVAILABLES 
     * @MongoDB\Field(type="string")
     */
	private $type;

    /**
     * Text description of the Page
     * @MongoDB\Field(type="string")
     */
    private $description;

    /**
     * List of tag detected in the text (not in form / auto-generated before save)
     * @MongoDB\Field(type="collection")
     */
    private $tags;

    /**
     * Text description of the Page
     * @MongoDB\Field(type="string")
     */
    private $tagsStr;

    /**
     * The country related to the Page
     * @MongoDB\Field(type="string")
     */
    private $country;

    /**
     * The city related to the Page
     * @MongoDB\Field(type="string")
     */
    private $city;

    /**
     * The street address related to the Page
     * @MongoDB\Field(type="string")
     */
    private $streetAddress;

    /**
     * Coordinates related to the Page
     * @MongoDB\Field(type="collection")
     */
    private $coordinates;

    /** @MongoDB\NotSaved */
    private $latitude;
    /** @MongoDB\NotSaved */
    private $longitude;

    /**
     * Owner is the User who created the page
     * @MongoDB\ReferenceOne(targetDocument="User", inversedBy="pages") 
     */
    private $owner;

    /**
     * parentPage is the proprietary page of the page
     * @MongoDB\ReferenceOne(targetDocument="Page", inversedBy="childrenPages") 
     */
    private $parentPage;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Page", mappedBy="parentPage") 
     */
    private $childrenPages;

    /**
     * Contains all Groups added by the Owner of the page, for this Page
     * @MongoDB\ReferenceMany(targetDocument="Group", mappedBy="owner")
     */
    private $groups;

    /**
     * Contains all Groups wich this Page is member
     * @MongoDB\ReferenceMany(targetDocument="Group", mappedBy="members")
     */
    private $inGroups;

    /**
     * Profil image
     * @MongoDB\Field(type="string")
     *
     */
    private $imageProfil;

    /**
     * Banner image
     * @MongoDB\Field(type="string")
     *
     */
    private $imageBanner;

    /**
     * When isPrivate == true : the Page can't be found in the search page<br>
     * Informations of a private page can't be display to User who are not member of this page
     * @MongoDB\Field(type="boolean")
     *
     */
    private $isPrivate;

    /**
     * Reference all News mapped by News::author
     * Allow symfony to recover & save automaticaly all news created by the Page<br>
     * @MongoDB\ReferenceMany(targetDocument="News", mappedBy="author")
     */
    private $newsAuthor;

    /**
     * Reference all News mapped by News::signed
     * Allow symfony to recover & save automaticaly all news signed by the Page<br>
     * @MongoDB\ReferenceMany(targetDocument="News", mappedBy="signed")
     */
    private $newsSigned;

    /**
     * Reference all News mapped by News::target
     * Allow symfony to recover & save automaticaly all news targeted by the Page<br>
     * @MongoDB\ReferenceMany(targetDocument="News", mappedBy="target")
     */
    private $newsTarget;

    /**
     * Reference all Comments mapped by Comment::signed
     * Allow symfony to recover & save automaticaly all comments signed by the Page<br>
     * @MongoDB\ReferenceMany(targetDocument="Comment", mappedBy="signed")
     */
    private $commentsSigned;

    
    /**
     * Reference all Comments mapped by Comment::author
     * Allow symfony to recover & save automaticaly all comments where the Page is the author<br>
     * @MongoDB\ReferenceMany(targetDocument="CommentStream", mappedBy="authors")
     */
    private $comments;

    
    /**
     * Embed : Relation with other Pages
     * admins - friends - followers - participants - interested - members
     * @MongoDB\EmbedOne(targetDocument="Relation") 
     */
    private $relations;


    /**
     * @MongoDB\Field(type="string")
     */
    private $chatToken;

    /**
     * @MongoDB\Field(type="string")
     */
    private $chatStatus;


    /**
     * Confidentiality parameters related to the Page
     * @MongoDB\Field(type="collection")
     */
    private $confidentiality;
    
    /**
     * Confidentiality parameters related to the Page
     * @MongoDB\Field(type="collection")
     */
    private $preferences;

    /**
     * Embed : Informations complementary
     * @MongoDB\EmbedOne(targetDocument="PageInfo") 
     */
    private $informations;

    /**
     * Report = moderation
     * @MongoDB\ReferenceOne(targetDocument="Report") 
     */
    private $report;

    
    /**
     * When isActive == false : the Page can't be found in the search page<br>
     * @MongoDB\Field(type="boolean")
     *
     */
    private $isActive;
    
    /**
     * When isActive == false : the Page can't be found in the search page<br>
     * @MongoDB\Field(type="integer")
     *
     */
    private $firstStep;

    /**
     * Date initialized when the Page is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $created;


    /**
     * For EVENT
     * start date of an event
     * @MongoDB\Field(type="date")
     */
    private $startDate;


    /**
     * For EVENT
     * end date of an event
     * @MongoDB\Field(type="date")
     */
    private $endDate;


    /**
     * date of the last check of notification
     * @MongoDB\Field(type="date")
     */
    private $dateCheckNotif;


    /**
     * date of the last check of notification
     * @MongoDB\Field(type="date")
     */
    private $dateOnline;

    /**
     * @MongoDB\Field(type="string")
     */
    private $sourceKey;


    /**
     * Price for pages type Classified
     * @MongoDB\Field(type="float")
     *
     */
    private $price;

    /**
     * @MongoDB\Field(type="string")
     */
    private $currency; // €, $, G1, etc

    /**
     * @MongoDB\Field(type="string")
     */
    private $publicKeyG1;

    /****************************************************************************************/


    public function __construct()
    {
        $this->relations = new Relation($this);
        $this->isActive = false;
        $this->dateOnline = new \Datetime();
        $this->dateCheckNotif = new \Datetime();

        // if($this->type == Page::TYPE_USER)
        // $this->firstStep = 1;
        // else
        // $this->firstStep = 0;
        $this->firstStep = 1;
        $this->TYPES_AVAILABLES = 
            array(  Page::TYPE_USER,      Page::TYPE_ASSOCIATION, Page::TYPE_BUSINESS, 
                    Page::TYPE_FREEGROUP, Page::TYPE_NGO,         Page::TYPE_INSTITUTION, 
                    Page::TYPE_PROJECT,   Page::TYPE_EVENT,       Page::TYPE_AGORA,       
                    Page::TYPE_CLASSIFIED, Page::TYPE_MD_DOC);

        /* default confidentiality parameters, for all new Pages */
        $this->confidentiality = array(
                array( "SHOW_ADMINS"     => "CONF_PRIVATE",
                       "SHOW_ADMINOF"    => "CONF_PRIVATE",
                       "SHOW_GROUP"      => "CONF_PRIVATE",
                       "SHOW_FRIENDS"    => "CONF_FRIENDS",
                       "SHOW_MEMBEROF"   => "CONF_PRIVATE",
                       "SHOW_FOLLOWERS"  => "CONF_FOLLOWERS",
                       "SHOW_FOLLOWS"    => "CONF_PRIVATE",
                       "SHOW_PAGECREATED"=> "CONF_PRIVATE",
                       "SHOW_BIRTHDAY"   => "CONF_PRIVATE",

                       "SHOW_ADDRESS"    => "CONF_FRIENDS",
                       "SHOW_GEOPOS"     => "CONF_FRIENDS",

                       //INFORMATIONS
                       "SHOW_INFO_EMAIL"    => "CONF_NEXXO",
                       "SHOW_INFO_WEBSITE"  => "CONF_NEXXO",
                       "SHOW_INFO_COMMUNECTER" => "CONF_NEXXO",
                       "SHOW_INFO_FACEBOOK" => "CONF_NEXXO",
                       "SHOW_INFO_TWITTER"  => "CONF_NEXXO",
                       "SHOW_INFO_YOUTUBE"  => "CONF_NEXXO",
                       "SHOW_INFO_DISCORD"  => "CONF_NEXXO",

                       //CHAT
                       "AUTH_CHAT"          => "CONF_FRIENDS",
                        
                       //NEWS
                       "AUTH_SENDPOST"      => "CONF_FRIENDS"
                      )
            );

        /* default preferences parameters, for all new Pages */
        $this->preferences = array(
                array("AUTH_CHAT_SOUND"     => "true",
                      "AUTH_NOTIF_SOUND"    => "true",
                      "AUTH_NOTIF_EMAIL"    => "true",
                      "USE_DARK_THEME"      => "true"
                      )
            );
    }


    /**
     * @return mixed
     */
    public static function getTypeAvailables()
    {
        $types = Page::getStaticListTypeAvailables();

        $completTypes = array();
        foreach ($types as $key => $value) {
            $completTypes[] = array("type" => $value,
                                    "icon" => Page::getFaIconStatic($value),
                                    "color" => Page::getColorTypeStatic($value));
        }
        return $completTypes;
    }

    /**
     * @return mixed
     */
    public static function getTypeInteropSearch()
    {
        $types = array( Page::TYPE_USER,   
                        Page::TYPE_ASSOCIATION, 
                        Page::TYPE_PROJECT,     
                        Page::TYPE_EVENT);

        $completTypes = array();
        foreach ($types as $key => $value) {
            $type = $value; 
            if($type == "user") $type = "person";
            $completTypes[] = array("type" => $type,
                                    "icon" => Page::getFaIconStatic($value),
                                    "color" => Page::getColorTypeStatic($value));
        }
        return $completTypes;
    }
    
    /**
     * @return mixed
     */
    public static function getTypeMainSearch()
    {
        $types = array( Page::TYPE_USER,   
                        Page::TYPE_ASSOCIATION, 
                        Page::TYPE_FREEGROUP, 
                        Page::TYPE_BUSINESS,   
                        Page::TYPE_PROJECT,     
                        Page::TYPE_EVENT,     
                        Page::TYPE_CHANNEL,     
                        Page::TYPE_ASSEMBLY);

        $completTypes = array();
        foreach ($types as $key => $value) {
            $completTypes[] = array("type" => $value,
                                    "icon" => Page::getFaIconStatic($value),
                                    "color" => Page::getColorTypeStatic($value));
        }
        return $completTypes;
    }
    
    /**
     * @return mixed
     */
    public static function getTypeMainSearchSimple()
    {
        return array(   Page::TYPE_USER,   
                        Page::TYPE_ASSOCIATION, 
                        Page::TYPE_FREEGROUP, 
                        Page::TYPE_BUSINESS,   
                        Page::TYPE_PROJECT,     
                        Page::TYPE_EVENT);
    }
    
  
    /**
     * @return mixed
     */
    public function getListTypeAvailables()
    {
        return array(   Page::TYPE_USER,   
                        Page::TYPE_ASSOCIATION, 
                        Page::TYPE_FREEGROUP, 
                        Page::TYPE_BUSINESS,   
                        Page::TYPE_PROJECT,     
                        Page::TYPE_EVENT,     
                        Page::TYPE_AGORA,     
                        Page::TYPE_CHANNEL,
                        Page::TYPE_ASSEMBLY,
                        Page::TYPE_CLASSIFIED,
                        Page::TYPE_MD_DOC);
    }

    /**
     * @return mixed
     */
    public static function getStaticListTypeAvailables()
    {
        return array(   Page::TYPE_USER,   
                        Page::TYPE_ASSOCIATION, 
                        Page::TYPE_FREEGROUP, 
                        Page::TYPE_BUSINESS,   
                        Page::TYPE_PROJECT,     
                        Page::TYPE_EVENT,     
                        Page::TYPE_AGORA,     
                        Page::TYPE_CHANNEL,
                        Page::TYPE_AGORA,     
                        Page::TYPE_ASSEMBLY,     
                        Page::TYPE_CLASSIFIED,
                        Page::TYPE_MD_DOC);
    }

    /**
     * @return mixed
     */
    public function getTypeAvailablesSimple()
    {
        return $this->TYPES_AVAILABLES;
    }

    /**
     * @param mixed $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     * my description of getName
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = strip_tags($name);
        if($this->owner != null && $this->type == Page::TYPE_USER)
            $this->owner->setUsername($this->name);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDescriptionHtml()
    {
        $description = Helper::linkToHtml($this->description);
        return $description;
    }

    /**
     * @param mixed $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = strip_tags($description);
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags == null ? array() : $this->tags;
    }

    /**
     * @param mixed $tags
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    
    /**
     * @return mixed
     */
    public function getTagsStr()
    {
        return $this->tagsStr;
    }

    /**
     * @param mixed $tags
     * @return self
     */
    public function setTagsStrFromArray($tags)
    {
        $tagsStr = "";
        foreach ($tags as $key => $tag) {
            if($key < count($tags)) $tagsStr .= "#".$tag." ";
            else $tagsStr .= "#".$tag;
        }
        $this->setTagsStr($tagsStr);
    }

    /**
     * @param mixed $tags
     * @return self
     */
    public function setTagsStr($tagsStr)
    {
        $stripedText = strip_tags($tagsStr);
        $tags = Helper::extractHashtags($stripedText);
       
        //s'il y a des tags : on initialise la liste des tags
        //sinon on ne fait rien
        if(!empty($tags)) $this->setTags($tags);

        $this->tags = $tags;
        $this->tagsStr = $tagsStr;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = strip_tags($city);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * @param mixed $streetAddress
     * @return self
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = strip_tags($streetAddress);
        return $this;
    }


    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return self
     */
    public function setType($type)
    {
        $typeAv = $this->getListTypeAvailables();
        if(in_array($type, $typeAv))
            $this->type = $type;

        if($type != Page::TYPE_USER)
            $this->firstStep = 0;

        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     *
     * @return User
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentPage()
    {
        return $this->parentPage;
    }

    /**
     * @param Page $parentPage
     *
     * @return Page
     */
    public function setParentPage(Page $parentPage)
    {
        $this->parentPage = $parentPage;
        return $this;
    }

    /*
     * get childrenPages
     */
    public function getChildrenPages()
    {
        return $this->childrenPages;
    }

    /**
     * add group
     */
    public function addGroup(Group $group)
    {
        $this->groups[] = $group;
    }

    /**
     * get groups
     */
    public function getGroups()
    {
        return $this->groups;
    }
    /**
     * remove group
     */
    public function removeGroup(Group $group)
    {   
        $this->groups->removeElement($group);
    }

    /**
     * get inGroups
     */
    public function getInGroups()
    {
        return $this->inGroups;
    }
    /**
     * add inGroups
     */
    public function addInGroup(Group $group)
    {
        $this->inGroups[] = $group;
    }

   
    /**
     * remove member
     */
    public function removeInGroup(Group $group)
    {   
        $this->inGroups->removeElement($group);
    }
    /**
     * add newsAuthor
     */
    public function addNewsAuthor(News $news)
    {
        $this->newsAuthor[] = $news;
    }

    /**
     * get news
     */
    public function getNewsAuthor()
    {
        return $this->newsAuthor;
    }

    /**
     * add newsSigned
     */
    public function addNewsSigned(News $news)
    {
        $this->newsSigned[] = $news;
    }

    /**
     * get newsSigned
     */
    public function getNewsSigned()
    {
        return $this->newsSigned;
    }

    /**
     * add newsTarget
     */
    public function addNewsTarget(News $news)
    {
        $this->newsTarget[] = $news;
    }

    /**
     * get comments
     */
    public function getCommentsStreams()
    {
        return $this->comments;
    }

    /**
     * get commentsSigned
     */
    public function getCommentsSigned()
    {
        return $this->commentsSigned;
    }


    /**
     * get newsTarget
     */
    public function getNewsTarget()
    {
        return $this->newsTarget;
    }


    /**
     * Get created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get startDate
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Get startDateStr
     */
    public function getStartDateStr()
    {
        return $this->startDate == null ? null : $this->startDate->format('Y-m-d H:i:s');
    }

    /**
     * Set startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * Get endDate
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Get endDate
     */
    public function getEndDateStr()
    {
        return $this->endDate == null ? null : $this->endDate->format('Y-m-d H:i:s');
    }

    /**
     * Set endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * Get dateCheckNotif
     */
    public function getDateCheckNotif()
    {
        return $this->dateCheckNotif;
    }

    /**
     * Set dateCheckNotif
     */
    public function setDateCheckNotif($dateCheckNotif)
    {
        $this->dateCheckNotif = $dateCheckNotif;
        return $this;
    }
    /**
     * Get dateOnline
     */
    public function getDateOnline()
    {
        return $this->dateOnline;
    }

    /**
     * Set dateOnline
     */
    public function setDateOnline($dateOnline)
    {
        $this->dateOnline = $dateOnline;
        return $this;
    }
    
    /**
     * Get sourceKey
     */
    public function getSourceKey()
    {
        return $this->sourceKey;
    }

    /**
     * Set sourceKey
     */
    public function setSourceKey($sourceKey)
    {
        $this->sourceKey = $sourceKey;
        return $this;
    }


    /**
     * Get coordinates
     */
    public function getCoordinates()
    {   if(empty($this->coordinates)) return array("false", "false");
        if($this->coordinates[0] == 0.0 && $this->coordinates[1] == 0.0) return array("false", "false");
        //dd($this->coordinates);
        return $this->coordinates;
    }

    /**
     * Set coordinates
     */
    public function setCoordinates($coordinates)//: array
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude)//: float
    {
        $this->latitude = $latitude;
        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude)//: float
    {
        $this->longitude = $longitude;
        return $this;
    }

    public function getImageProfilAsset()
    {
        if(strpos($this->imageProfil, "http") !== false){
            return $this->imageProfil;
        }else{
            if($this->imageProfil == "") {
                if($this->getType() == Page::TYPE_CLASSIFIED) 
                     return "/uploads/imgProfil/default_classified.png";
                else return "/uploads/imgProfil/default.png";
            }

            return "/uploads/imgProfil/".$this->imageProfil;
        }
        
        return $this->imageProfil;
    }
    public function getImageProfil()
    {
        if($this->imageProfil == "") return "default.png";
        return $this->imageProfil;
    }

    public function setImageProfil($imageProfil)
    {
        $this->imageProfil = $imageProfil;
        return $this;
    }

    public function getImageBanner()
    {
        if($this->imageBanner == "") return "default.png";
        return $this->imageBanner;
    }

    public function getImageBannerAsset()
    {
        if(strpos($this->imageBanner, "http") !== false){
            return $this->imageBanner;
        }else{
            if($this->imageBanner == "") return "/uploads/imgBanner/default.png";
            return "/uploads/imgBanner/".$this->imageBanner;
        }
    }
    public function getImageBannerAssetMin()
    {
        if(strpos($this->imageBanner, "http") !== false){
            return $this->imageBanner;
        }else{
            if($this->imageBanner == "") return "/uploads/imgBanner/min/default.png";
            return "/uploads/imgBanner/min/".$this->imageBanner;
        }
    }

    public function setImageBanner($imageBanner)
    {
        $this->imageBanner = $imageBanner;
        return $this;
    }

    public function isPrivate(): bool
    {
        return $this->isPrivate == null ? false : $this->isPrivate;
    }

    public function setIsPrivate($isPrivate): bool 
    {
        $this->isPrivate = $isPrivate;
        return $this->isPrivate;
    }


    public function getRelations()
    {
        return $this->relations;
    }

    public function setRelations($relations)
    {   
        $this->relations = $relations;
        return $this;
    }



    /*
     * Get chatToken
     */
    public function initChatToken()
    {
        $this->chatToken = bin2hex(random_bytes(20)); // 20 chars //$this->chatToken;
        return $this->chatToken;
    }

    /*
     * Get chatToken
     */
    public function getChatToken()
    {
        return $this->chatToken;
    }

    /*
     * Set chatToken
     */
    public function setChatToken($chatToken)
    {
        $this->chatToken = $chatToken;
        return $this;
    }


    /*
     * Get chatStatus
     */
    public function getChatStatus()
    {
        if($this->type != Page::TYPE_USER) return "online";
            
        if($this->chatStatus == "offline") return "offline";
        
        $date = new \Datetime();
        $dateMin = $date->sub(new \DateInterval("PT120S"));

        if($dateMin < $this->dateOnline){
            if($this->chatStatus == "away") return $this->chatStatus;
            return "online" ;
        }else{
            return "offline";
        }
        //return $this->chatStatus == null ? "online" : $this->chatStatus;
    }

    /*
     * Set chatStatus
     */
    public function setChatStatus($chatStatus)
    {
        $this->chatStatus = $chatStatus;
        return $this;
    }

    /*
     * Get informations
     */
    public function getInformations()
    {
        return $this->informations == null ? new PageInfo() : $this->informations;
    }

    /*
     * Set informations
     */
    public function setInformations(PageInfo $informations)
    {
        $this->informations = $informations;
        return $this;
    }


    /**
     * Get report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set report
     */
    public function setReport(Report $report)
    {
        $this->report = $report;
        return $this;
    }

    /**
     * Remove report
     */
    public function removeReport()
    {
        $this->report = null;
        return $this;
    }

    /**
     * Get isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }


    /**
     * Get firstStep
     */
    public function getFirstStep()
    {
        return $this->firstStep === null ? 1 : $this->firstStep;
    }

    /**
     * Set firstStep
     */
    public function setFirstStep($firstStep)
    {
        $this->firstStep = $firstStep;
        return $this;
    }

    /**
     * Incremente firstStep
     */
    public function incFirstStep()
    {
        $this->firstStep = $this->firstStep + 1;
        return $this;
    }



    /**
     * Get price
     */
    public function getPrice()
    {
        return $this->price != null ? $this->price : 0;
    }

    /**
     * Set price
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }



    /**
     * Get currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set currency
     */
    public function setCurrency($publicKeyG1)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Get publicKeyG1
     */
    public function getPublickeyG1()
    {
        return $this->publicKeyG1;
    }

    /**
     * Set publicKeyG1
     */
    public function setPublickeyG1($publicKeyG1)
    {
        $this->publicKeyG1 = $publicKeyG1;
        return $this;
    }


    public function refactorSlugChatMessage($oldSlug, $em){
        $chatRepo = $em->getRepository(ChatMessage::class);
        $messages = $chatRepo->findBy(array('$or' => array(array('receiverSlug' => $oldSlug),
                                                           array("senderSlug" =>  $oldSlug)
                                                          ),
                                            ));
        foreach ($messages as $key => $message) {
            if($message->getSenderSlug() == $oldSlug){
                $message->setSenderSlug($this->slug);
                $message->setSenderUsername($this->name);
            }
            if($message->getReceiverSlug() == $oldSlug) $message->setReceiverSlug($this->slug);
            $em->flush($message);
        }
    }

    public function cleanLock($em){
        //supprime les notifications liées à cette news
        $notifRepo = $em->getRepository(Notification::class);
        $notifs = $notifRepo->findBy(array('aboutType'=>"page",
                                           'aboutId' => $this->getId())); 
        foreach ($notifs as $n => $notif) {
            $em->remove($notif);
            $em->flush();
        }
        $notifs = $notifRepo->findBy(array('authors.$id'=>array('$in'=>array(new \MongoId($this->getId()))))); 
        foreach ($notifs as $n => $notif) {
            $em->remove($notif);
            $em->flush();
        }

        //desactivate all news publicated by this page
        $news = $this->getNewsAuthor();
        foreach ($news as $n => $post) {
            $sharedBy = $post->getSharedBy();
            foreach ($sharedBy as $n => $news1) {
                $news1->setIsActive(false);
                $news1->cleanLock($em);
                $em->flush($news1);
            }
            $post->setIsActive(false);
            $em->flush($post);
        }
        //desactivate all news signed by this page
        $news = $this->getNewsSigned();
        foreach ($news as $n => $post) {
            $sharedBy = $post->getSharedBy();
            foreach ($sharedBy as $n => $news1) {
                $news1->setIsActive(false);
                $news1->cleanLock($em);
                $em->flush($news1);
            }
            $post->setIsActive(false);
            $em->flush($post);
        }
    }

    public function cleanUnlock($em){
        
        //resactivate all news publicated by this page
        $news = $this->getNewsAuthor();
        foreach ($news as $n => $post) {
            $sharedBy = $post->getSharedBy();
            foreach ($sharedBy as $n => $news1) {
                $news1->setIsActive(true);
                $news1->cleanUnlock($em);
                $em->flush($news1);
            }
            $post->setIsActive(true);
            $em->flush($post);
        }
        //desactivate all news signed by this page
        $news = $this->getNewsSigned();
        foreach ($news as $n => $post) {
            $sharedBy = $post->getSharedBy();
            foreach ($sharedBy as $n => $news1) {
                $news1->setIsActive(true);
                $em->flush($news1);
            }
            $post->setIsActive(true);
            $em->flush($post);
        }
    }

    public function cleanRemove($em, $delete=true){

        dump("Page: start remove page - ".$this->getName());
        //dump("Page: start remove notifications à propos de cette page");
        //supprime les notifications liées à cette page
        $notifRepo = $em->getRepository(Notification::class);
        $notifs = $notifRepo->findBy(array('aboutType'=>"page",
                                           'aboutId' => $this->getId())); 
        foreach ($notifs as $n => $notif) {
            $em->remove($notif);
            $em->flush();
        }
        //supprime les notifications liées à cette page
        $notifRepo = $em->getRepository(Notification::class);
        $notifs = $notifRepo->findBy(array('whatType'=>"page",
                                           'whatId' => $this->getId())); 
        foreach ($notifs as $n => $notif) {
            $em->remove($notif);
            $em->flush();
        }


        //dump("Page: start remove notifications dont cette page a été l'auteur");
        $notifs = $notifRepo->findBy(array('authors.$id'=>array('$in'=>array(new \MongoId($this->getId()))))); 
        foreach ($notifs as $n => $notif) {
            $em->remove($notif);
            $em->flush();
        }

        //dump("News: start remove all reports about this page");
        //supprime les reports liées à ce commentaire
        $reportRepo = $em->getRepository(Report::class);
        $reports = $reportRepo->findBy(array('aboutType'=>"page",
                                             'aboutId' => $this->getId())); 
        foreach ($reports as $n => $report) {
            $report->removeAuthor($this);
            $report->removeVoteUp($this);
            $report->removeVoteDown($this);
            $em->flush($report);
        }
        
        //dump("News: start remove all reports dont cette page a été l'auteur");
        //supprime les reports liées à ce commentaire
        $reportRepo = $em->getRepository(Report::class);
        $reports = $reportRepo->findBy(array('authors.$id'=>array('$in'=>array(new \MongoId($this->getId()))))); 
        foreach ($reports as $n => $report) {
            $em->remove($report);
            $em->flush();
        }
        
        //dump("Page: start remove all news publiées par cette page (author)");
        //remove all news publicated by this page
        $news = $this->getNewsAuthor();
        foreach ($news as $n => $post) {
            $sharedBy = $post->getSharedBy();
            foreach ($sharedBy as $n => $news1) {
                $news1->cleanRemove($em);
                //$em->flush($news1);
            }
            $post->cleanRemove($em);
            //$em->flush($post);
        }
        //dump("Page: start remove all news publiées par cette page (signed)");
        //remove all news signed by this page
        $news = $this->getNewsSigned();
        foreach ($news as $n => $post) {
            $sharedBy = $post->getSharedBy();
            foreach ($sharedBy as $n => $news1) {
                $news1->cleanRemove($em);
                $em->flush($news1);
            }
            $post->cleanRemove($em);
            //$em->flush($post);
        }
        //dump("Page: start remove all news publiées sur cette page (target)");
        //remove all news targeted on this page
        $news = $this->getNewsTarget();
        foreach ($news as $n => $post) {
            $sharedBy = $post->getSharedBy();
            foreach ($sharedBy as $n => $news1) {
                $news1->cleanRemove($em);
                $em->flush($news1);
            }
            $post->cleanRemove($em);
            //$em->flush($post);
        }

        //dump("Page: start remove all my groups");
        //remove all my groups
        $groups = $this->getGroups();
        foreach ($groups as $n => $group) {
        
            $members = $group->getMembers();
            foreach ($members as $n => $member) {
                $member->removeInGroup($group);
                $em->flush($member);
            }
            $this->removeGroup($group);
            $em->remove($group);
            $em->flush($this);
            //$em->flush($post);
        }

        //dump("Page: start remove my page from groups");
        //remove  my page from groups
        $groups = $this->getInGroups();
        foreach ($groups as $n => $group) {
            $group->removeMember($this);
            $em->flush($group);
        }

        $css = $this->getCommentsStreams();
        //dd($comments);
        foreach ($css as $n => $cs) { dump($cs);
            //$cs = $comment->getParentStream();
            $stream = $cs->getComments();
            foreach ($stream as $k => $comment) { dump($comment);
                    $comment->cleanRemove($em, $this->getSlug());
                    $cs->removeAuthor($this);
                    $em->flush($cs);

            }
            $em->flush($cs);
        }
        //dd("stop");
        //dump("Page: start remove all my comments");
        //supprime tous les commentaires publiés par cette page
        /*$csRepo = $em->getRepository(CommentStream::class);
        $css = $csRepo->findBy(array('comments.author.$id'=> array('$in'=>array(new \MongoId($this->getId()))))); 
        foreach ($css as $n => $cs) {
            foreach ($cs->getComments() as $nn => $comment) {
                if( $comment->getSigned()->getId() == $this->getId() || 
                    $comment->getAuthor()->getId() == $this->getId()){ 
                    ////dump("remove my comment : ".$comment->getText());
                    $comment->cleanRemove($em);
                    $cs->removeComment($comment);
                    $em->flush($cs);
                }
            }
        }*/
        //dump("Page: start remove all my likes on comments");
        //supprime tous les commentaires publiés par cette page
        $csRepo = $em->getRepository(CommentStream::class);
        $css = $csRepo->findBy(array('comments.likes.$id'=> array('$in'=>array(new \MongoId($this->getId()))))); 
        foreach ($css as $n => $cs) {
            foreach ($cs->getComments() as $nn => $comment) {
                $comment->cleanRemoveLike($this);
                //dump("DELETE A LIKE on comment");
                $em->flush($cs);
            }
        }
        //dump("Page: start remove all my dislikes on comments");
        //supprime tous les commentaires publiés par cette page
        $csRepo = $em->getRepository(CommentStream::class);
        $css = $csRepo->findBy(array('comments.dislikes.$id'=> array('$in'=>array(new \MongoId($this->getId()))))); 
        foreach ($css as $n => $cs) {
            foreach ($cs->getComments() as $nn => $comment) {
                $comment->removeDislike($this);
                //dump("DELETE A DISLIKE on comment");
                $em->flush($cs);
            }
        }
        //dump("Page: start remove all my likes on Pages");
        //supprime tous les commentaires publiés par cette page
        $pageRepo = $em->getRepository(Page::class);
        $pages = $pageRepo->findBy(array('relations.likers.$id'=> array('$in'=>array(new \MongoId($this->getId()))))); 
        foreach ($pages as $n => $page) {        
            $page->getRelations()->removeLiker($this);
            $em->flush($page);
            //dump("DELETE A LIKE on Page");        
        }

        //dump("Page: start remove all my likes on News");
        //supprime tous les commentaires publiés par cette page
        $newsRepo = $em->getRepository(News::class);
        $news = $newsRepo->findBy(array('likes.$id'=> array('$in'=>array(new \MongoId($this->getId()))))); 
        foreach ($news as $n => $post) {        
            $post->removeLike($this);
            $em->flush($post);
            //dump("DELETE A LIKE on News");        
        }
        //dump("Page: start remove all my dislikes on News");
        //supprime tous les commentaires publiés par cette page
        $newsRepo = $em->getRepository(News::class);
        $news = $newsRepo->findBy(array('dislikes.$id'=> array('$in'=>array(new \MongoId($this->getId()))))); 
        foreach ($news as $n => $post) {        
            $post->removeDislike($this);
            $em->flush($post);
            //dump("DELETE A DISLIKE on News");        
        }
        //dd("wait");

        // RELATIONS

        //dump("Page: start remove all relations");

        foreach ($this->getRelations()->getFriends() as $key => $pageRel) {
          $pageRel->getRelations()->removeFriend($this);
          $em->flush($pageRel);   
        }
        
        foreach ($this->getRelations()->getFollowers() as $key => $pageRel) {
          $pageRel->getRelations()->removeFollow($this);
          $em->flush($pageRel);   
        }
        
        foreach ($this->getRelations()->getFollows() as $key => $pageRel) {
          $pageRel->getRelations()->removeFollower($this);
          $em->flush($pageRel);   
        }

        foreach ($this->getRelations()->getParticipants() as $key => $pageRel) {
          $pageRel->getRelations()->removeParticipateTo($this);
          $em->flush($pageRel);   
        }

        foreach ($this->getRelations()->getParticipateTo() as $key => $pageRel) {
          $pageRel->getRelations()->removeParticipant($this);
          $em->flush($pageRel);   
        }

        foreach ($this->getRelations()->getLikers() as $key => $pageRel) {
          $pageRel->getRelations()->removeLike($this);
          $em->flush($pageRel);   
        }

        foreach ($this->getRelations()->getLikes() as $key => $pageRel) {
          $pageRel->getRelations()->removeLiker($this);
          $em->flush($pageRel);   
        }

        //close all chatContact (dans les 2 sens)
        $chatMessageRepo = $em->getRepository(ChatMessage::class);
        foreach ($this->getRelations()->getChatContacts() as $key => $pageRel) {
            $pageRel->getRelations()->removeChatContacts($this);
            $em->flush($pageRel);

            //supprime les chatContacts des pages != user
            if($this->getType() != Page::TYPE_USER){
                $pageRepo = $em->getRepository(Page::class);
                $chatRequests= $pageRepo->findBy(array('relations.chatContacts.$id' => new \MongoId($this->getId())));
                foreach ($chatRequests as $key => $contact) {
                    $contact->getRelations()->removeChatContacts($this);
                    //dump($contact->getName());
                    $em->flush($contact);
                }
            }
        }

        //DELETE MESSAGE CHAT
        if($this->getType() == Page::TYPE_USER){
            $res = $chatMessageRepo->findBy(array('$or' => array(array('senderSlug' => $this->getSlug()),
                                                                 array('receiverSlug' => $this->getSlug())),
                                                ));
        }else{          
            $res = $chatMessageRepo->findBy(array('receiverSlug' => $this->getSlug()),
                                            array("created" => "DESC"), 8);
        }
        foreach ($res as $key => $msg) {
            $em->remove($msg); 
        }

        $pageRepo = $em->getRepository(Page::class);
        $friendsRequests= $pageRepo->findBy(array('relations.friendsRequests.$id' => new \MongoId($this->getId())));
        foreach ($friendsRequests as $key => $friend) {
            $friend->getRelations()->removeFriendRequest($this);
            $em->flush($friend);   
        }

        $pageRepo = $em->getRepository(Page::class);
        $friendsRequests= $pageRepo->findBy(array('relations.adminsRequests.$id' => new \MongoId($this->getId())));
        foreach ($friendsRequests as $key => $friend) {
            $friend->getRelations()->removeAdminRequest($this);
            $em->flush($friend);   
        }
        //dump($friendsRequests);
        //dd("stop");

        //ADMINS
        foreach ($this->getRelations()->getIsAdminOf() as $key => $pageRel) {
          if($pageRel->getSlug() != $this->getSlug()){
              $admins = $pageRel->getRelations()->getAdmins();
              if(sizeof($admins) > 1){ //sil y a plusieurs admin, je me retire de la liste
                //dump("NO LONGER ADMIN");
                $pageRel->getRelations()->removeAdmin($this);
                $this->getRelations()->removeIsAdminOf($pageRel);
                $admins = $pageRel->getRelations()->getAdmins();
                $newOwner = null;
                foreach ($admins as $key => $admin) {
                    $newOwner = $admin->getOwner(); break;
                }
                ////dump($newOwner);
                //$newOwner = $admins[0];
                $pageRel->setOwner($newOwner);
                //dump("NEW OWNER ".$newOwner->getUsername());
                
                $em->flush($pageRel); 
              }else{ //si je suis le seul admin, je supprime la page (cascade)
                //dump("DELETE THE PAGE ".$pageRel->getName());
                $pageRel->cleanRemove($em, false);
              }
          }
        }
        
        foreach ($this->getRelations()->getAdmins() as $key => $pageRel) {
          $pageRel->getRelations()->removeIsAdminOf($this);
          $em->flush($pageRel); 
        }

        dump("Page: end remove page - ".$this->getName());

        if($delete == true){
            if($this->type == Page::TYPE_USER)
                //$this->getOwner()->anonymify();
                $em->remove($this->getOwner()); 
        }
        //$this->anonymify();
        $em->remove($this); 
        $em->flush();   

        
        //}
        return true;
    }

    /*
     * Anonymifaction instead of remove
     * CURRENTLY NOT USED
     */
    public function anonymify()
    {
        $this->relations = new Relation($this);
        $this->isActive = false;

        $this->name = "";
        $this->slug = "";
        $this->imageProfil = "";
        $this->imageBanner = "";
        $this->startDate = null;
        $this->endsDate = null;
        $this->dateOnline = null;
        $this->dateCheckNotif = null;
        $this->firstStep = 1;
        $description="";
        $tags = "";
        $tagsStr = "";
        $country = "";
        $city = "";
        $streetAddress = "";
        $coordinates = null;
        $owner = null;
        $parentPage = null;
        /*$parentPage;
        $childrenPages;
        $groups;
        $inGroups;
        $imageProfil;
        $imageBanner;
        $isPrivate;
        $newsAuthor;
        $newsSigned;
        $newsTarget;
        $commentsSigned;
        $comments;
        $relations;
        $chatToken;
        $chatStatus;
        $confidentiality;
        $preferences;
        $informations;
        $report;
        $isActive;
        $firstStep;
        $created;
        $startDate;
        $endDate;
        $dateCheckNotif;
        $dateOnline;*/

    }

    /*
     * Get confidentiality
     */
    public function getConfidentiality()
    {   
        $allConf = array(   "SHOW_ADMINS"     => "CONF_PRIVATE",
                           "SHOW_ADMINOF"    => "CONF_PRIVATE",
                           "SHOW_GROUP"      => "CONF_PRIVATE",
                           "SHOW_FRIENDS"    => "CONF_FRIENDS",
                           "SHOW_MEMBEROF"   => "CONF_PRIVATE",
                           "SHOW_FOLLOWERS"  => "CONF_FOLLOWERS",
                           "SHOW_FOLLOWS"    => "CONF_PRIVATE",
                           "SHOW_PAGECREATED"=> "CONF_PRIVATE",
                           "SHOW_BIRTHDAY"   => "CONF_PRIVATE",

                           "SHOW_ADDRESS"    => "CONF_FRIENDS",
                           "SHOW_GEOPOS"     => "CONF_FRIENDS",

                           //INFORMATIONS
                           "SHOW_INFO_EMAIL"    => "CONF_NEXXO",
                           "SHOW_INFO_WEBSITE"  => "CONF_NEXXO",
                           "SHOW_INFO_COMMUNECTER" => "CONF_NEXXO",
                           "SHOW_INFO_FACEBOOK" => "CONF_NEXXO",
                           "SHOW_INFO_TWITTER"  => "CONF_NEXXO",
                           "SHOW_INFO_YOUTUBE"  => "CONF_NEXXO",
                           "SHOW_INFO_DISCORD"  => "CONF_NEXXO",

                           //CHAT
                           "AUTH_CHAT"          => "CONF_FRIENDS",
                            
                           //NEWS
                           "AUTH_SENDPOST"      => "CONF_FRIENDS"
                           );

        //check if the data has all key initialised
        foreach ($allConf as $key => $value) {
            if(!@$this->confidentiality[0][$key])
                $this->confidentiality[0][$key] = "CONF_NEXXO";
        }

        return isset($this->confidentiality[0]) ? 
                     $this->confidentiality[0] : 
                     $allConf;
    }

    /*
     * Get return
     */
    public function initConfidentiality()
    {
        if(!isset($this->confidentiality[0])){
            $this->confidentiality = array();
            $this->confidentiality[0] = array();
        }

        $allKeys = array("SHOW_ADMINS" ,"SHOW_ADMINOF","SHOW_GROUP"  ,"SHOW_FRIENDS","SHOW_MEMBEROF",
                        "SHOW_FOLLOWERS","SHOW_FOLLOWS","SHOW_PAGECREATED","SHOW_BIRTHDAY","SHOW_ADDRESS","SHOW_GEOPOS",
                        "SHOW_INFO_EMAIL", "SHOW_INFO_WEBSITE", "SHOW_INFO_FACEBOOK", "SHOW_INFO_COMMUNECTER", "SHOW_INFO_TWITTER", 
                        "SHOW_INFO_YOUTUBE", "SHOW_INFO_DISCORD", 
                        "AUTH_CHAT", "AUTH_SENDPOST");
        foreach($allKeys as $key){
            if(!isset($this->confidentiality[0][$key])) 
                $this->confidentiality[0][$key] = "CONF_PRIVATE";
        }
    }

    /*
     * Set return
     */
    public function setConfidentiality($confidentiality)
    {
        $this->confidentiality = array($confidentiality);
        return $this;
    }

    /*
     * Set return
     */
    public function setConfidentialityByKey($key, $val)
    {
        $this->confidentiality[0][$key] = $val;
        //dd($this->confidentiality);
        return $this;
    }

    public function getConfAuth($confkey){
        //dd($this->getConfidentiality()[$confkey]);
        if(isset($this->getConfidentiality()[$confkey]))
            return $this->getConfidentiality()[$confkey];
        else return false;
    }

    public function isConfAuth($confkey, $user){

        /*if($user->getIsActive() == false && $this->slug != $user->getSlug()){
            if($confkey != "SHOW_ADDRESS" && $confkey != "SHOW_GEOPOS")
            return false;
        }*/

        if($this->slug == $user->getSlug()) return true; //have all right in your page
        if($this->getRelations()->getIsAdmin($user->getSlug())) return true; //have all right in your admin's page
        
        $conf = $this->getConfAuth($confkey);
        
        if($conf == Page::CONF_ADMINS){
            return $this->getRelations()->getIsAdmin($user->getSlug());
        }
        if($conf == Page::CONF_FRIENDS){
            return $this->getRelations()->getImFriend($user->getSlug());
        }
        if($conf == Page::CONF_FOLLOWERS){
            if($this->getRelations()->inFollowers($user->getSlug())) return true;
            if($this->getRelations()->getImFriend($user->getSlug())) return true;
        }
        if($conf == Page::CONF_PRIVATE){
            return $user->getSlug() == $this->slug;
        }
        if($conf == Page::CONF_NEXXO){
            return $this->isActive;
        }
        if($conf == Page::CONF_ALL){
            return true;
        }

        return false;
    }



    /*
     * Get preferences
     */
    public function getPreferences()
    {
        return $this->preferences[0];
    }

    /*
     * Set preferences
     */
    public function setPreferences($preferences)
    {
        $this->preferences = array($preferences);
        return $this;
    }


    public function isPrefAuth($confkey){
        if(isset($this->getPreferences()[$confkey]))
            return $this->getPreferences()[$confkey];
        else return false;
    }


    /**
    * Return a clean array with data to be used in view by Javascript<br>
    * Currently it is used for SIG, to display data on the map 
    */
    public function getJson($userPage): array
    {
        $data = array(
           "id" => $this->id,
           "name" => $this->name,
           "description" => $this->description,
           "slug" => $this->slug,
           "type" => $this->type,
           "imageBanner" => $this->getImageBanner(),
           "imageProfil" => $this->getImageProfil(),
        );

        if($this->getParentPage() != null){
           $data["parent"] = $this->getParentPage()->getJson($userPage);
        }

        //{% if page.isConfAuth("AUTH_CHAT", app.user.myUserPage.slug) %}
        if($this->isConfAuth("SHOW_ADDRESS", $userPage)){
            $data["country"]        = $this->country;
            $data["city"]           = $this->city;
            $data["streetAddress"]  = $this->streetAddress;
        }

        //{% if page.isConfAuth("AUTH_CHAT", app.user.myUserPage.slug) %}
        if($this->isConfAuth("SHOW_GEOPOS", $userPage)){
            $data["coordinates"]    = $this->coordinates;
        }

        //{% if page.isConfAuth("AUTH_CHAT", app.user.myUserPage.slug) %}
        //dd($this->isConfAuth("SHOW_INFO_WEBSITE", $userPage));
        if($this->isConfAuth("SHOW_INFO_WEBSITE", $userPage)){
            $data["website"]    = $this->getInformations()->getWebsite();
        }

        if($this->type == Page::TYPE_EVENT){
            $data["startDate"]   = $this->startDate;
            $data["endDate"]     = $this->endDate;
        }

         return $data;
    }

    /**
     * return the fa-icon related to the attribut Type of the Page
     */
    public function getFaIcon(){
        $type = $this->type;
        return Page::getFaIconStatic($type);
    }
    /**
     * return the color related to the attribut Type of the Page<br>
     * cf : assets/css/nexxo_style.css to see all color availables (letter-color & border-color)
     */
    public function getColorType(){
        $type = $this->type;
        if($type == Page::TYPE_USER)        return "yellow";
        if($type == Page::TYPE_ASSOCIATION) return "green";
        if($type == Page::TYPE_BUSINESS)    return "azure";
        if($type == Page::TYPE_FREEGROUP)   return "blue";   
        if($type == Page::TYPE_NGO)         return "turq";        
        if($type == Page::TYPE_INSTITUTION) return "red"; 
        if($type == Page::TYPE_PROJECT)     return "purple";
        if($type == Page::TYPE_EVENT)       return "turqg";
        if($type == Page::TYPE_AGORA)       return "yellow";
        if($type == Page::TYPE_ASSEMBLY)    return "yellow";
        if($type == Page::TYPE_CHANNEL)     return "red";
        if($type == Page::TYPE_CLASSIFIED)  return "yellow";
        if($type == Page::TYPE_MD_DOC)      return "yellow";
        return "dark";
    }

    /**
     * return the fa-icon related to the attribut Type of the Page
     */
    public static function getFaIconStatic($type){ 
        if($type == Page::TYPE_USER)        return "user";
        if($type == Page::TYPE_ASSOCIATION) return "group";
        if($type == Page::TYPE_BUSINESS)    return "industry";
        if($type == Page::TYPE_FREEGROUP)   return "circle";  
        if($type == Page::TYPE_NGO)         return "group";        
        if($type == Page::TYPE_INSTITUTION) return "university"; 
        if($type == Page::TYPE_PROJECT)     return "lightbulb-o";
        if($type == Page::TYPE_EVENT)       return "calendar";
        if($type == Page::TYPE_AGORA)       return "connectdevelop";
        if($type == Page::TYPE_CHANNEL)     return "youtube-play";
        if($type == Page::TYPE_ASSEMBLY)    return "sun-o";
        if($type == Page::TYPE_CLASSIFIED)  return "thumb-tack";
        if($type == Page::TYPE_MD_DOC)      return "file";
        return "circle";
    }
    /**
     * return the color related to the attribut Type of the Page<br>
     * cf : assets/css/nexxo_style.css to see all color availables (letter-color & border-color)
     */
    public static function getColorTypeStatic($type){
        if($type == Page::TYPE_USER)        return "yellow";
        if($type == Page::TYPE_ASSOCIATION) return "green";
        if($type == Page::TYPE_BUSINESS)    return "azure";
        if($type == Page::TYPE_FREEGROUP)   return "blue";   
        if($type == Page::TYPE_NGO)         return "turq";        
        if($type == Page::TYPE_INSTITUTION) return "red"; 
        if($type == Page::TYPE_PROJECT)     return "purple";
        if($type == Page::TYPE_EVENT)       return "turqg";
        if($type == Page::TYPE_AGORA)       return "yellow";
        if($type == Page::TYPE_CHANNEL)     return "red";
        if($type == Page::TYPE_ASSEMBLY)    return "yellow";
        if($type == Page::TYPE_CLASSIFIED)  return "yellow";
        if($type == Page::TYPE_MD_DOC)      return "file";
        return "dark";
    }

    public function __toString(){
        return $this->name;
    }

    public function getFormConfidentiality(){
        return array("user" => array(

                    //CHAT
                    "AUTH_CHAT"          => array("h6"=>"Instant messaging",
                                                "lbl"=>"Who has the right to send you private messages ?"),
                    
                    //NEWS
                    "AUTH_SENDPOST"          => array("h6"=>"Publish on the page",
                                                "lbl"=>"Who has the right to publish on this page ?"),

                    //NETWORK
                    "SHOW_ADMINS"      => array("h6"=>"Administrators",
                                                "lbl"=>"Who has the right to see the list of administrators of this page ?"),

                    "SHOW_ADMINOF"     => array("h6"=>"Pages you are an administrator",
                                                "lbl"=>"Qui a le droit de savoir de quelles pages vous êtes administrateur ?"),
                    
                    "SHOW_GROUP"       => array("h6"=>"Friends groups",
                                                "lbl"=>"Who has the right to see your groups of friends ?"),

                    "SHOW_FRIENDS"     => array("h6"=>"Your friends",
                                                "lbl"=>"Who has the right to see your list of friends ?"),

                    "SHOW_MEMBEROF"    => array("h6"=>"Pages of which you are member",
                                                "lbl"=>"Who has the right to know the pages of which you are a member ?"),

                    "SHOW_FOLLOWERS"   => array("h6"=>"Your followers",
                                                "lbl"=>"Who has the right to display the list of your subscribers ?"),

                    "SHOW_FOLLOWS"     => array("h6"=>"Pages you follow",
                                                "lbl"=>"Who has the right to view the list of pages you follow ?"),

                    "SHOW_PAGECREATED" => array("h6"=>"Pages you have created",
                                                "lbl"=>"Who has the right to see your pages ?"),

                    "SHOW_ADDRESS"     => array("h6"=>"Your address",
                                                "lbl"=>"Who has the right to know your address ?"),

                    "SHOW_GEOPOS"      => array("h6"=>"Geographical position",
                                                "lbl"=>"Who has the right to know your geographical position, and view your icon on the map ?"),
                       
                        
                    ),
                "page" => array(
                    //CHAT
                    "AUTH_CHAT"        => array("h6"=>"Instant messaging",
                                                "lbl"=>"Who has the right to send you private messages ?"),
                    
                    //NEWS
                    "AUTH_SENDPOST"    => array("h6"=>"Publish on the page",
                                                "lbl"=>"Who has the right to publish on this page ?"),
                    //NETWORK
                    "SHOW_ADMINS"      => array("h6"=>"Administrators",
                                                "lbl"=>"Who has the right to see the list of administrators of this page ?"),

                    "SHOW_GROUP"       => array("h6"=>"Your groups of members",
                                                "lbl"=>"Who has the right to see groups of members ?"),

                    "SHOW_FRIENDS"     => array("h6"=>"Your members",
                                                "lbl"=>"Who has the right to see the list of members ?"),

                    "SHOW_FOLLOWERS"   => array("h6"=>"Your followers",
                                                "lbl"=>"Who has the right to display the list of your subscribers ?"),

                    "SHOW_PAGECREATED" => array("h6"=>"Pages created from this page",
                                                "lbl"=>"Who has the right to view the list of created pages ?"),

                    /*"SHOW_ADDRESS"     => array("h6"=>"Votre addresse",
                                                "lbl"=>"Qui a le droit de connaître votre addresse ?"),

                    "SHOW_GEOPOS"      => array("h6"=>"Position géographique",
                                                "lbl"=>"Qui a le droit de connaître votre position géographique, 
                                                        et de visualiser votre icône sur la carte ?"),*/

                        
                    ),
                "informations" => array( 
                    //INFORMATIONS
                    "SHOW_INFO_EMAIL" => array("h6"=>"Your e-mail",
                                                "lbl"=>"Who has the right to see your e-mail ?"),
                    
                    "SHOW_INFO_WEBSITE" => array("h6"=>"Your website",
                                                "lbl"=>"Who has the right to see your website ?"),
                    
                    "SHOW_INFO_COMMUNECTER" => array("h6"=>"Communecter",
                                                "lbl"=>"Who has the right to see your Communecter ?"),
                    
                    "SHOW_INFO_FACEBOOK" => array("h6"=>"Facebook",
                                                "lbl"=>"Who has the right to see your Facebook ?"),
                    
                    "SHOW_INFO_TWITTER" => array("h6"=>"Twitter",
                                                "lbl"=>"Who has the right to see your Twitter ?"),
                    
                    "SHOW_INFO_YOUTUBE" => array("h6"=>"Youtube",
                                                "lbl"=>"Who has the right to see your Youtube ?"),
                    
                    "SHOW_INFO_DISCORD" => array("h6"=>"Discord",
                                                "lbl"=>"Who has the right to see your Discord ?")
                ),
                "geo" => array( 
                    "SHOW_ADDRESS"     => array("h6"=>"Your address",
                                                "lbl"=>"Who has the right to know your address ?"),

                    "SHOW_GEOPOS"      => array("h6"=>"Geographical position",
                                                "lbl"=>"Who has the right to know your geographical position, and view your icon on the map ?"))
                
            );
    }

    public static function getCountryList(){
        return Country::getCountryList();
      }

    public static function getMaxReport(){
        return Report::getNbMaxReport();
      }


}
