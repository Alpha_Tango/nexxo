<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use App\Sockets\Chat;
use Ratchet\Server\IoConnection;
use React\EventLoop\Factory;
use React\Socket\Connector;

/**
 * @MongoDB\Document(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    const VERB_COMMENT  = "COMMENT";
    const VERB_LIKE     = "LIKE";
    const VERB_DISLIKE  = "DISLIKE";
    const VERB_SHARE    = "SHARE";
    const VERB_POST_NEWS = "POST_NEWS";
    const VERB_CREATE_PAGE = "CREATE_PAGE";

    const VERB_REQUEST_FRIEND = "REQUEST_FRIEND";
    const VERB_REQUEST_FRIEND_ACCEPTED = "REQUEST_FRIEND_ACCEPTED";

    const VERB_REQUEST_ADMIN = "REQUEST_ADMIN";
    const VERB_REQUEST_ADMIN_ACCEPTED = "REQUEST_ADMIN_ACCEPTED";

    const VERB_BECOME_FOLLOWER = "BECOME_FOLLOWER";
    const VERB_BECOME_PARTICIPANT = "BECOME_PARTICIPANT";

    const VERB_EDIT = "EDIT";
    const VERB_DELETE = "DELETE";

    const VERB_REPORT_VOTE_OPEN = "REPORT_VOTE_OPEN";
    const VERB_REPORT_DISABLED = "REPORT_DISABLED";
    const VERB_REPORT_CLEARED_BYADMIN = "REPORT_CLEARED_BYADMIN";
    const VERB_REPORT_CLEARED_BYCO = "REPORT_CLEARED_BYCO";

	/**
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

    /** 
     * Reference the Page wich own (publicate) the News
     * @MongoDB\ReferenceOne(targetDocument="Page") 
     */
    private $lastAuthor;
    /** 
     * Reference the Page wich own (publicate) the News
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $authors;

    /** 
     * Reference the Page wich own (publicate) the News
     * @MongoDB\ReferenceMany(targetDocument="Page") 
     */
    private $targets;

	/**
     * @MongoDB\Field(type="string")
     */
	private $verb;

    /**
     * @MongoDB\Field(type="string")
     */
    private $aboutId;

    /**
     * @MongoDB\Field(type="string")
     */
    private $aboutType;

    /**
     * @MongoDB\Field(type="string")
     */
    private $whatId;

    /**
     * @MongoDB\Field(type="string")
     */
    private $whatType;

    /**
     * Date initialized when the Notification is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $created;
    /**
     * Date update when target or author are added
     * @MongoDB\Field(type="date")
     */
    private $updated;


    /** @MongoDB\NotSaved */
    private $aboutObj;
    /** @MongoDB\NotSaved */
    private $whatObj;


	public function __construct($verb, $aboutId, $aboutType, $whatId, $whatType){
		$this->verb = $verb;
        $this->aboutId = $aboutId;
        $this->aboutType = $aboutType;
        $this->whatId = $whatId;
        $this->whatType = $whatType;
		
		$this->authors = array();
        $this->targets = array();

        $this->created = new \Datetime();
	}

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getLastAuthor()
    {
        return $this->lastAuthor;
    }

    /**
     * @param mixed $authors
     * @return self
     */
    public function setLastAuthor($author)
    {
        $this->lastAuthor = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @param mixed $authors
     * @return self
     */
    public function addAuthor($author)
    {
        if(is_array($this->authors) && empty($this->authors)){
            $this->authors[] = $author;
        }elseif(is_array($this->authors) && !in_array($author, $this->authors)){
            $this->authors[] = $author;
        }elseif(!is_array($this->authors) && !$this->authors->contains($author)){
            $this->authors[] = $author;
        }
        
        $this->updated = new \Datetime();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTargets()
    {
        return $this->targets;
    }
    /**
     * @return mixed
     */
    public function clearTargets()
    {
        return $this->targets = array();
    }

    /**
     * @param mixed $targets
     * @return self
     */
    public function addTarget($target)
    {
        if(is_array($this->targets) && empty($this->targets)){
    		$this->targets[] = $target;
    	}elseif(is_array($this->targets) && !in_array($target, $this->targets)){
            $this->targets[] = $target;
        }elseif(!is_array($this->targets) && !$this->targets->contains($target)){
            $this->targets[] = $target;
        }

        $this->updated = new \Datetime();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * @param mixed $verb
     * @return self
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAboutId()
    {
        return $this->aboutId;
    }

    /**
     * @param mixed $aboutId
     * @return self
     */
    public function setAboutId($aboutId)
    {
        $this->aboutId = $aboutId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAboutType()
    {
        return $this->aboutType;
    }

    /**
     * @param mixed $aboutType
     * @return self
     */
    public function setAboutType($aboutType)
    {
        $this->aboutType = $aboutType;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getWhatId()
    {
        return $this->whatId;
    }

    /**
     * @param mixed $aboutId
     * @return self
     */
    public function setWhatId($whatId)
    {
        $this->whatId = $whatId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWhatType()
    {
        return $this->whatType;
    }

    /**
     * @param mixed $whatType
     * @return self
     */
    public function setWhatType($whatType)
    {
        $this->whatType = $whatType;
        return $this;
    }


    /**
     * Get created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }


    /**
     * Get updated
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * Get aboutObj
     */
    public function getAboutObj()
    {
        return $this->aboutObj;
    }

    /**
     * Set aboutObj
     */
    public function setAboutObj($aboutObj)
    {
        $this->aboutObj = $aboutObj;
        return $this;
    }

    /**
     * Get whatObj
     */
    public function getWhatObj()
    {
        return $this->whatObj;
    }

    /**
     * Set whatObj
     */
    public function setWhatObj($whatObj)
    {
        $this->whatObj = $whatObj;
        return $this;
    }



    /* ************************************************************** */


    public static function sendNotification($author, $verb, $aboutId, $aboutType, $whatId, $whatType, $db, $x){

        $notifRepo = $db->getRepository(Notification::class);

        /*if($whatId == null){*/
            $notif = $notifRepo->findOneBy(array("aboutId" => $aboutId,
                                                 "aboutType" => $aboutType,
                                                 "verb" => $verb));
        /*}else{
            $notif = $notifRepo->findOneBy(array("aboutId" => $aboutId,
                                                 "aboutType" => $aboutType,
                                                 "whatId" => $whatId,
                                                 "whatType" => $whatType,
                                                 "verb" => $verb));
        }*/

        $notifExists = $notif == null ? false : true;
        //dd($notifExists);
        //if no notif found : create new notif
        if($notif == null){
            $notif = new Notification($verb, $aboutId, $aboutType, $whatId, $whatType);
            //dd($notif);
        }else{
            $notif->setWhatType($whatType)->setWhatId($whatId);
        }

        $notif->addAuthor($author);
        $notif->setLastAuthor($author);
        //$db->flush();

        $class = Notification::getClass($aboutType);

        $aboutObj = null;
        if($aboutType != "comment"){
            $repo = $db->getRepository($class);
            $aboutObj = $repo->findOneBy(array("id" => $aboutId));
        }else{
            $csRepo = $db->getRepository(CommentStream::class);
            $cs = $csRepo->findOneBy(array('parentId' => $whatId,
                                           'parentType' => $whatType) 
                                          );
            
            $aboutObj = $cs->getCommentById($aboutId);
        }

        if($verb == Notification::VERB_COMMENT){ //aboutObj is News
            //send a notification to author of the aboutObj
            $authorNews = $aboutObj->getAuthor();

            if($author->getId() != $authorNews->getId()) //do not notifiy the sender of the notif
                $notif->addTarget($authorNews);

            //send notification to every author of comment on this news
            $allComments = $aboutObj->getCommentStream();
            foreach ($allComments->getComments() as $key => $com) {
                if($author->getId() != $com->getAuthor()->getId()) //do not notifiy the sender of the notif
                    $notif->addTarget($com->getAuthor());
            }
            
            //send notification to every liker of this aboutObj
            foreach ($aboutObj->getLikes() as $key => $liker) {
                if($author->getId() != $liker->getId()) //do not notifiy the sender of the notif
                    $notif->addTarget($liker);
            }
        }
    
        if($verb == Notification::VERB_LIKE || $verb == Notification::VERB_DISLIKE ){ //aboutObj is News
            if($aboutType == "news" && $whatType != "comment"){
                //send a notification to author of the news
                $authorNews = $aboutObj->getAuthor();

                if($author->getId() != $authorNews->getId()) //do not notifiy the sender of the notif
                    $notif->addTarget($authorNews);

                //send notification to every liker of this news
                if(!empty($aboutObj->getLikes()))
                foreach ($aboutObj->getLikes() as $key => $user) {
                    if($author->getId() != $user->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($user);
                }
                //send notification to every disliker of this news
                if(!empty($aboutObj->getDislikes()))
                foreach ($aboutObj->getDislikes() as $key => $user) {
                    if($author->getId() != $user->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($user);
                }
            }
            
            if($whatType == "comment"){

                $whatObj = $aboutObj->getCommentStream()->getCommentById($whatId);

                //send a notification to author of the comment
                $authorComment = $whatObj->getAuthor();
                //dump($author->getName());
                if($author->getId() != $authorComment->getId()) //do not notifiy the sender of the notif
                    $notif->addTarget($authorComment);
                //dump($authorComment->getName());
                //send notification to every author of comment on this news
                $allComments = $whatObj->getAnswers();
                if(!empty($allComments))
                foreach ($allComments as $key => $com) {
                    if($author->getId() != $com->getAuthor()->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($com->getAuthor());
                }
                
                //$whatObj = $aboutObj->getCommentStream()->getCommentById($whatId);

                //send notification to every liker of this comment
                //if(!empty($whatObj->getLikes()) && $verb == Notification::VERB_LIKE)
                foreach ($whatObj->getLikes() as $key => $user) { 
                    //if($author->getId() != $user->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($user);
                }
                //send notification to every disliker of this comment
                //if(!empty($whatObj->getDislikes()) && $verb == Notification::VERB_DISLIKE)
                foreach ($whatObj->getDislikes() as $key => $user) {
                    //if($author->getId() != $user->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($user);
                }
            }

            if($aboutType == "page"){
                //send notification to every admins of this page
                if(!empty($aboutObj->getRelations()->getAdmins()))
                foreach ($aboutObj->getRelations()->getAdmins() as $key => $admin) {
                    if($author->getId() != $admin->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($admin);
                        //dump($admin->getSlug());
                }
            }
            
        }

        if($verb == Notification::VERB_SHARE){ //aboutObj is News
            if($aboutType == "news" /*|| $aboutType == "comment"*/){
                //send a notification to author of the news
                $authorNews = $aboutObj->getNewsShared()->getAuthor();
                //dd($aboutType);
                if($author->getId() != $authorNews->getId()) //do not notifiy the sender of the notif
                    $notif->addTarget($authorNews);

                 //send notification to every liker of this news
                if(!empty($aboutObj->getLikes()))
                foreach ($aboutObj->getLikes() as $key => $user) {
                    if($author->getId() != $user->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($user);
                }      
            }
        }

        if($verb == Notification::VERB_CREATE_PAGE){ //aboutObj is Page
            //send a notif to all my friend, when I create a new page
            foreach ($author->getRelations()->getRealFriends() as $key => $user) {
                $notif->addTarget($user);
            }
        }

        if($verb == Notification::VERB_POST_NEWS){ //aboutObj is News

            if($aboutObj->getScopeType() == "friends"){
                if(!empty($aboutObj->getTarget()->getRelations()->getRealFriends()))
                foreach ($aboutObj->getTarget()->getRelations()->getRealFriends() as $key => $user) {
                    if($author->getId() != $user->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($user);
                }
                //notifiy the target if it is a user
                if($author->getId() != $aboutObj->getTarget()->getId() && $aboutObj->getTarget()->getType() == Page::TYPE_USER)  
                    $notif->addTarget($aboutObj->getTarget());  
            }   
            if($aboutObj->getScopeType() == "followers"){
                //add members
                if(!empty($aboutObj->getTarget()->getRelations()->getRealFriends()))
                foreach ($aboutObj->getTarget()->getRelations()->getRealFriends() as $key => $user) {
                    if($author->getId() != $user->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($user);
                }
                //add followers
                if(!empty($aboutObj->getTarget()->getRelations()->getFollowers()))
                foreach ($aboutObj->getTarget()->getRelations()->getFollowers() as $key => $user) {
                    if($author->getId() != $user->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($user);
                }
                //notifiy the target if it is a user
                if($author->getId() != $aboutObj->getTarget()->getId() && $aboutObj->getTarget()->getType() == Page::TYPE_USER)
                    $notif->addTarget($aboutObj->getTarget());     
            }
            if($aboutObj->getScopeType() == "localised"){
                if($aboutObj->getRadiusKm() <= 10){
                    $targets = Notification::getTargetGeo($aboutObj->getScopeGeo(), $db);   
                    foreach ($targets as $key => $user) {
                        if($author->getId() != $user->getId()) //do not notifiy the sender of the notif
                            $notif->addTarget($user);
                    }
                }else{
                    $notifExists = false; //n'enregistre pas la notif si le radius est suppérieur à 10 km
                }
            }
        }

        if($verb == Notification::VERB_REQUEST_FRIEND ||
           $verb == Notification::VERB_REQUEST_ADMIN ||
           $verb == Notification::VERB_BECOME_FOLLOWER ||
           $verb == Notification::VERB_BECOME_PARTICIPANT){ //aboutObj is News
            if(!empty($aboutObj->getRelations()->getAdmins()))
                foreach ($aboutObj->getRelations()->getAdmins() as $key => $admin) {
                    if($author->getId() != $admin->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($admin);
                        //dump($admin->getSlug());
                }
        }

        if($verb == Notification::VERB_BECOME_FOLLOWER ||
            $verb == Notification::VERB_BECOME_PARTICIPANT){ 
            if(!empty($aboutObj->getRelations()->getRealFriends())) 
                foreach ($aboutObj->getRelations()->getRealFriends() as $key => $friend) {//Notifiy all friends/members
                    if($author->getId() != $friend->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($friend);
                        //dump($admin->getSlug());
                }
        }
        if($verb == Notification::VERB_REQUEST_ADMIN_ACCEPTED ||
            $verb == Notification::VERB_REQUEST_FRIEND_ACCEPTED){ 
            $notif->clearTargets();
            if(!empty($aboutObj->getRelations()->getAdmins())) 
                foreach ($aboutObj->getRelations()->getAdmins() as $key => $friend) { //Notifiy all admins
                    if($author->getId() != $friend->getId()) //do not notifiy the sender of the notif
                        $notif->addTarget($friend);
                        //dump($admin->getSlug());
                }
            $repo = $db->getRepository(Page::class);        
            $whatObj = $repo->findOneBy(array('id' => $whatId));
            $notif->addTarget($whatObj);            
            $notif->setWhatObj($whatObj);
        }

        if($verb == Notification::VERB_REPORT_VOTE_OPEN ||
           $verb == Notification::VERB_REPORT_DISABLED ||
           $verb == Notification::VERB_REPORT_CLEARED_BYCO ||
           $verb == Notification::VERB_REPORT_CLEARED_BYADMIN){ 
            
            if($aboutType == "news" && $whatType == null){
                foreach ($aboutObj->getSigned()->getRelations()->getAdmins() as $key => $admin) { 
                    //Notifiy all admins of the signer page
                    $notif->addTarget($admin);
                }
            }
            //dump($aboutType);
            if($aboutType == "news" && $whatType == "comment"){
                $whatObj = $aboutObj->getCommentStream()->getCommentById($whatId);
                //dd($whatObj);
                $notif->addTarget($whatObj->getAuthor());
                $notif->setWhatObj($whatObj);
            }
            
            if($aboutType == "page"){
                foreach ($aboutObj->getRelations()->getAdmins() as $key => $admin) { //Notifiy all admins of the page
                    $notif->addTarget($admin);
                }
                foreach ($aboutObj->getRelations()->getRealFriends() as $key => $friend) { //Notifiy all friends/members of the page
                    $notif->addTarget($friend);
                }
                foreach ($aboutObj->getRelations()->getFollowers() as $key => $follower) { //Notifiy all friends/members of the page
                    $notif->addTarget($follower);
                }
            }
        }

        if(!$notifExists)
            $db->persist($notif);
        
        $db->flush();

        if($aboutObj != null)
            $notif->setAboutObj($aboutObj);

        return $notif; //$notif->getTargets()->toArray();

    }

    public static function getTargetGeo($scopeGeo, $db){
        $repo = $db->getRepository(Page::class);        
        $res = $repo->findBy(array('type' => Page::TYPE_USER,
                                   'coordinates' => 
                                    array('$geoWithin' => 
                                        array('$geometry'=> array("type"=>$scopeGeo->getType(),
                                                                  "coordinates"=>$scopeGeo->getCoordinates())
                                        ),
                                    ))
                            );
        return $res;
    }

    public static function getClass($type){
        if($type == "news")     return News::class;
        if($type == "comment")  return Comment::class;
        if($type == "page")     return Page::class;
        if($type == "report")     return Report::class;

        return ""; 
    }


    public function getJson($userPage){
        // $lastAuthor = array();
        // foreach ($this->getAuthors()->toArray() as $key => $auth) {
        //     $lastAuthor = $auth->getJson();
        // }

        return array("id" => $this->id,
                     "lastAuthor" => $this->getLastAuthor()->getJson($userPage),
                     "nbAuthor" => sizeof($this->authors),
                     "verb" => $this->verb,
                     "aboutId" =>$this->aboutId,
                     "aboutType" => $this->aboutType,
                     "whatId" =>$this->whatId,
                     "whatType" => $this->whatType);
    }
}