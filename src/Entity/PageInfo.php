<?php 
namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use App\Entity\User;
use App\Entity\Page;

/** @MongoDB\EmbeddedDocument */
class PageInfo
{

    /**
     * @MongoDB\Field(type="string")
     */
	private $email;

    /**
     * @MongoDB\Field(type="string")
     */
	private $website;

    /**
     * @MongoDB\Field(type="string")
     */
    private $communecter;

    /**
     * @MongoDB\Field(type="string")
     */
    private $facebook;

    /**
     * @MongoDB\Field(type="string")
     */
	private $twitter;

    /**
     * @MongoDB\Field(type="string")
     */
	private $youtube;

    /**
     * @MongoDB\Field(type="string")
     */
	private $discord;


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommunecter()
    {
        return $this->communecter;
    }

    /**
     * @param mixed $communecter
     *
     * @return self
     */
    public function setCommunecter($communecter)
    {
        $this->communecter = $communecter;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param mixed $website
     *
     * @return self
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $facebook
     *
     * @return self
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @param mixed $twitter
     *
     * @return self
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * @param mixed $youtube
     *
     * @return self
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscord()
    {
        return $this->discord;
    }

    /**
     * @param mixed $discord
     *
     * @return self
     */
    public function setDiscord($discord)
    {
        $this->discord = $discord;

        return $this;
    }
}