<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @MongoDB\Document(repositoryClass="App\Repository\MediaRepository")
 */
class Media
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    private $id;
    /** @MongoDB\Field(type="string") */
    private $title;

    /** @MongoDB\Field(type="string") */
    private $description;

    /** @MongoDB\Field(type="string") */
    private $image;

    /** @MongoDB\Field(type="string") */
    private $url;

    /** @MongoDB\Field(type="string") */
    private $host;

    /** @MongoDB\Field(type="boolean") */
    private $preview;

    public function __construct($media){ //dd($media);
        $this->title = @$media["title"];
        $this->description = @$media["description"];
        $this->image = @$media["image"];
        $this->url = @$media["url"];
        $this->preview = @$media["preview"];
        //dump($media);
        //dump($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = strip_tags($title);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = strip_tags($description);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = strip_tags($image);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = strip_tags($url);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     * @return self
     */
    public function setHost($host)
    {
        $this->host = strip_tags($host);
        return $this;
    }


    /*
     * Get preview
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /*
     * Set preview
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;
        return $this;
    }

    //try to identify if a link is a tube (video)
    public function isTubeUrl(){ 
        if($this->isYoutubeUrl()) return "youtube";
        if($this->isPeertubeUrl()) return "peertube";
        if($this->isVimeoUrl()) return "vimeo";
        
        return false;
    }

    public function isYoutubeUrl(){
        $pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
        if(preg_match($pattern, $this->url)){
            return "youtube";
        }else{ return false; }
    }

    public function isPeertubeUrl(){
        $patterns = array('https://peertube.', 'https://hostyour.tv', 'https://peervideo.net', 'https://video.ploud.fr');
        foreach ($patterns as $key => $pattern) {
            $found = strpos($this->url, $pattern); //dump($found);
            if($found === 0) return "peertube";
        }
        return false;
    }
    public function isVimeoUrl(){
        $pattern = "https://vimeo.com";
        $found = strpos($this->url, $pattern); //dump($found);
        if($found === 0) return "vimeo";
        else return false;
    }

    public function getTubeIframeUrl($origine){
        if($origine == "youtube"){
            //YOUTUBE
            $pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
            preg_match($pattern, $this->url, $matches);
            if(isset($matches[1]))
                return (isset($matches[1])) ? "https://www.youtube.com/embed/".$matches[1] : false;
        }

        if($origine == "peertube"){
            //PEERTUBE https://peertube.mastodon.host/videos/watch/6915bc88-78db-4abb-bb24-16b4b509be0a
            $patterns = array('https://peertube.', 'https://hostyour.tv', 'https://peervideo.net', 'https://video.ploud.fr');
            foreach ($patterns as $key => $pattern) {
                $found = strpos($this->url, $pattern);
                if($found > -1){
                    $pattern2 = "/videos/watch/";
                    $hostPost = strpos($this->url, $pattern2); //echo $this->url." [".(string)$hostPost."] ";
                    $domain = substr($this->url, 0, $hostPost);
                    $id = substr($this->url, $hostPost + strlen($pattern2), strlen($this->url));
                    return $domain."/videos/embed/".$id;

                }
            }
            return false;
        }

        if($origine == "vimeo"){
            //YOUTUBE
            $pattern = 'https://vimeo.com/';
            $found = strpos($this->url, $pattern);
            if($found > -1){
                //$pattern2 = "/video/";
                //$hostPost = strpos($this->url, $pattern2); //echo $this->url." [".(string)$hostPost."] ";
                $domain = "https://player.vimeo.com/video/";
                $id = substr($this->url, strlen($pattern), strlen($this->url));
                return $domain.$id;

            }
        }

        
    }

    
    /**
     * return the list of all channels
     */
    public static function getChannels()
    {
        return array(   "https://www.youtube.com/user/elfuegoo/videos",                    //partager c sympa
                        "https://www.youtube.com/channel/UCGl2QLR344ry4Y20RV9dM3g/videos", //professeur feuillage
                        "https://www.youtube.com/user/NicolasMeyrieux/videos",             //la barbe
                        "https://www.youtube.com/channel/UCVeMw72tepFl1Zt5fvf9QKQ/videos", //osons causer
                        "https://www.youtube.com/channel/UC9hHeywcPBnLglqnQRaNShQ/videos", //fil d'actu
                        "https://www.youtube.com/user/thinkerview/videos",                 //thinkerview
                        "https://www.youtube.com/channel/UC0i7t1CC7T0xheeBahaWZYQ/videos", //next
                        "https://www.youtube.com/channel/UCnIGrvSrQF7uCovnIm8LCSQ/videos", //DemosKratos
                        "https://www.youtube.com/channel/UCLHlsGMNqgfZbVFjRoy8_iA/videos", //Le poste
                        "https://www.youtube.com/channel/UC-2EkisRV8h9KsHpslQ1gXA/videos", //Et tout le monde s'en fou
                        "https://www.youtube.com/channel/UC9NB2nXjNtRabu3YLPB16Hg/videos", //jsuis pas content
                        "https://www.youtube.com/channel/UCESTwDXpoMgiYBHipMdKTkQ/videos", //Sud Radio
                        "https://www.youtube.com/channel/UCT67YOMntJxfRnO_9bXDpvw/videos", //le media
                        "https://www.youtube.com/channel/UCl8QnSIy57iOpMmgW4RtwDA/videos", //culture-RIC
                        "https://www.youtube.com/channel/UCUh1sBArXQGCzdJi2qyhAJQ/videos", //ass des ass
                        "https://www.youtube.com/user/LaBajon/videos",                     //la bajon
                        "https://www.youtube.com/channel/UCqt99sKYNTxqlHtzV9weUYA/videos", //VU FranceTV
                        "https://www.youtube.com/user/vivresansargent/videos",             //ma ferme autonome

                        "https://www.youtube.com/channel/UCSFY15spR_TTeqD8EWVnXgg/videos",
                        "https://www.youtube.com/channel/UCtg3B1mxTUXSO30nT9azYGw/videos",
                        "https://www.youtube.com/user/chriscreateharmony1/videos",
                        "https://www.youtube.com/user/ernestodeupoinzero/videos",
                        "https://www.youtube.com/channel/UC9U_1ONuDEj0ZfLXYFaTpVA/videos",
                        "https://www.youtube.com/channel/UCMaO2u7ZMuyPVzj5h_CDl3A/videos",
                        "https://www.youtube.com/channel/UCM1dPrcar1pIz0EwfXDHzBQ/videos",
                        "https://www.youtube.com/channel/UCdwvgwMAV66OVckB07psJBg/videos",
                        "https://www.youtube.com/channel/UC1JEQiM754n4EYg2b_63zpw/videos",
                        "https://www.youtube.com/channel/UCIJffkItaprUTFmDNOMXYxA/videos",
                        "https://www.youtube.com/user/sambo29000/videos",
                        "https://www.youtube.com/channel/UC5COTnDgJep08Blz-IJqk_Q/videos",
                        "https://www.youtube.com/channel/UC3RlAzNzgy_7QiVPukFiDZQ/videos",
                        "https://www.youtube.com/channel/UCJgY8TLEJ7kDK9NlTlwgwVg/videos",
                        "https://www.youtube.com/channel/UCRAdymUfW2fDk3WNz6Hhf1w/videos",
                        "https://www.youtube.com/channel/UC5s7HmoOXXIc1Z-Rlz4FAcA/videos",
                        "https://www.youtube.com/channel/UCy02TAtkAyU0y-bkSiMhidw/videos",
                        "https://www.youtube.com/channel/UCeXsZz8C5sUZe1YgDxR6j9A/videos",
                        "https://www.youtube.com/channel/UCXzgD_oZ2jVOnEWNVDCnjjA/videos",
                        "https://www.youtube.com/channel/UCynFUJ4zUVuh3GX7bABTjGQ/videos",
                        "https://www.youtube.com/channel/UC6-BWVphnrCj5xNL73qOd0w/videos",
                        "https://www.youtube.com/channel/UC1IkSPDrUVO9_UIxpA8arRQ/videos",
                        "https://www.youtube.com/channel/UCpwWIg7cSrFja9MQgtdwBkw/videos",
                        "https://www.youtube.com/channel/UC5s7HmoOXXIc1Z-Rlz4FAcA/videos",
                        "https://www.youtube.com/channel/UC3Mj5hKC0Nbqv3YXHLKxdyw/videos",
                        "https://www.youtube.com/user/vpouliquen/videos",
                        "https://www.youtube.com/channel/UCBIyR71Yvq7rWHSF2p-uH4Q/videos",
                        "https://www.youtube.com/channel/UCUToCUpK5ZAH6S6ZXktfWEA/videos",
                        "https://www.youtube.com/user/rtenfrancais/videos",
                        "https://www.youtube.com/user/UPRdiffusion/videos"
                        /**/
                    );
    }

}