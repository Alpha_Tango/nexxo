<?php
// src/Entity/User.php
namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

//use Captcha\Bundle\CaptchaBundle\Validator\Constraints as CaptchaAssert;

/**
 * User class contain only minimal informations, needed for login.<br>
 * All other informations about Users are stocked in Page class 
 *
 * @MongoDB\Document(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{

	const ROLE_DEFAULT = 'ROLE_USER';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

	/**
	 * Auto generated mongoID
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

	/**
     * @MongoDB\Field(type="string")
     */
	private $username;

	/**
     * @MongoDB\Field(type="string")
     */
	private $password;

	/**
     * @MongoDB\Field(type="string")
     */
	private $email;

	/**
	 * By default : false<br>
	 * isActive = true after email validation
     * @MongoDB\Field(type="boolean")
     */
	private $isActive;

	
	/**
     * @MongoDB\Field(type="collection")
     */
	private $roles;

    
	/** @MongoDB\NotSaved */
    private $plainPassword;

	/**
     * @MongoDB\Field(type="date")
     */
    private $passwordRequestedAt;

    /**
     * @MongoDB\Field(type="string")
     */
    private $token;


	/**
	 * Date initialized when user register
	 * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $created;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Page", mappedBy="owner")
     */
    private $pages;

    /**
	 * ImageProfil are not register in User data.<br>
	 * This attribute allow us to retrieve ImageProfil from the Page linked to the user<br>
	 * Only to display ImageProfil in TopMenu from app.user
    */
    private $imageProfil;

	/**
	 * By default : false<br>
	 * isActive = true after email validation
     * @MongoDB\Field(type="boolean")
     */
	private $isSuperAdmin;



	public function __construct()
	{
		$this->isActive = true;
		$this->roles = ['ROLE_USER'];
		$this->token = bin2hex(random_bytes(20)); // 20 chars //$this->chatToken;
		$this->created = new \Datetime();
	}
    
	/*
	 * Get id
	 */
	public function getId()
	{
		return $this->id;
	}

	public function getRealUsername()
	{
		return $this->username;
	}
	public function getUsername()
	{
		return $this->email;
	}

	public function setUsername($username)
	{
		$this->username = $username;
		return $this;
	}


	public function getPassword()
	{
		return $this->password;
	}

	public function setPassword($password)
	{
		$this->password = $password;
		return $this;
	}

	/*
	 * Get email
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/*
	 * Set email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}

	/*
	 * Get isActive
	 */
	public function getIsActive()
	{
		return $this->isActive;
	}

	/*
	 * Set isActive
	 */
	public function setIsActive($isActive)
	{
		$this->isActive = $isActive;
		return $this;
	}

	public function getSalt()
	{
		// pas besoin de salt puisque nous allons utiliser bcrypt
		// attention si vous utilisez une méthode d'encodage différente !
		// il faudra décommenter les lignes concernant le salt, créer la propriété correspondante, et renvoyer sa valeur dans cette méthode
		return null;
	}

	// modifier la méthode getRoles
	public function getRoles()
	{
		return $this->roles; 
	}

	public function setRoles(array $roles)
	{
		if (!in_array('ROLE_USER', $roles))
		{
			$roles[] = 'ROLE_USER';
		}
		foreach ($roles as $role)
		{
			if(substr($role, 0, 5) !== 'ROLE_') {
				throw new InvalidArgumentException("Chaque rôle doit commencer par 'ROLE_'");
			}
		}
		$this->roles = $roles;
		return $this;
	} 

	public function eraseCredentials()
	{
	}

	/** @see \Serializable::serialize() */
	public function serialize()
	{
		return serialize(array(
			$this->id,
			$this->username,
			$this->password,
			$this->isActive,
			// voir remarques sur salt plus haut
			// $this->salt,
		));
	}

	/** @see \Serializable::unserialize() */
	public function unserialize($serialized)
	{
		list (
			$this->id,
			$this->username,
			$this->password,
			$this->isActive,
			// voir remarques sur salt plus haut
			// $this->salt
		) = unserialize($serialized);
	}

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
 
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

     /*
     * Get created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /*
     * Set created
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

     /*
     * Get passwordRequestedAt
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    /*
     * Set passwordRequestedAt
     */
    public function setPasswordRequestedAt($passwordRequestedAt)
    {
        $this->passwordRequestedAt = $passwordRequestedAt;
        return $this;
    }

    /*
     * Get token
     */
    public function getToken()
    {
        return $this->token;
    }

    /*
     * Set token
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }


    /*
     * add page
     */
    public function addPage(Page $page)
    {
        $this->pages[] = $page;
    }

    /*
     * get page
     */
    public function getPages()
    {
    	return $this->pages;
    }

    /*
     * get the page related to this user
     */
    public function getMyUserPage()
    {
    	foreach ($this->pages as $key => $page) {
    		if($page->getType() == "user") return $page;
    	}
    }
    /*
     * get the position (coordinates) of the page related to this user
     */
    public function getMyPosition()
    {
    	$page = $this->getMyUserPage();
    	return @$page->getCoordinates() ? $page->getCoordinates() : false;
    }

    public function getImageProfil(){
    	$myPage = $this->getMyUserPage();
    	return $myPage->getImageProfil();
    }

    /*
     * Get isSuperAdmin
     */
    public function isSuperAdmin()
    {
        return isset($this->isSuperAdmin) ? $this->isSuperAdmin : false;
    }

    /*
     * Set isSuperAdmin
     */
    public function setIsSuperAdmin($isSuperAdmin)
    {
        $this->isSuperAdmin = $isSuperAdmin;
        return $this;
    }
    
 //    public function getCaptchaCode()
	// {
	//     return $this->captchaCode;
	// }

	// public function setCaptchaCode($captchaCode)
	// {
	//     $this->captchaCode = $captchaCode;
	// }
	
    /*
     * Anonymifaction instead of remove
     */
    public function anonymify()
    {
        $this->username="";
		$this->password="";
		$this->email="";
		$this->isActive=false;
		$this->plainPassword="";
		$this->token="";
		$this->isSuperAdmin=false;

        return $this;
    }


}
