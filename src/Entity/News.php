<?php

namespace App\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


use App\Entity\Group;
use App\Entity\CommentStream;
use App\Services\Helper;
use App\Entity\Report;


/**
 * @MongoDB\Document(repositoryClass="App\Repository\NewsRepository")
 */
class News
{

	/**
     * @MongoDB\Id(strategy="auto")
     */
	private $id;

    /**
     * Main text (message)
     * @MongoDB\Field(type="string")
     */
    private $text;

    /**
     * Image
     * @MongoDB\Field(type="string")
     *
     */
    private $image;

    /**
     * List of tag detected in the text (not in form / auto-generated before save)
     * @MongoDB\Field(type="collection")
     */
    private $tags;

	
    /** 
     * Reference the Page wich own (publicate) the News
     * @MongoDB\ReferenceOne(targetDocument="Page", inversedBy="news") 
     */
    private $author;


    /** 
     * Reference the Page wich signed the News
     * @MongoDB\ReferenceOne(targetDocument="Page", inversedBy="newsSigned") 
     */
    private $signed;


    /** 
     * Reference the Page wich will receive the News in his stream (wall)
     * @MongoDB\ReferenceOne(targetDocument="Page", inversedBy="newsTarget") 
     */
    private $target;


    /**
     * Define wich scope has been chosen by the author<br>
     * (friends, followers, groups, localised, all)
     * @MongoDB\Field(type="string")
     */
    private $scopeType;

    /**
     * Reference a Polygon area (geoJson) used to share a News with people<br>
     * based on their geographical position
     * @MongoDB\EmbedOne(targetDocument="scopeGeo") 
     */
    private $scopeGeo;

    /**
     * radius for scopeGeo
     * @MongoDB\Field(type="float")
     */
    private $radiusKm;

    /** 
     * Reference a list of Groups (receiver of the news)
     * @MongoDB\ReferenceMany(targetDocument="Group") 
     */
    private $scopeGroups;


    /**
     * isSharable = false : cant share this news
     * otherwise is false
     * @MongoDB\Field(type="boolean")
     *
     */
    private $isSharable;


    /** 
     * Reference the News shared in this news
     * @MongoDB\ReferenceOne(targetDocument="News", inversedBy="sharedBy") 
     */
    private $newsShared;


    /** 
     * Reference all News sharing this news
     * @MongoDB\ReferenceMany(targetDocument="News", inversedBy="newsShared") 
     */
    private $sharedBy;


    
    /** 
     * cf : NewsRepository : <br>
     * used in post-process, to clear multi-post sharedBy multiple pages in the same time
     * @MongoDB\NotSaved */
    private $sharedByMyRelations;


    /** 
     * Reference the Pages (user) who like the News
     * @MongoDB\ReferenceMany(targetDocument="Page", inversedBy="iLikeIt") 
     */
    private $likes;

    /** 
     * Reference the Pages (user) who dislikes the News
     * @MongoDB\ReferenceMany(targetDocument="Page", inversedBy="iDislikeIt") 
     */
    private $dislikes;


    /** 
     * Reference the commentStream linked to this News
     * @MongoDB\ReferenceOne(targetDocument="CommentStream") 
     */
    private $commentStream;


    /** 
     * Reference the Media metadata of shared links
     * @MongoDB\EmbedMany(targetDocument="Medias") 
     */
    private $medias = array();

    /**
     * Owner is the User who created the page
     * @MongoDB\ReferenceOne(targetDocument="Report") 
     */
    private $report;

    /**
     * +1 each view
     * @MongoDB\Field(type="integer")
     */
    private $viewCount;

    /**
     * When isActive == false : the News can't be found in the search page<br>
     * @MongoDB\Field(type="boolean")
     *
     */
    private $isActive;

    /**
     * When isFirstMsg == true : the News can be display unless the author's page is not active 
     * (email not validated after registration)<br>
     * @MongoDB\Field(type="boolean")
     *
     */
    private $isFirstMsg;

    /**
     * Date initialized when the News is created
     * Set one time, never modified after initialization
     * @MongoDB\Field(type="date")
     */
    private $created;


    public function __construct()
    {
        $this->isActive = true;
        $this->isFirstMsg = false;
        $this->viewCount = 0;

        /*$this->TYPES_AVAILABLES = array(Page::TYPE_USER,      Page::TYPE_ASSOCIATION, Page::TYPE_BUSINESS, 
                                        Page::TYPE_FREEGROUP, Page::TYPE_INSTITUTION, Page::TYPE_PROJECT, 
                                        Page::TYPE_EVENT);*/
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        //Helper::getEncryptKey();
        //dd(base64_encode("zrieuztyeiruetyhuirehgyuertuyigheryuteritgy"));
        return Helper::decrypt($this->text);
    }

    /**
     * transform hashtag string in text to html (<span>)
     * you can define css classes to be added in the result
     * @return mixed
     */
    public function getTextHtml($class="")
    {
        $text = Helper::decrypt($this->text);
        $text = Helper::hashtagToHtml($text, $class);
        $text = Helper::linkToHtml($text);
        return $text;
    }

    /**
     * @param mixed $name
     * @return self
     */
    public function setText($text)
    {
        $stripedText = strip_tags($text);
        $tags = Helper::extractHashtags($stripedText);
        //s'il y a des tags : on initialise la liste des tags
        //sinon on ne fait rien
        if(!empty($tags)) $this->setTags($tags);

        $this->text = Helper::encrypt($stripedText);

        return $this;
    }


    public function getImage()
    {
        if($this->image == "") return "";
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags == null ? array() : $this->tags;
    }

    /**
     * @param mixed $tags
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return self
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getSigned()
    {
        return $this->signed;
    }

    /**
     * @param mixed $signed
     * @return self
     */
    public function setSigned($signed)
    {
        $this->signed = $signed;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param mixed $target
     * @return self
     */
    public function setTarget($target)
    {
        $this->target = $target;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScopeType()
    {
        return $this->scopeType;
    }


    /**
     * @param mixed $scopeType
     * @return self
     */
    public function setScopeType($scopeType)
    {
        $this->scopeType = $scopeType;
        return $this;
    }


    /*
     * Get scopeGeo
     */
    public function getScopeGeo()
    {
        return $this->scopeGeo;
    }

    /*
     * Set scopeGeo
     */
    public function setScopeGeo($scopeGeo)
    {   
        $this->scopeGeo = new ScopeGeo($scopeGeo);
        return $this;
    }


    public function getRadiusKm(){ return $this->radiusKm; }
    public function setRadiusKm($radiusKm){ $this->radiusKm = $radiusKm; }


    /**
     * get scopeGroups
     */
    public function getScopeGroups()
    {   
        return $this->scopeGroups;
    }

    /**
     * set scopeGroups
     */
    public function setScopeGroups($scopeGroups)
    {   
        $this->scopeGroups = $scopeGroups;
        return $this;
    }

    /**
     * add scopeGroups
     */
    public function addScopeGroup($group)
    {   
        $this->scopeGroups[] = $group;
    }

    /**
     * remove group
     */
    public function removeScopeGroup(Group $group)
    {   
        $this->scopeGroups->removeElement($group);
    }


    /*
     * Get isSharable
     */
    public function getIsSharable()
    {
        return $this->isSharable;
    }

    /*
     * Set isSharable
     */
    public function setIsSharable($isSharable)
    {
        $this->isSharable = $isSharable;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getNewsShared()
    {
        return $this->newsShared;
    }

    /**
     * @param mixed $newsShared
     * @return self
     */
    public function setNewsShared(News $newsShared)
    {
        $this->newsShared = $newsShared;
        return $this;
    }


    /**
     * get sharedBy
     */
    public function getSharedBy()
    {   
        return $this->sharedBy;
    }

    /**
     * set sharedBy
     */
    public function setSharedBy(array $sharedBy)
    {   
        $this->sharedBy = $sharedBy;
        return $this;
    }

    /**
     * add sharedBy
     */
    public function addSharedBy(News $sharedBy)
    {   
        $this->sharedBy[] = $sharedBy;
    }

    /**
     * get sharedByMyRelations
     */
    public function getSharedByMyRelations()
    {   
        return $this->sharedByMyRelations;
    }

    /**
     * set sharedByMyRelations
     */
    public function setSharedByMyRelations(array $sharedByMyRelations)
    {   
        $this->sharedByMyRelations = $sharedByMyRelations;
        return $this;
    }

    /**
     * add sharedByMyRelations
     */
    public function addSharedByMyRelations(News $sharedByMyRelations)
    {   
        $this->sharedByMyRelations[] = $sharedByMyRelations;
    }


    /**
     * get likes
     */
    public function getLikes()
    {   
        return $this->likes;
    }

    /**
     * add likes
     */
    public function addLike(Page $page)
    {
        //add follower if the Page is not already a follower
        //and if it is not the parent (you cant be follower of yourself)
        if(empty($this->likes) || !$this->likes->contains($page)){
            $this->likes[] = $page;
            $this->removeDislike($page);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove likes
     */
    public function removeLike(Page $page)
    {   
        $this->likes->removeElement($page);
    }


    /**
     * get dislikes
     */
    public function getDislikes()
    {   
        return $this->dislikes;
    }

    /**
     * add dislikes
     */
    public function addDislike(Page $page)
    {
        //add follower if the Page is not already a follower
        //and if it is not the parent (you cant be follower of yourself)
        if(empty($this->dislikes) || !$this->dislikes->contains($page)){
            $this->dislikes[] = $page;
            $this->removeLike($page);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * remove dislikes
     */
    public function removeDislike(Page $page)
    {   
        $this->dislikes->removeElement($page);
    }



    /**
     * @return mixed
     */
    public function getCommentStream()
    {
        return empty($this->commentStream) ? new CommentStream() : $this->commentStream;
    }

    /**
     * @param mixed $commentStream
     * @return self
     */
    public function setCommentStream(CommentStream $commentStream)
    {
        $this->commentStream = $commentStream;
        return $this;
    }


    /**
     * get dislikes
     */
    public function getMedias()
    {   
        return $this->medias;
    }

    /**
     * add medias
     */
    public function setMedias($medias)
    {   //dump($medias);
        if(!empty($medias))
        foreach ($medias as $key => $media) {
            //add a media if the News does not contain yet the same media
            //and if it is not the parent (you cant be follower of yourself)
            //dump($this->medias);
            //if(!in_array($media, $this->medias)){
                //dump($this->medias);
            if(is_array($media))
                $this->medias[] = new Medias($media);
            else
                $this->medias[] = $media;
                //dump($this->medias);
                
           // }
        }
        return true;
    }

    /**
     * add media
     */
    public function addMedia($media)
    { 
        //dump($media);
        //add a media if the News does not contain yet the same media
        //and if it is not the parent (you cant be follower of yourself)
        //if(empty($this->medias) || !$this->medias->contains($media)){
       // dump($media);
            $this->medias[] = new Medias($media);
            return true;
        //}
        //else{
        //    return false;
        //}
    }

    /**
     * remove medias
     */
    public function removeMedia(Medias $media)
    {   
        $this->medias->removeElement($media);
    }


    /**
     * Get report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set report
     */
    public function setReport(Report $report)
    {
        $this->report = $report;
        return $this;
    }
    
    /**
     * Remove report
     */
    public function removeReport()
    {
        $this->report = null;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }

    /**
     * @param mixed $viewCount
     *
     * @return self
     */
    public function setViewCount($viewCount)
    {
        $this->viewCount = $viewCount;
        return $this;
    }

    /**
     * @param mixed $count
     *
     * @return self
     */
    public function incViewCount()
    {
        $this->viewCount = $this->viewCount + 1;
        if($this->newsShared != null)
            $this->newsShared->incViewCount();
        return $this;
    }


    /**
     * Get isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isFirstMsg
     */
    public function getIsFirstMsg()
    {
        return $this->isFirstMsg;
    }

    /**
     * Get isFirstMsg
     */
    public function isFirstMsg()
    {
        return $this->isFirstMsg;
    }

    /**
     * Set isFirstMsg
     */
    public function setIsFirstMsg($isFirstMsg)
    {
        $this->isFirstMsg = $isFirstMsg;
        return $this;
    }


    /*
     * Get created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /*
     * Set created
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /*
     * Set get JSON
     */
    public function getJson()
    {
        $json = array("id" => $this->id,
                     "text" => $this->getTextHtml(),
                     "created" => $this->created);

        if($this->scopeType == "localised") $json["scopeGeo"] = $this->scopeGeo->getJson();
        return $json;
    }

    public function cleanLock($em){
        //supprime les notifications liées à cette news
        $notifRepo = $em->getRepository(Notification::class);
        $notifs = $notifRepo->findBy(array('aboutType'=>"news",
                                           'aboutId' => $this->getId())); 
        foreach ($notifs as $n => $notif) {
            $em->remove($notif);
            $em->flush();
        }

        //supprime les news qui partagent cette news
        foreach ($this->getSharedBy() as $n => $news) {
            $news->cleanRemove($em);
        }

        //on ne supprime pas les commentaires
        //$this->getCommentStream()->cleanRemove($em);
    }

    public function cleanRemove($em){
        //dump("News: start remove all notifications à propos de cette news");
        //supprime les notifications liées à cette news
        $notifRepo = $em->getRepository(Notification::class);
        $notifs = $notifRepo->findBy(array('aboutType'=>"news",
                                           'aboutId' => $this->getId())); 
        foreach ($notifs as $n => $notif) {
            $em->remove($notif);
            $em->flush();
        }
        //dump("News: start remove all reports about this news");
        //supprime les reports liées à cette news
        $reportRepo = $em->getRepository(Report::class);
        $reports = $reportRepo->findBy(array('aboutType'=>"news",
                                             'aboutId' => $this->getId())); 
        foreach ($reports as $n => $report) {
            $em->remove($report);
            $em->flush();
        }

        //dump("News: start remove all news qui partage cette news");
        //supprime les news qui partagent cette news
        foreach ($this->getSharedBy() as $n => $news) {
            $news->cleanRemove($em);
        }

        //remove image file if exists
        if($this->getImage() != null){
            if(file_exists("uploads/imgNews/".$this->getImage())){
                unlink("uploads/imgNews/".$this->getImage());
            }
        }

        //dump("News: start remove commentStream");
        $this->getCommentStream()->cleanRemove($em);

        $em->remove($this);
        $em->flush();
    }


}




/** 
 * ScopeGeo embed the polygon for scopeGeo<br>
 * its the good data format to execute geoQuery with mongoDB<br>
 * do not modify nor add any attributes in this class<br>
 * it would break geoQueries
 * @MongoDB\EmbeddedDocument 
 */
class ScopeGeo
{
    /** @MongoDB\Field(type="string") */
    private $type;

    /** @MongoDB\Field(type="collection") */
    private $coordinates;

    /** @MongoDB\Field(type="collection") */
    private $origin;

    public function __construct($scopeGeo)
    {
        $this->type = $scopeGeo["type"];

        $this->origin = $scopeGeo["origin"];
        
        $polygon = array(array());
        //dd($scopeGeo);
        if($scopeGeo["coordinates"] != null)
        foreach ($scopeGeo["coordinates"] as $key => $value) {
            $polygon[0][] = array(floatval($value[0]), floatval($value[1]));
        }

        $this->coordinates = $polygon;
    }

    public function getType(){
        return $this->type;
    }
    public function getCoordinates(){
        return $this->coordinates;
    }

    public function getOrigin(){
        return $this->origin != null ? $this->origin : array("false","false");
    }

    public function getJson(){
        return array("type" => $this->type,
                     "coordinates" => $this->coordinates,
                     "origin" => $this->origin,
                     );
    }

}


/** 
 * Medias embed metadata about links shared in the news
 * @MongoDB\EmbeddedDocument 
 */
class Medias
{
    /** @MongoDB\Field(type="string") */
    private $title;

    /** @MongoDB\Field(type="string") */
    private $description;

    /** @MongoDB\Field(type="string") */
    private $image;

    /** @MongoDB\Field(type="string") */
    private $url;

    /** @MongoDB\Field(type="string") */
    private $host;

    /** @MongoDB\Field(type="boolean") */
    private $preview;

    public function __construct($media){ //dd($media);
        $this->title = @$media["title"];
        $this->description = @$media["description"];
        $this->image = @$media["image"];
        $this->url = @$media["url"];
        $this->preview = @$media["preview"];
        //dump($media);
        //dump($this);
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = strip_tags($title);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = strip_tags($description);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = strip_tags($image);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = strip_tags($url);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     * @return self
     */
    public function setHost($host)
    {
        $this->host = strip_tags($host);
        return $this;
    }


    /*
     * Get preview
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /*
     * Set preview
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;
        return $this;
    }

    //try to identify if a link is a tube (video)
    public function isTubeUrl(){ 
        if($this->isYoutubeUrl()) return "youtube";
        if($this->isPeertubeUrl()) return "peertube";
        if($this->isVimeoUrl()) return "vimeo";
        
        return false;
    }

    public function isYoutubeUrl(){
        $pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
        if(preg_match($pattern, $this->url)){
            return "youtube";
        }else{ return false; }
    }

    public function isPeertubeUrl(){
        $patterns = array('https://peertube.', 'https://hostyour.tv', 'https://peervideo.net', 'https://video.ploud.fr');
        foreach ($patterns as $key => $pattern) {
            $found = strpos($this->url, $pattern); //dump($found);
            if($found === 0) return "peertube";
        }
        return false;
    }
    public function isVimeoUrl(){
        $pattern = "https://vimeo.com";
        $found = strpos($this->url, $pattern); //dump($found);
        if($found === 0) return "vimeo";
        else return false;
    }

    public function getTubeIframeUrl($origine){
        if($origine == "youtube"){
            //YOUTUBE
            $pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
            preg_match($pattern, $this->url, $matches);
            if(isset($matches[1]))
                return (isset($matches[1])) ? "https://www.youtube.com/embed/".$matches[1] : false;
        }

        if($origine == "peertube"){
            //PEERTUBE https://peertube.mastodon.host/videos/watch/6915bc88-78db-4abb-bb24-16b4b509be0a
            $patterns = array('https://peertube.', 'https://hostyour.tv', 'https://peervideo.net', 'https://video.ploud.fr');
            foreach ($patterns as $key => $pattern) {
                $found = strpos($this->url, $pattern);
                if($found > -1){
                    $pattern2 = "/videos/watch/";
                    $hostPost = strpos($this->url, $pattern2); //echo $this->url." [".(string)$hostPost."] ";
                    $domain = substr($this->url, 0, $hostPost);
                    $id = substr($this->url, $hostPost + strlen($pattern2), strlen($this->url));
                    return $domain."/videos/embed/".$id;

                }
            }
            return false;
        }

        if($origine == "vimeo"){
            //YOUTUBE
            $pattern = 'https://vimeo.com/';
            $found = strpos($this->url, $pattern);
            if($found > -1){
                //$pattern2 = "/video/";
                //$hostPost = strpos($this->url, $pattern2); //echo $this->url." [".(string)$hostPost."] ";
                $domain = "https://player.vimeo.com/video/";
                $id = substr($this->url, strlen($pattern), strlen($this->url));
                return $domain.$id;

            }
        }

        
    }

}

