<?php

namespace App\Services;
use Symfony\Component\Translation\Translator;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Helper
 */
class Helper
{
    public static function accentToRegex($text) {

        $from = str_split(utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ'));
        $to   = str_split(strtolower('SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyy'));
        $text = utf8_decode($text);
        $regex = array();

        foreach ($to as $key => $value)
        {
            if (isset($regex[$value]))
                $regex[$value] .= $from[$key];
            else 
                $regex[$value] = $value;
        }

        foreach ($regex as $rg_key => $rg)
        {
            $text = preg_replace("/[$rg]/", "_{$rg_key}_", $text);
        }

        foreach ($regex as $rg_key => $rg)
        {
            $text = preg_replace("/_{$rg_key}_/", "[$rg]", $text);
        }
        return utf8_encode($text);
    }


    public static function slugify ($text) {    
       // replace non letter or digits by -
      $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

      // trim
      $text = trim($text, '-');

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // lowercase
      $text = strtolower($text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '', $text);

      if (empty($text))
      {
        return 'n-a';
      }

      return $text;
    }

    public static function saveImageMin($base64_img, $filename){
        //$im = base64_decode($base64_string);
        //$filename = "/tmp/";
        //$filename .= uniqid("intrinsic_").".png";

        $file = fopen($filename, "wb");
        $newIamge = Helper::resize_image($base64_img, 500, 500);

        fwrite($file, $newIamge);
        fclose($file);
        return $filename;
    }

    public static function resize_image($file, $w, $h, $crop=FALSE) {
        //list($width, $height) = getimagesize($file);
        $src = imagecreatefromstring($file);
        if (!$src) return false;
        $width = imagesx($src);
        $height = imagesy($src);

        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
              $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
              $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
              $newwidth = $h*$r;
              $newheight = $h;
            } else {
              $newheight = $w/$r;
              $newwidth = $w;
            }
        }
        //$src = imagecreatefrompng($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        // Buffering
        ob_start();
        imagepng($dst);
        $data = ob_get_contents();
        ob_end_clean();
        return $data;
      }

      public static function extractHashtags($string){

        /* Match hashtags at first charater of the string : ^  */
        preg_match_all('/^#([a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/', $string, $matches);
        $keywords = array();
        if(count($matches[1]) > 0){
          foreach ($matches[1] as $match) {
            $keywords[] = "#".$match;
          }
        }
        
        /* Match hashtags with space before #  */
        preg_match_all('/ #([a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/', $string, $matches);
        if(count($matches[1]) > 0){
          foreach ($matches[1] as $match) {
            $keywords[] = "#".$match;
          }
        }

        /* Match hashtags with new line before #  */
        preg_match_all('/\n#([a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/', $string, $matches);
        if(count($matches[1]) > 0){
          foreach ($matches[1] as $match) {
            $keywords[] = "#".$match;
          }
        }
         
        return (array) $keywords;
      }

      public static function hashtagToHtml($string, $class) {

        //dd($string);

        preg_match_all('/^#([a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/', $string, $matches);
        foreach ($matches[1] as $match) {
          $string = str_replace("#$match", "<span class='".$class."'>#$match</span>", "$string");
        }
         
        preg_match_all('/ #([a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/', $string, $matches);
        foreach ($matches[1] as $match) {
          $string = str_replace("#$match", "<span class='".$class."'>#$match</span>", "$string");
        }

        preg_match_all('/\n#([a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/', $string, $matches);
        foreach ($matches[1] as $match) {
          $string = str_replace("#$match", "<span class='".$class."'>#$match</span>", "$string");
        }
         
        return $string;
      }

      public static function linkToHtml($string){ 
        
        // The Regular Expression filter
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(\/\S*)?/";

        $i = 0; $workStr = $string;
        preg_match_all($reg_exUrl, $workStr, $urls);
        //dump($urls);

        foreach ($urls[0] as $key => $url) {
          $string = str_replace($url, "<a href=\"{$url}\" target=\"_blank\">{$url}</a> ", $string);
          //dump($string);
        }

        return $string;
      }


      public static function pastTime($date) {//, $type, $timezone=null) {

        $translator = new Translator('fr_FR');

        //dd($date);
          //$date2 = strtotime($date); // depuis cette date
          $date2 = $date->getTimestamp(); // depuis cette date
          //if(@$date->sec) $date2 = $date->getTimestamp;
          //dump($date2);
          //dd(time());
          $Ecart = time()-$date2;
          $lblEcart = "";
          $tradAgo=true;
          if(time() < $date2){
            $tradAgo=false;
            $lblEcart = $translator->trans("in")." ";
            $Ecart = $date2 - time();
          }

          $Annees = date('Y',$Ecart)-1970;
          $Mois = date('m',$Ecart)-1;
          $Jours = date('d',$Ecart)-1;
          $Heures = date('H',$Ecart);
          $Minutes = date('i',$Ecart);
          $Secondes = date('s',$Ecart);

          $res = "";
          if($Annees > 0) {
              $res = $lblEcart." ".$translator->transchoice($Annees, "year.Annees")." year ".
                     $translator->trans("and")." ".$Jours." ".$translator->transchoice($Jours, "day.Jours"); 
                     // on indique les jours avec les année pour être un peu plus précis
          }
          // else if($Mois > 0) {
          //     $res=$lblEcart.$Mois." ".$translator->trans("month".($Mois>1?"s":""));
          //     if($Jours > 0)
          //       $res.=" ".$translator->trans("and")." ".$Jours." ".$translator->trans("day".($Jours>1?"s":"")); // on indique les jours aussi
          // }
          // else if($Jours > 0) {
          //     $res=$lblEcart.$Jours." ".$translator->trans("day".($Jours>1?"s":""));
          // }
          else if($Heures > 0) {
              if($Heures < 10) $Heures = substr($Heures, 1, 1);
              $res=$lblEcart." ".$translator->transchoice($Heures, "hour.Heures"). "h";
              /*if($Minutes > 0) {
              if($Minutes < 10) $Minutes = substr($Minutes, 1, 1);
                $res.=$lblEcart." ".$translator->transchoice($Minutes, "minute.Minutes")."m";
            }*/
          }
          else if($Minutes > 0) {//var_dump($Minutes); //exit;
            if($Minutes < 10) $Minutes = substr($Minutes, 1, 1);
              $res=$lblEcart." ".$translator->transchoice($Minutes, "minute.Minutes")." min";
          }
          // else if($Secondes > 0) {
          //   if($Secondes < 10) $Secondes = substr($Secondes, 1, 1);
          //     $res=$lblEcart.$Secondes." ".$translator->trans("second".($Secondes>1?"s":""));
          // } else {
          //   //$tradAgo=false;
          //   if($tradAgo == true)
          //     $res=$translator->trans("Right now");
          //   else
          //     $res=$translator->trans("In a few second");
          // }
          // if($tradAgo)
          //   $res=$translator->trans("{time} ago",array("{time}"=>$res));
          return $res;
      }

      public static function exctractMetaData($url){
        //$url = "https://www.google.com/";
        $html = file_get_contents($url);
        $crawler = new Crawler($html);
        //dd($crawler);

        if($crawler->filter('title') == null){ $metadata['title'] = ""; } 
        else { $metadata['title'] = $crawler->filter('title')->text(); }

        if($metadata['title'] == "") 
          $metadata['title'] = $crawler->filter('meta[property="og:title"]')->attr('content');

        $metadesc = array("description"=>array('meta[property="og:description"]', 
                                               'meta[name="description"]'), 

                          "image"=>array('meta[property="og:image"]',
                                         'meta[itemprop="image"]',
                                         'meta[name="image"]', 
                                         'img'), 
                         );

        foreach ($metadesc as $key => $selector) {
          foreach ($selector as $select) {
            $elem = $crawler->filter($select);
            if($elem->count() > 0){

              $content = $elem->attr('content');
             
              $metadata[$key] = $content;

              if($metadata[$key] == "" && $select == "img")
                $metadata[$key] = $elem->attr('src');
              
              
              break;
            }
            else{
              $metadata[$key] = "";
            }
          }
        }

        if(strlen($metadata["description"]) > 200){
          $metadata["description"] = (substr(($metadata["description"]), 0, 200)).'...';
        }
        
        $parse = parse_url($url);
        if(strpos($metadata["image"], 'http') === false){
          $metadata["image"] =  $parse["scheme"]."://". $parse["host"]."/".$metadata["image"];
        }
        
        $metadata["host"] = $parse["host"];

        if($metadata["image"] == "") $metadata["image"] = "/img/no-img-link.png";

        return $metadata;
      }

      public static function getCountryList(){
        return array(
                    'France'         => 'fr',
                    'Italie'         => 'it',
                    'Espagne'        => 'es',
                    'Royaume-Uni'    => 'en',
                    );
      }

      public static function getUniqueSlug($name, $pageRepo){
        $slugExists = true; $i=0;
        while($slugExists != null && $i < 3000){ 
            $slug = Helper::slugify($name);
            if($i>0) $slug = $slug.$i;
            //dump($slug);
            $slugExists = $pageRepo->findOneBy(array("slug" => $slug));
            $i++;
        }
        return $slug;
      }

      public static function getEncryptPath(){
        $pwd = `pwd`;
        if(strpos($pwd, "public")) return '../encrypt/'; 
        else                       return 'encrypt/';
      }

      public static function getEncryptKey(){ 
        return file_get_contents(Helper::getEncryptPath().'encryptkey.txt', true);
      }
      public static function getEncryptIV(){
        return file_get_contents(Helper::getEncryptPath().'encryptiv.txt', true);
      }

      public static function getEncryptMethod(){
        return file_get_contents(Helper::getEncryptPath().'encryptmethod.txt', true);
      }

      public static function encrypt($text){
        if($text == "") return $text;
        $key = Helper::getEncryptKey();
        $iv = Helper::getEncryptIv();
        $method = Helper::getEncryptMethod();
        $encrypted = openssl_encrypt($text, $method, $key, 0, $iv);
        return $encrypted;
      }

      public static function decrypt($text){ //dump(openssl_get_cipher_methods());
        if($text == "") return $text;
        $key = Helper::getEncryptKey();
        $iv = Helper::getEncryptIv();
        $method = Helper::getEncryptMethod();
        $decrypted = openssl_decrypt($text, $method, $key, 0, $iv);
        return $decrypted;
      }
}
