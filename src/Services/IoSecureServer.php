<?php 

namespace App\Services;

use Ratchet\Server\IoServer;
use Ratchet\MessageComponentInterface;
use React\EventLoop\LoopInterface;
use React\Socket\ServerInterface;
use React\EventLoop\Factory as LoopFactory;
use React\Socket\Server as Reactor;
use React\Socket\SecureServer as SecureReactor;

class IoSecureServer extends IoServer{

	public static function factory(MessageComponentInterface $component, $port = 80, $address = '0.0.0.0', $secureOptions = []) {
		$loop = LoopFactory::create();
		$socket = new Reactor($address . ':' . $port, $loop);

	    if (!empty($secureOptions) && is_array($secureOptions)) {
	        $socket = new SecureReactor($socket, $loop, $secureOptions);

        	echo "\nSTART CHAT SECURE SERVER\n";
	    }

	    return new static($component, $socket, $loop);
	}

}