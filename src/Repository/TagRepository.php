<?php
namespace App\Repository;
 
use Doctrine\ORM\EntityRepository;
use Doctrine\ODM\MongoDB\DocumentRepository;

use App\Services\Helper;
 
class TagRepository extends DocumentRepository
{
    public function getTopTen()
    {

    	//$date = new \Datetime();
    	//$date = $date->sub(new \DateInterval("P1W"));
    	return $this->findBy(array("scope" => "P1W"), array("count" => "DESC"), 10);
    }

}
