<?php
namespace App\Repository;
 
use Doctrine\ORM\EntityRepository;
use Doctrine\ODM\MongoDB\DocumentRepository;

use App\Services\Helper;
 use App\Entity\SharedBy;
 use App\Entity\Page;
 use App\Entity\Report;

class NewsRepository extends DocumentRepository
{

    
    public function getPostsPage($page, $myPage, $filter="all", $limit=20, $tagsSearch=array(), $timestamp=null){
        $friendsId = $this->objsToId($myPage->getRelations()->getFriends());
        $followsId = $this->objsToId($myPage->getRelations()->getFollows());
        $groupsId  = $this->objsToId($myPage->getInGroups());

        $request = array('$or'=>array(), '$and'=>array());

        /* geo */
        $request['$or'][0] = array('$and' => array());
        $request['$or'][0]['$and'] = $this->addRequestGeo($request['$or'][0]['$and'], $myPage->getCoordinates());
        /*if($page->getType() != Page::TYPE_USER)
        $request['$or'][0]['$and'][] = array('$or'=>array(array('target.$id' => array('$in'=>$friendsId)),
                                                          array('target.$id' => array('$in'=>$followsId))));*/
        $request['$or'][0]['$and'][] = array('target.id' => new \MongoId($page->getId()));
        
        //j'ai access membre ou follower ou group
        $request['$or'][1] = array('$and' => array());
        $request['$or'][1]['$and'][] = array('$or'=>array(array('target.$id' => array('$in'=>$friendsId)),
                                                          array('target.$id' => array('$in'=>$followsId))));
        $request['$or'][1]['$and'][] = array('scopeType' => array('$ne' => 'localised'));
        $request['$or'][1]['$and'][] = array('target.id' => new \MongoId($page->getId()));
        
        //je suis l'auteur
        $request['$or'][2] = array('$and' => array());
        $request['$or'][2]['$and'][] = array('author.id' => new \MongoId($myPage->getId()));
        $request['$or'][2]['$and'][] = array('target.id' => new \MongoId($page->getId()));
     

        //si c'est ma page
        if($myPage->getSlug() == $page->getSlug()){
          //tous les messages que j'ai posté + les messages qui sont target sur moi
          $request['$or'][2] = array('$and' => array());
          $request['$or'][2]['$and'][] = array('$or'=>array(array('signed.id' => new \MongoId($myPage->getId())),
                                                            array('target.id' => new \MongoId($myPage->getId())),
                                                ));
          //$request['$or'][2]['$and'][] = array('target.id' => new \MongoId($page->getId()));
        }

        $request['$or'][3] = array('$and' => array());
        $request['$or'][3]['$and'][] = array('target.id' => new \MongoId($page->getId()));
        $request['$or'][3]['$and'][] = array('scopeType' => array('$in' => array('followers', "all")));
        

        /* filter news disabled by moderation */
        $request['$and'][] = array('isActive' => true);

        if($timestamp != null){
          $date = \DateTime::createFromFormat("U", $timestamp);
          //dump($timestamp);dump($date);
          /* filter news disabled by moderation */
          $request['$and'][] = array('created' =>  array('$lt' => $date));
        }
        
        /* tags */
        $request['$and'] = $this->addRequestByTags($request['$and'], $tagsSearch);

        /* exec request */
        $res = $this->findBy($request, array("created" => "DESC"), 20 /*, $skip for pagination*/);

        /* POST TRAITEMENT to filter by scope */
        foreach ($res as $key => $news) {
          if($news->getScopeType() == "friends")
          {
            if($myPage->getRelations()->getImFriend($page->getSlug()) == false &&
               $page->getId() != $myPage->getId() && 
               $news->getAuthor()->getId() != $myPage->getId()){
               unset($res[$key]);
            }
          }
          /*elseif($news->getScopeType() == "followers")
          { 
            if($myPage->getRelations()->inFollows($page->getSlug()) == false &&
               $myPage->getRelations()->getImFriend($page->getSlug()) == false &&
               $page->getId() != $myPage->getId() ){
               unset($res[$key]);
            }
          }*/
          elseif($news->getScopeType() == "groups")
          {
            $found = false;
            $groups1 = $news->getScopeGroups();
            $groups2 = $myPage->getInGroups();
            foreach ($groups1 as $k1 => $group1){
              foreach ($groups2 as $k2 => $group2){
                if($group1->getId() == $group2->getId())
                  $found = true;
              }
            }
            //dd($found);
            if($found == false && $page->getId() != $myPage->getId())
              unset($res[$key]);

          }
          elseif($news->getScopeType() == "localised")
          {
            //si je ne suis pas la cible
            //commenté : on peut lire les messages localisés sur les Pages, si on est dans la zone définit, même si on est pas abonné
            // if($news->getTarget()->getId() != $myPage->getId() &&
            //    $news->getSigned()->getId() != $myPage->getId() &&
            //    $news->getTarget()->getType() != Page::TYPE_USER &&
            //    $myPage->getRelations()->inFollows($page->getSlug()) == false &&   //ni follower
            //    $myPage->getRelations()->getImFriend($page->getSlug()) == false    //ni ami/membre
            //    ){
            //     unset($res[$key]);
            // }

          }

          if($news->getAuthor()->getIsActive() == false) unset($res[$key]);
          if($news->getSigned()->getIsActive() == false) unset($res[$key]);
          if($news->getNewsShared() != null && $news->getNewsShared()->getAuthor()->getIsActive() == false) unset($res[$key]);
          if($news->getNewsShared() != null && $news->getNewsShared()->getSigned()->getIsActive() == false) unset($res[$key]);
        }

        return $res;
    }

    public function getPostsDashboard($newsRepo, $myPage, $coordinates, $page, 
                                      $filter="all", $tagsSearch=array(), $timestamp=null, 
                                      $channels=array()){

        $friendsId = $this->objsToId($page->getRelations()->getFriends());
        $followsId = $this->objsToId($page->getRelations()->getFollows());
        $groupsId  = $this->objsToId($page->getInGroups());
        $channelsId  = $this->objsToId($channels);

        $whiteListId  = $this->objsToId($page->getRelations()->getWhiteList());
        $blackListId  = $this->objsToId($page->getRelations()->getBlackList());

        $request = array('$or'=>array(), '$and'=>array());

        /* geo */
        //if($filter == "all" || $filter == "localised")
        if($filter != "favorites")
        $request['$or'] = $this->addRequestGeo($request['$or'], $coordinates);
        
        /* for all */
        if($filter == "all") {
          $request['$or'][] = array('scopeType' => 'all');
          //dump($channelsId);
          if(!empty($channelsId)){
              //$res = $this->findBy(array('scopeType' => 'all'), array("created" => "DESC"), 15 /*, $skip for pagination*/);
              $request['$and'][] = array('signed.$id' => array('$nin'=>$channelsId));
          }
        }
        
        
        /* signed friends/members */
        if($filter == "all" || $filter == "contacts")
        $request['$or'] = $this->addRequestByContact($request['$or'], $friendsId, $followsId, $groupsId, $page);
        
        
        /* tags */
        $request['$and'] = $this->addRequestByTags($request['$and'], $tagsSearch);
        
        /*  */
        if($filter != "favorites")
          $request['$and'] = $this->addRequestBlackList($request['$and'], $blackListId);
        else
          $request['$and'] = $this->addRequestWhiteList($request['$and'], $whiteListId);


        /* filter news disabled by moderation */
        $request['$and'][] = array('isActive' => true);

        if($timestamp != null){
          $date = \DateTime::createFromFormat("U", $timestamp);
          //dump($timestamp);dump($date);
          /* filter news disabled by moderation */
          $request['$and'][] = array('created' =>  array('$lt' => $date));
        }
        //dump($request);
        /* exec request */
        $res = $this->findBy($request, array("created" => "DESC"), 15 /*, $skip for pagination*/);

        /* post traitement */
        $res = $this->clearMultiShare($res);
        
        //dd($request);
        //dump($res);
        /* POST TRAITEMENT to filter by scope */
        foreach ($res as $key => $news) {
          //dump($news->getScopeType());
          if($news->getScopeType() == "localised")
          {           
            //si je ne suis pas la cible
            if(($news->getTarget()->getId() != $page->getId() && 
                ($news->getTarget()->getType() != "user" || $filter == "contacts")) &&

               $page->getRelations()->inFollows($news->getTarget()->getSlug()) == false &&   //ni follower
               $page->getRelations()->getImFriend($news->getTarget()->getSlug()) == false    //ni ami/membre
               //$page->getSlug() != 
               ){
                //dump("unset - ".$news->getText());
                unset($res[$key]);
            }

            if($news->getAuthor()->getIsActive() == false) unset($res[$key]);
            if($news->getSigned()->getIsActive() == false) unset($res[$key]);
            if($news->getNewsShared() != null && $news->getNewsShared()->getAuthor()->getIsActive() == false) unset($res[$key]);
            if($news->getNewsShared() != null && $news->getNewsShared()->getSigned()->getIsActive() == false) unset($res[$key]);
          }
        }
        //dump($res);
        return $res;
    }

    public function getPostsForward($newsRepo, $coordinates, $page, $tagsSearch=array(), $attCondition, $countCondition){

        $friendsId = $this->objsToId($page->getRelations()->getFriends());
        $followsId = $this->objsToId($page->getRelations()->getFollows());
        $groupsId  = $this->objsToId($page->getInGroups());

        $request = array('$or'=>array(), '$and'=>array());

        /* geo */
        $request['$or'] = $this->addRequestGeo($request['$or'], $coordinates);
        
        /* signed friends/members */
        $request['$or'] = $this->addRequestByContact($request['$or'], $friendsId, $followsId, $groupsId, $page);
        
        /* tags */
        $request['$and'] = $this->addRequestByTags($request['$and'], $tagsSearch);
        
        $request['$and'][] = array( $attCondition.".".$countCondition => array('$exists'=>true) );
        //exemple :                 likes.1                           => array('$exists'=>true);
        
        /* filter news disabled by moderation */
        $request['$and'][] = array('isActive' => true);
        
        /* only news publicated last 14 days */
        $date = new \Datetime();
        $date->sub(new \DateInterval("P14D"));
        $request['$and'][] = array('created' => array('$gt'=>$date));
        
        /* exec request */
        $res = $this->findBy($request, array("created" => "DESC"), 20 /*, $skip for pagination*/);

        /* post traitement */
        $res = $this->clearMultiShare($res);

        /* select the news most liked */
        $newsMostLike = null; $nbLikes = 0;
        foreach ($res as $key => $news) {
          if($attCondition == "likes"){
            if(count($news->getLikes()) > $nbLikes){
              $newsMostLike = $news;
              $nbLikes = count($news->getLikes());
            }
          }elseif($attCondition == "dislikes"){
            if(count($news->getDislikes()) > $nbLikes){
              $newsMostLike = $news;
              $nbLikes = count($news->getDislikes());
            }

          }
        }

        $res = array($newsMostLike);
        
        //dump($res);
        return $res;
    }

    public function getPostsToday($page){
      /* only news publicated last 14 days */
      $date = new \Datetime();
      $date->sub(new \DateInterval("P1D"));

      $request = array('signed.$id' => new \MongoId($page->getId()),
                       'scopeType' => 'all',
                       'created' => array('$gt'=>$date));

      $res = $this->findBy($request, array("created" => "ASC"));
      return $res;
    }


    private function addRequestGeo($request, $coordinates){
      if($coordinates != false && $coordinates[0] != "false" && $coordinates[1] != "false"){
        $request[] = array('scopeGeo' => 
                       array('$geoIntersects' => //si je suis dans la zone géographique du scopeGeo
                          array('$geometry'=> 
                              array('type' => "Point",
                                    'coordinates' => $coordinates) )), 
                            'scopeType'=>'localised');
        return $request;
      }else{ 
        //$request[] = array('scopeType'=>'localised-forbiden');
        return $request; }
    }

    private function addRequestByContact($request, $friendsId, $followsId, $groupsId, $page){
      $request[] = array('signed.$id' => new \MongoId($page->getId())); //si je suis l'auteur du message
      $request[] = array('target.$id' => new \MongoId($page->getId())); //si je suis la cible du message

      /* target followers */
      $request[] = array('target.$id' => array('$in'=>$followsId),  //si je suis abonné à la cible qui reçoit
                         '$or'=>array(array('scopeType'=>'followers'), 
                                      array('scopeType' => 'all')));//et que le scope est pour ses followers

      /* target friend */
      $request[] = array('target.$id' => array('$in'=>$friendsId),  //si la cible est un ami ou 
                                                                    //une page dont je suis membre
                        '$or'=>array(array('scopeType'=>'followers'), 
                                      array('scopeType' => 'all'))); //et que le scope est pour ses followers

      /* target friends/members */
      $request[] = array('target.$id' => array('$in'=>$friendsId),  //si la cible est un ami ou 
                                                                    //une page dont je suis membre
                         '$or'=>array(array('scopeType'=>'friends'), 
                                      array('scopeType' => 'all'))); //et que le scope est pour ses followers

      /* groups */
      $request[] = array('scopeGroups.$id' => array('$in'=>$groupsId), 
                         'scopeType'=>'groups'); //si le scope est pour un groupe auquel j'appartiens

      return $request;
    }

    private function addRequestByTags($request, $tagsSearch){
      if(!empty($tagsSearch))
        $request[] = array('tags' => array('$in'=>$tagsSearch)); //si les tags correspondent aux tags demandés
      return $request;
    }


    private function addRequestWhiteList($request, $whiteListId){
      /*  */
      $request[] = array('$or'=>array(
                                  array('target.$id' => array('$in'=>$whiteListId)),
                                  array('signed.$id' => array('$in'=>$whiteListId))
                    ));
      return $request;
    }
    private function addRequestBlackList($request, $blackListId){
      /*  */
      $request[] = array('target.$id' => array('$nin'=>$blackListId),  //si la cible est dans ma whitelist
                         'signed.$id' => array('$nin'=>$blackListId)); //ou que l'auteur est dans ma whitelist
      return $request;
    }



    private function clearMultiShare($res){
      /* post traitement 
        remove all multiple share
        wrap all share in the fist share
      */
      $imax=1000; $i=0; //care to infinite loop
      foreach ($res as $k1 => $post1) {  //dump($post1);
          if($post1->getNewsShared() != null){
              $post1->setSharedByMyRelations(array());
              foreach ($res as $k2 => $post2) {  
                  if($k2 > $k1){ //dont look to news already managed
                      if($post2->getNewsShared() != null && 
                         $post1->getNewsShared()->getId() == $post2->getNewsShared()->getId()){
                         $post1->addSharedByMyRelations(clone $post2);
                         unset($res[$k2]);
                      }
                      if($post1->getNewsShared()->getId() == $post2->getId()){
                         unset($res[$k2]);
                      }
                  }
                  $i++;
                  if($i > $imax){ dump("infinite loop in NewsRepository::clearMultiShare()");  dump($i);  exit; }
              }
          }
          $i++;
      }
      return $res;
    }

    private function objsToId($array){
      //dump($array);
      $res = array();
      if(!empty($array))
        foreach($array as $key => $val)  { $res[] = new \MongoId($val->getId()); }

      //dd($res);
      return $res;
    }


    public function getCount($scopeType){ //dd($myFriends);
        $res = array();
        $request = array();
        
        if($scopeType != "")
        $request = array('scopeType' => $scopeType);
        
        $res = count($this->findBy($request));
        return $res;
    }

}
