<?php
namespace App\Form;

use App\Entity\User;
use App\Entity\Page;
use App\Entity\Group;
use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Translation\TranslatorInterface;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;

use Doctrine\ODM\MongoDB\DocumentRepository;
use App\Form\SharedByType;
use App\Form\MediasType;

class NewsType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	//dd($options);
    	//if(!isset($options["attr"]["pageGroups"])) return;
    	//$pageGroups = $options["attr"];
		
		$builder
            ->add('text', TextareaType::class, [
                  'required'   => false ])

            /*->add('scopeGeo', HiddenType::class, [
                  'required'   => false])*/

            ->add('scopeType', HiddenType::class, [
                  'required'   => true ])

            ->add('scopeGroups', HiddenType::class, [
                  'required'   => false ])
            
            ->add('radiusKm', HiddenType::class, [
                  'required'   => false ])

            ->add('isSharable', HiddenType::class, [
                  'required'   => false ])

            /*->add('medias', HiddenType::class, [
                  'required'   => false ])*/ 

            ->add('image', HiddenType::class, [
                  'required'   => false ])
            ;

      
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => News::class,
            "csrf_protection" => false,
            "allow_extra_fields" => true,
        ));
    }
}
