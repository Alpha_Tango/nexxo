<?php
namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Translation\TranslatorInterface;

//use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;

class UserType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => $this->translator->trans('E-mail address', array(), "form") ])

            ->add('username', TextType::class, [
                'label' => $this->translator->trans('Username / Pseudo', array(), "form"),
                'attr' => ['pattern' => '[a-zA-Z0-9ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ-_]*']
            ]);

        if (in_array('registration', $options['validation_groups'])) {
            $builder
                ->add('plainPassword', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'first_options'  => array('label' => $this->translator->trans('Password', array(), "form")),
                    'second_options' => array('label' => $this->translator->trans('Confirm password', array(), "form")),
                ));
        } else {
            $builder
                ->add('plainPassword', RepeatedType::class, array(
                    'required' => false,
                    'type' => PasswordType::class,
                    'first_options'  => array('label' => $this->translator->trans('Password', array(), "form")),
                    'second_options' => array('label' => $this->translator->trans('Confirm password', array(), "form")),
                ));
        }

        // $builder->add('captchaCode', CaptchaType::class, array(
        //   'captchaConfig' => 'RegisterCaptcha',
        // ));

        //$builder->add('terms', CheckboxType::class, array('property_path' => 'termsAccepted'));
        //https://symfony.com/doc/master/bundles/DoctrineMongoDBBundle/form.html
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
